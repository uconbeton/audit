<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahapan extends Model
{
    protected $table = 'tahapan';
    public $timestamps = false;
    public function jadwal()
    {
        return $this->hasOne('App\Jadwal');
    }
}
