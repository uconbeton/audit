<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApproveDeskaudit extends Model
{
    protected $table = 'approve_deskaudit';
    public $timestamps = false;
}
