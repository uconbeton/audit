<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $primaryKey ='nik';
    
    public function unit_kerja()
    {
        return $this->belongsTo('App\UnitKerja');
    }

    public function obyektim()
    {
        return $this->belongsTo('App\ObyekTim', 'nik', 'nik');
    }

    public function posisi()
    {
        return $this->belongsTo('App\Posisi');
    }

    public function obyekKaryawans()
    {
        return $this->hasOne('App\ObyekTim', 'nik', 'nik');
    }

    
}
