<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LangkahKerja extends Model
{
    protected $table = 'langkah_kerja';
    public $timestamps = false;
}
