<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mengetahui extends Model
{
    protected $table = 'mengetahui';
    public $timestamps = false;
}
