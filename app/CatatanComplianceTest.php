<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatatanComplianceTest extends Model
{
    protected $table = 'catatan_compliance_test';
    public $timestamps = false;
}
