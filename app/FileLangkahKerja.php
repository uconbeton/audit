<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileLangkahKerja extends Model
{
    protected $table = 'file_langkah_kerja';
    public $timestamps = false;
}
