<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriAudit extends Model
{
    protected $table = 'kategori_audit';
    public $timestamps = false;
}
