<?php

namespace App\Imports;

use App\Temuan;
use Maatwebsite\Excel\Concerns\ToModel;

class TemuanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $cek=Temuan::where('tahun',$row[0])->where('kode_lap',$row[1])->where('kode_lha',$row[2])->where('no_temuan',$row[3])->count();
        if($cek>0){

        }else{
            return new Temuan([
            
                'sts'   =>0,
                'tahun' =>$row[0],
                'kode_lap' =>$row[1],
                'kode_lha' =>$row[2],
                'no_temuan' =>$row[3],
                'last_status' =>$row[4],
                'pic' =>$row[5],
                'aging' =>$row[6],
                'email' =>$row[7],
                'cc' =>$row[8],
                'keterangan' =>$row[9],
                'tanggal' =>date('Y-m-d'),
                
            ]);
        }
        
    }
}
