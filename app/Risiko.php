<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Risiko extends Model
{
    protected $table = 'risiko';
    public $timestamps = false;
}
