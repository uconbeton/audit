<?php

namespace App\Observers;

use App\ObyekTim;
use Session;

class ObyekTimObserver
{
    public function updating(ObyekTim $obyekTim)
    {
        Session::flash("flash_notification",[
            "level" => "danger",
            "message" => "Ora gelem",
        ]);
        return false;
    }
    /**
     * Handle the obyek tim "created" event.
     *
     * @param  \App\ObyekTim  $obyekTim
     * @return void
     */
    public function created(ObyekTim $obyekTim)
    {
        //
    }

    /**
     * Handle the obyek tim "updated" event.
     *
     * @param  \App\ObyekTim  $obyekTim
     * @return void
     */
    public function updated(ObyekTim $obyekTim)
    {
        //
    }

    /**
     * Handle the obyek tim "deleted" event.
     *
     * @param  \App\ObyekTim  $obyekTim
     * @return void
     */
    public function deleted(ObyekTim $obyekTim)
    {
        //
    }

    /**
     * Handle the obyek tim "restored" event.
     *
     * @param  \App\ObyekTim  $obyekTim
     * @return void
     */
    public function restored(ObyekTim $obyekTim)
    {
        //
    }

    /**
     * Handle the obyek tim "force deleted" event.
     *
     * @param  \App\ObyekTim  $obyekTim
     * @return void
     */
    public function forceDeleted(ObyekTim $obyekTim)
    {
        //
    }
}
