<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    protected $table = 'posisi';
    public $timestamps = false;
    public function tim()
    {
        return $this->hasMany('App\Tim');
    }

    public function obyektim()
    {
        return $this->belongsTo('App\ObyekTim');
    }
}
