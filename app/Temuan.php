<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temuan extends Model
{
    protected $table = 'temuan';
    public $timestamps = false;
    protected $fillable = [
        'tahun', 'kode_lha', 'kode_lap','no_temuan','last_status','pic','aging','email','sts','cc','keterangan','tanggal',
    ];
}
