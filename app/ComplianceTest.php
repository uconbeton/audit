<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplianceTest extends Model
{
    protected $table = 'compliance_test';
    public $timestamps = false;
}
