<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruanglingkup extends Model
{
    protected $table = 'ruanglingkup';
    public $timestamps = false;
    public function divisi()
    {
        return $this->belongsTo('App\Divisi');
    }

    public function unit_kerja()
    {
        return $this->belongsTo('App\UnitKerja');
    }
}
