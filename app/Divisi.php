<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    protected $table = 'divisi';
    public $timestamps = false;

    

    public function obyeks()
    {
        return $this->hasMany('App\Obyek');
    }

    public function ruanglingkup()
    {
        return $this->hasMany('App\Ruanglingkup');
    }
}
