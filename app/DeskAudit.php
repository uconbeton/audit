<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeskAudit extends Model
{
    protected $table = 'desk_audit';
    public $timestamps = false;
}
