<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LangkahSubstantive extends Model
{
    protected $table = 'langkah_kerja_substantive';
    public $timestamps = false;
}
