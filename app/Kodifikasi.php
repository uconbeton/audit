<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kodifikasi extends Model
{
    protected $table = 'kodifikasi';
    public $timestamps = false;
}
