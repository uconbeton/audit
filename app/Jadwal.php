<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    public $timestamps = false;
    protected $dates= [
        'tgl_mulai',
        'tgl_sampai'
    ];
    

    public function tahapan()
    {
        return $this->belongsTo('App\Tahapan');
    }
    
}
