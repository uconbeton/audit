<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekomendasi extends Model
{
    protected $table = 'rekomendasi';
    public $timestamps = false;
    public function subrekomendasi()
    {
        return $this->hasMany('App\SubRekomendasi', 'rekomendasi_id', 'id');
    }
    public function subrekom()
    {
        return $this->hasMany('App\SubRekomendasi', 'rekomendasi_id', 'id');
    }
    public function loadJumRek()
    {
        return $this->load(['subrekomendasi'])->subrekomendasi()->count();
    }
}
