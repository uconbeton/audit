<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObyekTim extends Model
{
    protected $table = 'obyek_tim';
    public $timestamps = false;
    public function karyawan()
    {
        return $this->belongsTo('App\Karyawan', 'nik', 'nik');
    }

    public function posisi()
    {
        return $this->belongsTo('App\Posisi');
    }

    public function karyawans()
    {
        return $this->hasOne('App\Karyawan', 'nik', 'nik');
    }
}
