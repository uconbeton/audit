<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obyek extends Model
{
   protected $table = 'obyek';
   public $timestamps = false;
    public function unit_kerja()
    {
        return $this->belongsTo('App\UnitKerja');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function obyektim()
    {
        return $this->belongsTo('App\ObyekTim');
    }
    public function lha()
    {
        return $this->hasOne('App\LhaAudit');
    }

    public function kategori_audit()
    {
        return $this->belongsTo('App\KategoriAudit');
    }

    public function pengawas()
    {
        return $this->hasOne('App\Karyawan', 'nik', 'nik_pengawas');
    }

    public function jadwal()
    {
        return $this->hasMany('App\Jadwal', 'obyek_id', 'id');
    }

    public function loadDeskAuditProgram()
    {
        return $this->jadwal()->where('tahapan_id', 1)->first();
    }

    public function loadDeskAuditCatatan()
    {
        return $this->jadwal()->where('tahapan_id', 2)->first();
    }

    public function loadComplianceProgram()
    {
        return $this->jadwal()->where('tahapan_id', 3)->first();
    }

    public function loadComplianceCatatan()
    {
        return $this->jadwal()->where('tahapan_id', 4)->first();
    }

    public function loadSubstantiveeProgram()
    {
        return $this->jadwal()->where('tahapan_id', 5)->first();
    }

    public function loadSubstantiveCatatan()
    {
        return $this->jadwal()->where('tahapan_id', 6)->first();
    }
    public function loadPenyusunanLha()
    {
        return $this->jadwal()->where('tahapan_id', 7)->first();
    }

    public function loadMemoLha()
    {
        return $this->jadwal()->where('tahapan_id', 8)->first();
    }

    public function loadPenerbitanLha()
    {
        return $this->jadwal()->where('tahapan_id', 9)->first();
        
    }

    public function ketua_tim()
    {
        return $this->hasOne('App\Karyawan', 'nik', 'nik_ketua_tim');
    }

}


