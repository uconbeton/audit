<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $table = 'unit_kerja';
    public $timestamps = false;
    public function obyeks()
    {
        return $this->hasMany('App\Obyek');
    }

    public function ruanglingkup()
    {
        return $this->hasMany('App\Ruanglingkup');
    }
}
