<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatatanDeskAudit extends Model
{
    protected $table = 'catatan_deskAudit';
    public $timestamps = false;
}
