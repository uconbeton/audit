<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class useraudit extends Model
{
    protected $table = 'useraudit';
    public $timestamps = false;
    public function akses()
    {
        return $this->hasOne('App\akses', 'id', 'akses_id');
    }

    public function karyawan()
    {
        return $this->hasOne('App\Karyawan', 'nik', 'nik');
    }
}

