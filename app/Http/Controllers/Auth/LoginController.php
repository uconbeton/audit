<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
Use App\Karyawan;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }


    public function programaticallyEmployeeLogin(Request $request, $personnel_no, $email=null)
    {
        $personnel_no   = base64_decode($personnel_no);
        $email          = base64_decode($email);
        // $personnel_no   = $personnel_no;
        // $email          = $email;
        try {
            
                $userlogin = Karyawan::where('nik', $personnel_no)->first();
                if(is_null($userlogin)){
                    return redirect('https://sso.krakatausteel.com/public/index');
                }else{
                    Auth::loginUsingId($userlogin->nik);
                    return redirect('/');
                }
                
                
                
        } catch (ModelNotFoundException $e) {
            
            return redirect('https://sso.krakatausteel.com/public/index');
        }
        return $this->sendLoginResponse($request);
    }
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //---- menginisial username
    public function username(){
        return 'nik';
    }

    // public function logout(Request $request)
    // {
    //     $this->performLogout($request);
    //     return redirect('/');
    // }

    public function logout(Request $request)
    {
        // do the normal logout
        $this->performLogout($request);
        
        // redirecto to sso
        return redirect()->away('https://sso.krakatausteel.com');
    }

}
