<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use Session;
use App\Obyek;
use App\Tahapan;
use App\Sasaran;
use App\Risiko;
use App\RuangLingkup;
use App\ObyekTim;
use App\Unit;
use App\UnitKerja;
use App\Tujuan;
use App\Jadwal;
use App\Posisi;
use App\HisPic;
use App\Pic;
use App\KategoriAudit;
use App\SubRekomendasi;
use App\SubSubRekomendasi;
use App\Karyawan;
use App\LhaAudit;
use App\Kodifikasi;
use App\Rekomendasi;
use App\Mengetahui;
use App\DeskAudit;
use App\LangkahKerja;
use App\ComplianceTest;
use App\LangkahCompliance;
use App\SubstantiveTest;
use App\LangkahSubstantive;
use App\FileLangkahKerja;

class LhaController extends Controller
{
    public function index(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
        if(Auth::user()->posisi_id==1){
            $obyeks = Obyek::with(['lha','unit_kerja','pengawas','ketua_tim'])->where('sts_pencatatan_substantive',1)->where('tahun_pkat',$tahun)->where('nik_pengawas',Auth::user()->nik)->orWhere('nik_ketua_tim',Auth::user()->nik)->orderBy('id','DESC')->paginate(10);
        }elseif(Auth::user()->posisi_id==5){
            $obyeks = Obyek::with(['lha','unit_kerja','pengawas','ketua_tim'])->where('sts_pencatatan_substantive',1)->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        }else{
            $obyeks = Obyek::with(['lha','unit_kerja','pengawas','ketua_tim'])->where('sts_pencatatan_substantive',1)->where('tahun_pkat',$tahun)->where('nik_ketua_tim',Auth::user()->nik)->orWhere('nik_anggota','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->orderBy('id','DESC')->paginate(10);
        }
        
        $obyeks->map(function ($item, $key){
            $item['nama_create']=Karyawan::where('nik',$item->lha['nik_create'])->first();
            return $item;
        });

        //dd($obyeks->toArray());


        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        
        return view( 'lha.index', compact('obyeks','obyektims','tujuans') );

    
    }

    public function form($id){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data kategori
            $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
        //Tampilkan data jadwal
            $jadwalmulai = Jadwal::where('obyek_id',$id)->where('tahapan_id',1)->first();
            $jadwalsampai = Jadwal::where('obyek_id',$id)->where('tahapan_id',9)->first();
            // var_dump($obyeks);
        //Tampilkan data Kodifikasi
            $kodifikasi  = Kodifikasi::all();
        //Tampilkan data sasaran
            $sasaran  = Sasaran::where('obyek_id',$id)->where('sts',1)->first();
           // dd($sasaran);
            $sasarans  = Sasaran::with(['unitkerja'])->where('obyek_id',$id)->where('sts',2)->get();
        //Tampilkan data ruanglingkup
            $ruanglingkup  = RuangLingkup::where('obyek_id',$id)->where('sts',1)->first();
            $ruanglingkups  = RuangLingkup::where('obyek_id',$id)->where('sts',2)->get();
       
        
        return view( 'lha.form', compact('obyeks','sasaran','sasarans','kodifikasi','ruanglingkup','ruanglingkups','kategori','jadwalmulai','jadwalsampai') );

    
    }

    public function formrekomendasi($id){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data rekomendasi
            $rekomendasi = Rekomendasi::where('obyek_id',$id)->get();
        //Tampilkan data rekomendasi
            $pics = UnitKerja::whereIn('unit_id',['3','1','5','6','4'])->get();
        
        return view( 'lha.formrekomendasi', compact('rekomendasi','obyeks','pics') );

    
    }

    public function formsub($id){
        $subrekomendasi = SubRekomendasi::where('id',$id)->first();
        $subsubrekomendasi = SubSubRekomendasi::where('sub_rekomendasi_id',$id)->get();
        $alphabet = range('a', 'z');
        //Tampilkan data Kodifikasi
        $kodifikasi  = Kodifikasi::all();   
        return view( 'lha.formsub', compact('subrekomendasi','kodifikasi','alphabet','subsubrekomendasi') );

    
    }

    public function editrekomendasi($id){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data rekomendasi
            $rekomendasi = Rekomendasi::where('obyek_id',$id)->get();
            $rekomendasisub = Rekomendasi::with(['subrekom'])->where('obyek_id',$id)->first();

        //dd($rekomendasisub->subrekom);
        //Tampilkan data rekomendasi
            $subrekomendasi = SubRekomendasi::where('obyek_id',$id)->get();

        //Tampilkan data rekomendasi
            $subsubrekomendasi = SubSubRekomendasi::all();
        
        //Tampilkan data Kodifikasi
            $kodifikasi  = Kodifikasi::all();   

        //Tampilkan data rekomendasi
            $pics = UnitKerja::whereIn('unit_id',['3','1','5','6','4'])->get();
        
        return view( 'lha.editrekomendasi', compact('rekomendasi','subrekomendasi','kodifikasi','obyeks','pics','subsubrekomendasi','rekomendasisub') );

    
    }

    public function tanggapan($id){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data rekomendasi
            $rekomendasi = Rekomendasi::where('obyek_id',$id)->get();
            $rekomendasisub = Rekomendasi::with(['subrekom'])->where('obyek_id',$id)->first();

        //Tampilkan data rekomendasi
            $subrekomendasi = SubRekomendasi::where('obyek_id',$id)->get();

        //Tampilkan data rekomendasi
            $subsubrekomendasi = SubSubRekomendasi::all();
            
        //Tampilkan data rekomendasi
            $pics = UnitKerja::whereIn('unit_id',['3','1','5','6','4'])->get();
        
        return view( 'lha.tanggapan', compact('rekomendasi','subrekomendasi','obyeks','pics','subsubrekomendasi','rekomendasisub') );

    
    }

    public function edit($id){
        error_reporting(0);
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data lha
            $lha = LhaAudit::where('obyek_id',$id)->first();
            $jumlha = LhaAudit::where('obyek_id',$id)->count();
        //Tampilkan data rekomendasi
            $rekomendasi = Rekomendasi::where('obyek_id',$id)->where('lha_audit_id',$lha['id'])->get();
            $counts = Rekomendasi::where('obyek_id',$id)->where('lha_audit_id',$lha['id'])->count();
        //Tampilkan data Kodifikasi
            $kodifikasi  = Kodifikasi::all();   
        return view( 'lha.edit', compact('obyeks','lha','rekomendasi','kodifikasi','counts') );

    
    }

    public function laporan($id){
        error_reporting(0);
        //$phpWord = new \PhpOffice\PhpWord\PhpWord();
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data lha
            $lha = LhaAudit::where('obyek_id',$id)->first();
        //Tampilkan data rekomendasi
            $rekomendasi = Rekomendasi::where('obyek_id',$id)->where('lha_audit_id',$lha->id)->orderBy('nomor', 'asc')->get();
            $jumsub = collect([]);
            foreach($rekomendasi as $rekomen){
                $subreko= SubRekomendasi::where('rekomendasi_id',$rekomen->id)->count();
                $jumsub->push([
                    'rekomendasi_id' => $rekomen->id,
                    'count' => $subreko
                ]);
            }
           
            $subrekomendasi = SubRekomendasi::with(['pic'])->get();
            $subsubrekomendasi = SubSubRekomendasi::all();
            $counts = Rekomendasi::where('obyek_id',$id)->where('lha_audit_id',$lha->id)->count();
        
        return view( 'lha.laporanlha', compact('jumsub','subsubrekomendasi','obyeks','lha','rekomendasi','counts','subrekomendasi') );

    
    }

    public function insert(Request $request){
        
       
        $rules = [
            'latar_belakang'     => 'required',
            'sasaran'     => 'required',
            'ruang_lingkup' => 'required',
            'pelaksanaan' => 'required',
            'penjelasan'              => 'required'
            
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('lha/form/'.$request->id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $obyek                   =  Obyek::find($request->id);
        $obyek->sts_lha          =  3;
        $obyek->no_surattugas    =  0;
        $obyek->sts_surattugas    = 0;
        $obyek->no_memo          =  0;
        $obyek->sts_memo         =  0;
        $obyek->no_pengantar     =  0;
        $obyek->sts_pengantar    =  0;
        $obyek->save();
        
        $data   = New LhaAudit;
        $data->latar_belakang             =$request->latar_belakang;
        $data->sasaran                    =$request->sasaran;
        $data->ruang_lingkup              =$request->ruang_lingkup;
        $data->pelaksanaan                =$request->pelaksanaan;
        $data->penutup                    =$request->penutup;
        $data->kesimpulan                 =$request->kesimpulan;
        $data->obyek_id                   =$request->id;
        $data->penjelasan                 =$request->penjelasan;
        $data->nik_create                 =Auth::user()->nik;
        $data->tanggal                    =date('Y-m-d h:i:s');
        $data->save();

        

        // ---- save rekomendasi lha

        for ($x=0;$x < sizeof($request->rekomendasi6);$x++) {
            $rekomendasi                   = New Rekomendasi;
            $rekomendasi->nomor            = $request->nomor[$x];
            $rekomendasi->head_rekomendasi = $request->head_rekomendasi[$x];
            $rekomendasi->kodifikasi       = $request->kodifikasi[$x];
            $rekomendasi->judul            = $request->judul[$x];
            $rekomendasi->rekomendasi      = $request->rekomendasi6[$x];
            $rekomendasi->lha_audit_id     = $data->id;
            $rekomendasi->obyek_id         = $request->id;
            $rekomendasi->save();
        } 

       
       
        if($data){
            return redirect('/lha/edit/'.$request->id);
        }
     }


     public function insertrekomendasi(Request $request){
        

        $rekomendasi = Rekomendasi::where('obyek_id',$request->id)->get();
        
        $obyek                      = Obyek::find($request->id);
        $obyek->sts_sublha          = 3;
        $obyek->save();
        
        $data                       = LhaAudit::where('obyek_id',$request->id)->first();
        $data->nik_create           = Auth::user()->nik;
        $data->tanggal              = date('Y-m-d h:i:s');
        $data->save();

        
        foreach ($rekomendasi as $no => $rek) {
            $nomor='subnomor'.$no;
            $subnomor = $request->$nomor;

            $rekom='subrekomendasi'.$no;
            $subrekom = $request->$rekom;

            $pic='pic_id'.$no;
            $picid = $request->$pic;
            if(is_null($subnomor)){

            }else{

            
                for ($x=0;$x < sizeof($subnomor);$x++) {
                    if($subrekom[$x]==''){

                    }else{
                        $rekomendasi                   = New SubRekomendasi;
                        $rekomendasi->subnomor         = $subnomor[$x];
                        $rekomendasi->subrekomendasi   = $subrekom[$x];
                        $rekomendasi->no               = $x;
                        $rekomendasi->totalsub         = 0;
                        $rekomendasi->tanggal          = date('Y-m-d');
                        $rekomendasi->pic_id           = $picid[$x];
                        $rekomendasi->obyek_id         = $request->id;
                        $rekomendasi->rekomendasi_id   = $rek->id;
                        $rekomendasi->save();

                        $pic                   = New HisPic;
                        $pic->subnomor         = $subnomor[$x];
                        $pic->obyek_id         = $request->id;
                        $pic->tanggal          = date('Y-m-d');
                        $pic->pic_id           = $picid[$x];
                        $pic->sts              = 0;
                        $pic->sub_rekomendasi_id = $rekomendasi->id;
                        $pic->save();
                    }
                    
                    
                } 
           
          }  
        } 
       
        
        if($rekomendasi){
            return redirect('/lha/editrekomendasi/'.$request->id);
        }
     }

     public function inserttanggapan(Request $request){
        

        $rekomendasis = Rekomendasi::where('obyek_id',$request->id)->get();
        $data                       = LhaAudit::where('obyek_id',$request->id)->first();
        $data->nik_create           = Auth::user()->nik;
        $data->tanggal              = date('Y-m-d h:i:s');
        $data->save();

        
        foreach ($rekomendasis as $no => $rek) {
            $rekomendasi                   =  Rekomendasi::find($request->rekomendasi_id[$no]);
            $rekomendasi->tanggapan        =  $request->tanggapan[$no];
            $rekomendasi->save();          
        } 
       
        
        if($rekomendasi){
            return redirect('/lha/tanggapan/'.$request->id);
        }
     }

     public function insertsub(Request $request){
            
            $data                       = LhaAudit::where('obyek_id',$request->obyek_id)->first();
            $data->nik_create           = Auth::user()->nik;
            $data->tanggal              = date('Y-m-d h:i:s');
            $data->save();

            $subnomor = $request->subnomor;
            $deletesub=SubSubRekomendasi::where('sub_rekomendasi_id',$request->id)->delete();
            if(is_null($subnomor)){
                $subrekomendasi                   =  SubRekomendasi::find($request->id);
                $subrekomendasi->totalsub         =  0;
                $subrekomendasi->save();  
            }else{

                $subrekomendasi                   =  SubRekomendasi::find($request->id);
                $subrekomendasi->totalsub         =  sizeof($subnomor);
                $subrekomendasi->kodifikasi       =  0;
                $subrekomendasi->save();  
                
                
                for ($x=0;$x < sizeof($subnomor);$x++) {
                    $alphabet = range('a', 'z');
                    $rekomendasi                    = New SubSubRekomendasi;
                    $rekomendasi->subnomor          = $subnomor[$x].'.'.$alphabet[$x];
                    $rekomendasi->subrekomendasi    = $request->subsubrekomendasi[$x];
                    $rekomendasi->kodifikasi        = $request->kodefikasi[$x];
                    $rekomendasi->nilai             = $request->nilai[$x];
                    $rekomendasi->no                = $alphabet[$x];
                    $rekomendasi->obyek_id          = $request->obyek_id;
                    $rekomendasi->sub_rekomendasi_id= $request->id;
                    $rekomendasi->save();
                    
                    
                } 
           
          }  
        
        
        if($subrekomendasi){
            return redirect('/lha/formsub/'.$request->id);
        }
     }

     public function updaterekomendasi(Request $request){
        
        $data                       =LhaAudit::where('obyek_id',$request->id)->first();
        $data->nik_create           = Auth::user()->nik;
        $data->tanggal              = date('Y-m-d h:i:s');
        $data->save();

        $rekomendasi = Rekomendasi::where('obyek_id',$request->id)->get();
        
       // $subrekomendasi=SubRekomendasi::where('obyek_id',$request->id)->delete();
        foreach ($rekomendasi as $no => $rek) {
            $nomor='subnomor'.$no;
            $subnomor = $request->$nomor;

            $rekom='subrekomendasi'.$no;
            $subrekom = $request->$rekom;

            $idsub='sub_rekomendasi_id'.$no;
            $idsubrek = $request->$idsub;

            $pic='pic_id'.$no;
            $picid = $request->$pic;

            $kodi='kodefikasi'.$no;
            $kodifikasi = $request->$kodi;

            $nil='nilai'.$no;
            $nilai = $request->$nil;
            
            if(is_null($subnomor)){

            }else{

            
                for ($x=0;$x < sizeof($subnomor);$x++) {
                    
                      if($request->$idsub[$x]=='0'){
                        $rekomendasi                   = New SubRekomendasi;
                        $rekomendasi->subnomor         = $subnomor[$x];
                        $rekomendasi->subrekomendasi   = $subrekom[$x];
                        $rekomendasi->kodifikasi       = $kodifikasi[$x];
                        $rekomendasi->nilai            = $nilai[$x];
                        $rekomendasi->no               = ($x+1);
                        $rekomendasi->tanggal          = date('Y-m-d');
                        $rekomendasi->pic_id           = $picid[$x];
                        $rekomendasi->obyek_id         = $request->id;
                        $rekomendasi->rekomendasi_id   = $rek->id;
                        $rekomendasi->save();
                      }else{
                        $cekrek= SubSubRekomendasi::where('sub_rekomendasi_id',$request->$idsub[$x])->count();
                        if($cekrek>0){$kodif=0;}else{$kodif=$kodifikasi[$x];}
                        $rekomendasi                   = SubRekomendasi::find($request->$idsub[$x]);
                        $rekomendasi->subnomor         = $subnomor[$x];
                        $rekomendasi->subrekomendasi   = $subrekom[$x];
                        $rekomendasi->kodifikasi       = $kodif;
                        $rekomendasi->nilai            = $nilai[$x];
                        $rekomendasi->no               = ($x+1);
                        $rekomendasi->tanggal          = date('Y-m-d');
                        $rekomendasi->pic_id           = $picid[$x];
                        $rekomendasi->obyek_id         = $request->id;
                        $rekomendasi->rekomendasi_id   = $rek->id;
                        $rekomendasi->save();
                      }
                        
                        
                        
                        $pic                   = New HisPic;
                        $pic->subnomor         = $subnomor[$x];
                        $pic->obyek_id         = $request->id;
                        $pic->tanggal          = date('Y-m-d');
                        $pic->pic_id           = $picid[$x];
                        $pic->sts              = 0;
                        $pic->sub_rekomendasi_id = $rekomendasi->id;
                        $pic->save();
                   
                } 
           
          }  
        } 
       
        if($rekomendasi){
            return redirect('/lha/editrekomendasi/'.$request->id);
        }
       
     }


     public function update(Request $request){
        
       error_reporting(0);
        $rules = [
            'latar_belakang'     => 'required',
            'sasaran'     => 'required',
            'ruang_lingkup' => 'required',
            'pelaksanaan' => 'required',
            'penjelasan'              => 'required'
            
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('lha/edit/'.$request->id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $obyek                   =  Obyek::find($request->id);
        $obyek->sts_lha          =  3;
        $obyek->save();
        
        $data                             =LhaAudit::where('obyek_id',$request->id)->first();
        $data->latar_belakang             =$request->latar_belakang;
        $data->sasaran                    =$request->sasaran;
        $data->ruang_lingkup              =$request->ruang_lingkup;
        $data->pelaksanaan                =$request->pelaksanaan;
        $data->penutup                    =$request->penutup;
        $data->kesimpulan                 =$request->kesimpulan;
        $data->obyek_id                   =$request->id;
        $data->penjelasan                 =$request->penjelasan;
        $data->nik_create                 =Auth::user()->nik;
        $data->tanggal                    =date('Y-m-d h:i:s');
        $data->save();

        

        // ---- save rekomendasi lha
        //$delrekomendasi=Rekomendasi::where('lha_audit_id',$data->id)->delete();
        for ($x=0;$x < sizeof($request->rekomendasi6);$x++) {
            if($request->rekomendasi_id[$x]=='0'){
                $rekomendasi                   = New Rekomendasi;
                $rekomendasi->nomor            = $request->nomor[$x];
                $rekomendasi->head_rekomendasi = $request->head_rekomendasi[$x];
                $rekomendasi->judul            = $request->judul[$x];
                $rekomendasi->rekomendasi      = $request->rekomendasi6[$x];
                $rekomendasi->kodifikasi       = $request->kodifikasi[$x];
                $rekomendasi->lha_audit_id     = $data->id;
                $rekomendasi->obyek_id         = $request->id;
                $rekomendasi->save();
            }else{
                
                $rekomendasi                   = Rekomendasi::find($request->rekomendasi_id[$x]);
                $rekomendasi->nomor            = $request->nomor[$x];
                $rekomendasi->judul            = $request->judul[$x];
                $rekomendasi->head_rekomendasi = $request->head_rekomendasi[$x];
                $rekomendasi->rekomendasi      = $request->rekomendasi6[$x];
                $rekomendasi->kodifikasi       = $request->kodifikasi[$x];
                $rekomendasi->lha_audit_id     = $data->id;
                $rekomendasi->obyek_id         = $request->id;
                $rekomendasi->save();
            }
            
            
        } 

       
       
        if($data){
            return redirect('/lha/edit/'.$request->id);
        }
     }

     public function deleterekomendasi($id){
        //Tampilkan dan delete langkah kerja
           $subrek                      =  SubRekomendasi::find($id);
           $subrekomendasi              =  SubRekomendasi::where('id',$id)->delete();
           $subsubrekomendasi           =  SubSubRekomendasi::where('sub_rekomendasi_id',$id)->delete();
            return redirect('/lha/editrekomendasi/'.$subrek->obyek_id);
    }

    public function deletesub($id){
        //Tampilkan dan delete langkah kerja
           $subrek                   =  SubSubRekomendasi::find($id);
           $subrekomendasi           =  SubSubRekomendasi::where('id',$id)->delete();
            return redirect('/lha/formsub/'.$subrek->sub_rekomendasi_id);
    }

    public function surattugas($id)
    {
        //Tampilkan data obyek
            $oby = Obyek::where('id',$id)->first();
         
        //Tampilkan data obyek
            $nosurat = Obyek::orderBy('no_surattugas', 'asc')->max('no_surattugas');
            if($oby->sts_surattugas==0){
                $obyek                   =  Obyek::find($id);
                $obyek->no_surattugas    =  $nosurat+1;
                $obyek->sts_surattugas    = 1;
                $obyek->save();
            }
            
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit'])->where('id',$id)->first();    
         //Tampilkan data kategori
            $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
            //dd( $kategori);
         //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->get();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $tahapans1 = Tahapan::with(['jadwal'])->where('kategori',1)->get();
            $tahapans2 = Tahapan::with(['jadwal'])->where('kategori',2)->get();
            $tahapans3 = Tahapan::with(['jadwal'])->where('kategori',3)->get();
         return view( 'lha.surattugas', compact('tahapans1','tahapans2','tahapans3','kategori','obyekstim','obyeks','obyektims','ruanglingkups') );
    }

    public function memo($id)
    {
        //Tampilkan data obyek
            $oby = Obyek::where('id',$id)->first();
         
        //Tampilkan data obyek
            $nomemo = Obyek::orderBy('no_memo', 'asc')->max('no_memo');
            if($oby->sts_memo==0){
                $obyek                   =  Obyek::find($id);
                $obyek->no_memo    =  $nomemo+1;
                $obyek->sts_memo    = 1;
                $obyek->save();
            }
            $seminggu = array(
                'Sun' => 'Minggu',
                'Mon' => 'Senin',
                'Tue' => 'Selasa',
                'Wed' => 'Rabu',
                'Thu' => 'Kamis',
                'Fri' => 'Jumat',
                'Sat' => 'Sabtu'
            ); 
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit'])->where('id',$id)->first();    
         //Tampilkan data kategori
            $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
        //Tampilan menetahui
            $mengetahui = Mengetahui::where('obyek_id',$id)->where('sts',3)->orderBy('id','ASC')->get();
         //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->get();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $tahapans1 = Tahapan::with(['jadwal'])->where('kategori',1)->get();
            $tahapans2 = Tahapan::with(['jadwal'])->where('kategori',2)->get();
            $tahapans3 = Tahapan::with(['jadwal'])->where('kategori',3)->get();
         return view( 'lha.memo', compact('mengetahui','obyekstim','obyeks','seminggu') );
    }


    public function hapus_temuan(request $request)
    {
        $data=Rekomendasi::where('id',$request->id)->delete();
    }
    public function pengantar($id)
    {
        //Tampilkan data obyek
            $oby = Obyek::where('id',$id)->first();
         
        //Tampilkan data obyek
            $nopengantar = Obyek::orderBy('no_pengantar', 'asc')->max('no_pengantar');
            if($oby->sts_pengantar==0){
                $obyek                   =  Obyek::find($id);
                $obyek->no_pengantar     =  $nopengantar+1;
                $obyek->sts_pengantar    = 1;
                $obyek->tgl_sts_pengantar    = date('Y-m-d');
                $obyek->save();

                $rek              =Rekomendasi::where('obyek_id',$id)->first();
                $rek->sts_progres =1;
                $rek->save();

                $subrek              =SubRekomendasi::where('obyek_id',$id)->first();
                $subrek->sts_progres =1;
                $subrek->save();

                $subsubrek              =SubSubRekomendasi::where('obyek_id',$id)->first();
                $subsubrek->sts_progres =1;
                $subsubrek->save();

            }
            $seminggu = array(
                'Sun' => 'Minggu',
                'Mon' => 'Senin',
                'Tue' => 'Selasa',
                'Wed' => 'Rabu',
                'Thu' => 'Kamis',
                'Fri' => 'Jumat',
                'Sat' => 'Sabtu'
            ); 
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit'])->where('id',$id)->first();    
         //Tampilkan data kategori
            $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
        //Tampilan menetahui
            $mengetahui = Mengetahui::where('obyek_id',$id)->where('sts',4)->orderBy('id','ASC')->get();
         //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->get();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $tahapans1 = Tahapan::with(['jadwal'])->where('kategori',1)->get();
            $tahapans2 = Tahapan::with(['jadwal'])->where('kategori',2)->get();
            $tahapans3 = Tahapan::with(['jadwal'])->where('kategori',3)->get();
         return view( 'lha.pengantar', compact('mengetahui','obyekstim','obyeks','seminggu') );
    }

    public function slip($id)
    {
        //Tampilkan data obyek
            $oby = Obyek::where('id',$id)->first();
       
            $seminggu = array(
                'Sun' => 'Minggu',
                'Mon' => 'Senin',
                'Tue' => 'Selasa',
                'Wed' => 'Rabu',
                'Thu' => 'Kamis',
                'Fri' => 'Jumat',
                'Sat' => 'Sabtu'
            ); 
        //Tampilkan data obyek
         $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit','kategori_audit'])->where('id',$id)->first();    
         //Tampilkan data kategori
         $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
         $jadwal1=Jadwal::where('obyek_id',$id)->where('tahapan_id',8)->first();
         $jadwal2=Jadwal::where('obyek_id',$id)->where('tahapan_id',9)->first();
         $dt = Carbon::parse($obyeks->tgl_sts_pencatatan_substantive);
         $dt0 = Carbon::parse($obyeks-> tgl_revsetujui1);
         $selisih0 = ($dt->diffInDaysFiltered(function(Carbon $date) {
         return !$date->isWeekend();
         }, $dt0))+1;

         $dt1 = Carbon::parse($obyeks->tgl_revsetujui1);
         $dt2 = Carbon::parse($obyeks-> tgl_sts_setujui1);
         $selisih = ($dt1->diffInDaysFiltered(function(Carbon $date) {
         return !$date->isWeekend();
         }, $dt2))+1;

         $dt11 = Carbon::parse($obyeks->tgl_revsetujui2);
         $dt12 = Carbon::parse($obyeks-> tgl_sts_setujui2);
         $selisih1 = ($dt11->diffInDaysFiltered(function(Carbon $date) {
         return !$date->isWeekend();
         }, $dt12))+1;

         $dt21 = Carbon::parse($obyeks->tgl_revsetujui3);
         $dt22 = Carbon::parse($obyeks-> tgl_sts_setujui3);
         $selisih2 = ($dt21->diffInDaysFiltered(function(Carbon $date) {
         return !$date->isWeekend();
         }, $dt22))+1;

         $dt31 = Carbon::parse($obyeks->tgl_sts_setujui3);
         $dt32 = Carbon::parse($obyeks-> tgl_sts_pengantar);
         $selisih3 = ($dt31->diffInDaysFiltered(function(Carbon $date) {
         return !$date->isWeekend();
         }, $dt32))+1;
        
         $total=$selisih0+$selisih+$selisih1+$selisih2+$selisih3;
            $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
        //Tampilan menetahui
            $mengetahui = Mengetahui::where('obyek_id',$id)->where('sts',4)->orderBy('id','ASC')->get();
         //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->get();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $tahapans1 = Tahapan::with(['jadwal'])->where('kategori',1)->get();
            $tahapans2 = Tahapan::with(['jadwal'])->where('kategori',2)->get();
            $tahapans3 = Tahapan::with(['jadwal'])->where('kategori',3)->get();

                
                    
                
         return view( 'lha.slip', compact('kategori','jadwal1','jadwal2','total','selisih3','selisih0','mengetahui','obyekstim','obyeks','seminggu','selisih','selisih1','selisih2') );
    }


    public function tujuan($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        $mengetahui = Mengetahui::where('obyek_id',$id)->where('sts',3)->get();
        return view('lha.tujuan',compact('obyeks','mengetahui'));
    }

    public function tembusan($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        $mengetahui = Mengetahui::where('obyek_id',$id)->where('sts',4)->get();
        return view('lha.tembusan',compact('obyeks','mengetahui'));
    }
    public function inserttujuan(Request $request){
        //dd($request->jam_memo);
        // ---- delete megetahui
            $obyek                   =  Obyek::find($request->id);
            $obyek->tgl_undangan_memo  =  date('Y-m-d',strtotime($request->tanggal));
            $obyek->jam_undangan_memo  =  date('H:i:s',strtotime($request->jam_memo));
            $obyek->save();

            $delmengetahui  =   Mengetahui::where('obyek_id',$request->id)->where('sts',3)->delete();
            foreach ($request->kepada as $a) {
               if($a==''){

               }else{
                $kepada                = New Mengetahui;
                $kepada->kepada        = $a;
                $kepada->obyek_id      = $request->id;
                $kepada->sts           = 3;
                $kepada->save();
               }
            }
            
       
        if($kepada){
            return redirect('/lha/tujuan/'.$request->id);
        }
     }
    
     public function inserttembusan(Request $request){
            $obyek                =  Obyek::find($request->id);
            $obyek->kode_laporan  =  $request->kode_laporan;
            $obyek->no_kode_laporan  =  1;

            $obyek->save();
            $delmengetahui  =   Mengetahui::where('obyek_id',$request->id)->where('sts',4)->delete();
            foreach ($request->kepada as $a) {
               if($a==''){

               }else{
                $kepada                = New Mengetahui;
                $kepada->kepada        = $a;
                $kepada->obyek_id      = $request->id;
                $kepada->sts           = 4;
                $kepada->save();
               }
            }
            
       
        if($kepada){
            return redirect('/lha/tembusan/'.$request->id);
        }
     }

     public function hapustujuan($id){
        // ---- delete megetahui
            $tujuan     =  Mengetahui::where('id',$id)->first();
            $deltujuan  =   Mengetahui::where('id',$id)->delete();
            if($deltujuan){
                return redirect('/lha/tujuan/'.$tujuan->obyek_id);
            }
     }

     public function hapustembusan($id){
        // ---- delete megetahui
            $tujuan     =  Mengetahui::where('id',$id)->first();
            $deltujuan  =   Mengetahui::where('id',$id)->delete();
            if($deltujuan){
                return redirect('/lha/tembusan/'.$tujuan->obyek_id);
            }
     }

    public function updatemenyetujui(Request $request , $id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->menyetujui                 =$request->menyetujui;
        $data->sebagai_menyetujui         =$request->sebagai_menyetujui;
        $data->save();

        if($data){
            return redirect('/lha/surattugas/'.$id);
        }
     }

     public function setujui1($id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->sts_setujui1             =1;
        $data->tgl_sts_setujui1         =date('Y-m-d');
        $data->save();

        if($data){
            return redirect('/lha/');
        }
     }

     public function setujui2($id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->sts_setujui2             =1;
        $data->tgl_sts_setujui2         =date('Y-m-d');
        $data->save();

        if($data){
            return redirect('/lha/');
        }
     }

     public function setujui3($id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->sts_setujui3             =1;
        $data->tgl_sts_setujui3         =date('Y-m-d');
        $data->save();

        if($data){
            return redirect('/lha/');
        }
     }
     
     public function ijin1($id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->sts_setujui1             =2;
        $data->tgl_revsetujui1          =date('Y-m-d');
        $data->save();

        if($data){
            return redirect('/lha/');
        }
     }
     public function ijin2($id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->sts_setujui2             =2;
        $data->tgl_revsetujui2        =date('Y-m-d');
        $data->save();

        if($data){
            return redirect('/lha/');
        }
     }
     public function ijin3($id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->sts_setujui3             =2;
        $data->tgl_revsetujui3         =date('Y-m-d');
        $data->save();

        if($data){
            return redirect('/lha/');
        }
     }


     public function cetakdeskaudit($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        $tujuan = Tujuan::where('obyek_id',$id)->get();
        $deskaudit = DeskAudit::where('obyek_id',$id)->get();
        $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        $langkahkerja = LangkahKerja::where('obyek_id',$id)->get();
        $langkahnya = LangkahKerja::where('obyek_id',$id)->get();
        $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        return view('lha.deskaudit',compact('obyeks','tujuan','obyektims','deskaudit','langkahkerja','obyektims','langkahnya'));
    }

    public function cetakcompliance($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        $tujuan = Tujuan::where('obyek_id',$id)->get();
        $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        $compliance = ComplianceTest::where('obyek_id',$id)->get();
        $langkahkerja = LangkahCompliance::where('obyek_id',$id)->get();
        $langkahnya = LangkahCompliance::where('obyek_id',$id)->get();
        $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        return view('lha.compliance',compact('obyeks','tujuan','obyektims','compliance','langkahkerja','obyektims','langkahnya'));
    }

    public function cetaksubstantive($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        $tujuan = Tujuan::where('obyek_id',$id)->get();
        $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        $substantive = SubstantiveTest::where('obyek_id',$id)->get();
        $langkahkerja = LangkahSubstantive::where('obyek_id',$id)->get();
        $langkahnya = LangkahSubstantive::where('obyek_id',$id)->get();
        $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        return view('lha.substantive',compact('obyeks','tujuan','obyektims','substantive','langkahkerja','obyektims','langkahnya'));
    }

    public function filelha($id){
        $obyek_id=$id;
        $obyekfile=Obyek::where('id',$id)->first();
        $file=FileLangkahKerja::where('obyek_id',$id)->where('sts_catatan',0)->get();
        $files=FileLangkahKerja::where('obyek_id',$id)->where('sts_catatan',0)->get();
        return view( 'lha.file', compact('obyek_id','file','files','obyekfile') );

    }

    public function insertfile(request $request){
        $rules = [
            'filepencatatan'        => 'required|max:200|mimes:jpeg,bmp,png,gif,pdf,xlsx,doc,docx,ppt',
            'nama'                  => 'required',
        ];

        $customMessages = [
            'nama.required'                 => '- Nama File Harus diisi ',
            'filepencatatan.required'       => '- Upload file terlebih dahulu ',
            'filepencatatan.max'            => '- Maximal file 200kb',
            'filepencatatan.mimes'          => '- Format file hanya jpeg,bmp,png,gif,pdf,xlsx,doc,docx,ppt',
        ];
    
        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('lha/file/'.$request->obyek_id)
                        ->withErrors($validator)
                        ->withInput();
        }
        $type = $request->filepencatatan->extension();
        $fileed = $request->filepencatatan->storeAs('images',$request->nama.'.'.$type);


        $file               =New FileLangkahKerja;
        $file->relasi_id    =0;
        $file->obyek_id     =$request->obyek_id;
        $file->file         =$fileed;
        $file->type         =$type;
        $file->sts_catatan  =0;
        $file->save();
        
        return redirect('/lha/file/'.$request->obyek_id);

    }

    public function deletefile($kode,$id){
        //Tampilkan data langkahkerja
            $subs             =  FileLangkahKerja::where('id',$kode)->delete();
            return redirect('/lha/file/'.$id);
    }
    
}
