<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubstantiveTest extends Controller
{
    public function form($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'compliance.form', compact('obyeks') );
    }

    public function formrevisi($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'compliance.formrevisi', compact('obyeks') );
    }

    public function formrevisipencatatan($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'compliance.formrevisipencatatan', compact('obyeks') );
    }

    public function edit($id){
        $ComplianceTest = ComplianceTest::where('id',$id)->first();
        $langkahkerja = LangkahCompliance::where('compliance_test_id',$id)->get();
        return view( 'compliance.edit', compact('ComplianceTest','langkahkerja') );
    }

    public function obyeklist(){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->get();
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'compliance.index', compact('obyeks','obyektims','tujuans') );

       
    }

    public function index(){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts_pencatatan_deskaudit',1)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',3)->orderBy('id','DESC')->get();
        return view( 'compliance.index', compact('revisis','obyeks','obyektims','tujuans') );

       
    }

    public function indexapp(){
        //Tampilkan data obyek
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])
                ->whereIn('sts_compliance_id',[0,1,2])->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
       
        return view( 'compliance.indexapp', compact('obyeks','obyektims','tujuans') );

       
    }

    public function indexpencatatan(){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_compliance_id',1)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',3)->orderBy('id','DESC')->get();
        return view( 'compliance.pencatatan', compact('obyeks','obyektims','tujuans','revisis') );

       
    }

    public function indexpencatatanpeng(){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->whereIn('sts_pencatatan_compliance',[0,1,2])->where('sts_deskaudit_id',1)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
       
        return view( 'compliance.pencatatanpeng', compact('obyeks','obyektims','tujuans') );

       
    }

    public function indexpencatatanket(){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_compliance_id',1)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',3)->orderBy('id','DESC')->get();
        return view( 'compliance.pencatatanket', compact('obyeks','obyektims','tujuans','revisis') );

       
    }

   

    public function kirimcompliance($id){
        // ---- update Obyek
            $compliance                =  Obyek::find($id);
            $compliance->sts_compliance_id  =  0;
            $compliance->tgl_sts_compliance_id  =  date('Y-m-d');
            $compliance->save();
            if($compliance){
                return redirect('/compliance/');
            }
    }

    public function kirimpencatatancompliance($id){
        // ---- update Obyek
            $compliance                =  Obyek::find($id);
            $compliance->sts_pencatatan_compliance  =  0;
            $compliance->tgl_sts_pencatatan_compliance  =  date('Y-m-d');
            $compliance->save();
            if($compliance){
                return redirect('/compliance/pencatatanket');
            }
    }
    
    public function setujui($id){
        // ---- update Obyek
        $compliances                =  Obyek::find($id);
        $compliances->sts_compliance_id  =  1;
        $compliances->tgl_sts_compliance_id  =  date('Y-m-d');
        $compliances->save();
        if($compliances){
            return redirect('/compliance/indexapp');
        }
    }

    public function setujuipencatatan($id){
        // ---- update Obyek
        $compliance                            =  Obyek::find($id);
        $compliance->sts_pencatatan_compliance  =  1;
        $compliance->tgl_sts_pencatatan_compliance  =  date('Y-m-d');
        $compliance->save();
        if($compliance){
            return redirect('/compliance/pencatatanpeng');
        }
    }


    public function indexcompliance($id){
        //Tampilkan data obyek
            $compliances = ComplianceTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahCompliance::all();
           // dd($obyekstim);
        return view( 'compliance.detailcompliance', compact('compliances','langkahkerjas') );

       
    }

    public function detailpencatatan($id){
        //Tampilkan data obyek
            $obyeks = Obyek::where('id',$id)->first();
        //Tampilkan data deskaudit
            $ComplianceTest = ComplianceTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahCompliance::all();
           // dd($obyekstim);
       
        return view( 'compliance.detailpencatatan', compact('obyeks','ComplianceTest','langkahkerjas') );
    }

    public function detailpencatatanpeng($id){
        //Tampilkan data obyek
            $obyeks = Obyek::where('id',$id)->first();
        //Tampilkan data deskaudit
            $ComplianceTest = ComplianceTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahCompliance::all();
           // dd($obyekstim);
        return view( 'compliance.detailpencatatanpeng', compact('obyeks','ComplianceTest','langkahkerjas') );
    }

    public function indexpengawas($id){
        //Tampilkan data obyek
            $compliances = ComplianceTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahCompliance::all();
           // dd($obyekstim);
        return view( 'compliance.detailpengawas', compact('compliances','langkahkerjas') );

       
    }

    public function insert_revisi(Request $request , $id){
        $rules = [
            'keterangan'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('compliance/formrevisi')
                        ->withErrors($validator)
                        ->withInput();
        }
        // ---- update Obyek
            $obyek                    =  Obyek::find($id);
            $obyek->sts_compliance_id  =  2;
            $obyek->save();
        // ---- save Revisi
            $revisi                   =  New ApproveDeskaudit;
            $revisi->keterangan       =  $request->keterangan;
            $revisi->obyek_id         =  $request->id;
            $revisi->tanggal          =  date('Y-m-d');
            $revisi->sts              =  3;
            $revisi->save();

        if($revisi){
            return redirect('/compliance/indexapp');
        }
    }

    public function insert_revisipencatatan(Request $request , $id){
        $rules = [
            'keterangan'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('compliance/formrevisi')
                        ->withErrors($validator)
                        ->withInput();
        }
        // ---- update Obyek
            $obyek                    =  Obyek::find($id);
            $obyek->sts_pencatatan_compliance  =  2;
            $obyek->save();
        // ---- save Revisi
            $revisi                   =  New ApproveDeskaudit;
            $revisi->keterangan       =  $request->keterangan;
            $revisi->obyek_id         =  $request->id;
            $revisi->tanggal          =  date('Y-m-d');
            $revisi->sts              =  3;
            $revisi->save();

        if($revisi){
            return redirect('/compliance/pencatatanpeng');
        }
    }

    public function insert(Request $request){
        
       
        $rules = [
            'pokok_materi'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('compliance/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        // ---- save compliancetest
            $compliancetest                   = New ComplianceTest;;
            $compliancetest->pokok_materi     =  $request->pokok_materi;
            $compliancetest->obyek_id         =  $request->id;
            $compliancetest->tgl_compliance    =  date('Y-m-d');
            $compliancetest->sts              =  1;
            $compliancetest->save();

        // save Langkah Kerja
            for ($x=0;$x < sizeof($request->langkahkerja);$x++) {
                $langkahkerja                = New LangkahCompliance;
                $langkahkerja->obyek_id      = $request->id;
                $langkahkerja->urut          = $x;
                $langkahkerja->compliance_test_id  = $compliancetest->id;
                $langkahkerja->sts           = 1;
                $langkahkerja->langkah_kerja = $request->langkahkerja[$x];
                $langkahkerja->tgl_rencana   = date('Y-m-d',strtotime($request->tgl_rencana[$x]));
                $langkahkerja->save();
            }
        if($compliancetest){
            return redirect('/compliance/');
        }
     }

     public function update(Request $request , $id){
        
       
        $rules = [
            'pokok_materi'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('compliance/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        // ---- save deskaudit
            $ComplianceTest                   =  ComplianceTest::find($id);
            $ComplianceTest->pokok_materi     =  $request->pokok_materi;
            $ComplianceTest->save();

        // delete Langkah Kerja
            $dellangkah=LangkahCompliance::where('compliance_test_id',$id)->delete();
        // save Langkah Kerja
            
            for ($x=0;$x < sizeof($request->langkahkerja);$x++) {
                $langkahkerja                       = New LangkahCompliance;
                $langkahkerja->obyek_id             = $ComplianceTest->obyek_id;
                $langkahkerja->urut           = $x;
                $langkahkerja->compliance_test_id   = $request->id;
                $langkahkerja->sts                  = 1;
                $langkahkerja->langkah_kerja        = $request->langkahkerja[$x];
                $langkahkerja->tgl_rencana          = date('Y-m-d',strtotime($request->tgl_rencana[$x]));
                $langkahkerja->save();
            }
        if($ComplianceTest){
            return redirect('/compliance/detailcompliance/'.$ComplianceTest->obyek_id);
        }
     }

     public function insertpencatatan(Request $request , $id){
        
        // ---- save langkah kerja
        for ($x=0;$x < sizeof($request->id);$x++) {
            //dd($request->filelangkahkerja[$x]->store('images'));
            if (is_null($request->filelangkahkerja[$x])) {
                $langkahkerja                = New LangkahCompliance;
                $langkahkerja                = LangkahCompliance::find($id); 
                $langkahkerja->pencatatan    = $request->pencatatan[$x];
                $langkahkerja->tgl_aktual    =date('Y-m-d',strtotime($request->tgl_aktual[$x]));
                $langkahkerja->save();
                
            }else{
                
                
                //Upload file
                $extension = $request->filelangkahkerja[$x]->extension();
                if($extension=='jpg' || $extension=='jpeg' || $extension=='png' || $extension=='gif')
                    {
                        $filee                = $request->filelangkahkerja[$x]->store('images');
                    
                        if ($request->filelangkahkerja[$x]->isValid()) {
                            
                            $langkahkerja                = New LangkahCompliance;
                            $langkahkerja                = LangkahCompliance::find($id); 
                            $langkahkerja->pencatatan    = $request->pencatatan[$x];
                            $langkahkerja->files         = $filee;
                            $langkahkerja->tgl_aktual    =date('Y-m-d',strtotime($request->tgl_aktual[$x]));
                            $langkahkerja->save();
                    
                        }

                        
                    }
                else{
                        $rules = [
                            'xxxxx'   => 'required'
                        ];
                    
                        $customMessages = [
                            'required' => ':File harus berexitensi gambar jpg / jpeg / gif / png',
                            'max' => ':attribute Harus diisi'
                        ];
                    
                        
                
                        $validator = Validator::make($request->all(), $rules,$customMessages); 
                    
                        if ($validator->fails()) {
                            return redirect('/compliance/detailpencatatan/'.$request->obyek_id)
                                        ->withErrors($validator)
                                        ->withInput();
                        }
                        
                    }
            }
           
                return redirect('/compliance/detailpencatatan/'.$request->obyek_id);
                 
                            
         }
        
        
     }

     public function delete($id){
        //Tampilkan data langkahkerja
            $com           =  ComplianceTest::find($id);
            $compliance      =  ComplianceTest::where('id',$id)->delete();
            $langkahkerja   =  LangkahCompliance::where('compliance_test_id',$id)->delete();
            return redirect('/compliance/detailcompliance/'.$com->obyek_id);
     }

     public function delete_langkah($id){
        //Tampilkan dan delete langkah kerja
           $langkah                   =  LangkahCompliance::find($id);
           $langkahkerja=LangkahCompliance::where('id',$id)->delete();
            return redirect('/compliance/detailcompliance/'.$langkah->obyek_id);
    }
}
