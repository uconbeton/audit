<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Karyawan;
use App\UnitKerja;
use App\Obyek;
class Popcontroller extends Controller
{
    public function poppj($id){
         $items = Karyawan::with(['posisi','unit_kerja'])->where('posisi_id',$id)->get();
         return view('popup.popuppj',compact('items','id'));
        
    }

    public function popketuatim(){
        $posisi="1,2,3";
        $items = Karyawan::whereIn('posisi_id',['1','2','3','5'])->get();
        return view('popup.popketuatim',compact('items','id'));
       
   }

    public function popunitkerja($id){
        
        $items = UnitKerja::where('unit_id',$id)->get();
        return view('popup.popunitkerja',compact('items','id'));
       
    }

    public function tarikkaryawan(){
        $del=Karyawan::where('nik','!=','11797')->where('nik','!=','91000115')->where('nik','!=','91000113')->where('nik','!=','91000158')->delete();
                
        $json = file_get_contents('https://portal.krakatausteel.com/eos/api/structdisp/costCenter/111301');
        $item = json_decode($json,true);
        $unit=$item;
        foreach($unit as $o){
            $potng=$o['position_name'];
            $cekdata=Karyawan::where('nik',$o['personnel_no'])->count();
            
            if($potng=='Commercial Audit Manager'){$unt=1; }
            elseif($potng=='Head of Internal Audit'){$unt=1;}
            elseif($potng=='Sr. Auditor Operational'){$unt=2;}
            elseif($potng=='Sr. Auditor Commercial'){$unt=2;}
            elseif($potng=='Group Leader Research, Cont. & Dev Audit'){$unt=2;}
            //elseif($potng=='Sr. Auditor Operational'){$unt=3;}
            else{
                if($o['personnel_no']=='11797'){
                    $unt=5;
                }else{
                    $unt=3;
                }
                
            }

            if($o['personnel_no']=='11797'){
                
            }else{
                $key        =   New Karyawan;
                $key->nik  =   $o['personnel_no'];
                $key->password   =   Hash::make($o['personnel_no']);
                $key->nama =   $o['name'];
                $key->posisi_id  =   $unt;
                $key->unit_kerja_id  =   $o['cost_ctr'];
                $key->jabatan  =   $o['position_name'];
                $key->save();
            }
           
           
            
        }
    }

    public function tarik(){
        $json = file_get_contents('https://portal.krakatausteel.com/eos/api/organization');
        $item = json_decode($json,true);
        
        $unit=$item;
        //var_dump($item);
        foreach($unit as $o){
            $potng=explode(' ',$o['Objectname']);
            $cekdata=UnitKerja::where('kode',$o['ObjectID'])->count();
            
            if($potng[0]=='Subdit'){$unt=1; $pim='General Manager';}
            elseif($potng[0]=='Divisi'){$unt=3;$pim='Manager';}
            elseif($potng[0]=='Direktorat'){$unt=5;$pim='Direktur Utama';}
            elseif($potng[0]=='Dinas'){$unt=4;$pim='Supertintendant';}
            elseif($potng[0]=='PT'){$unt=6;$pim='Direktur Utama';}
            else{$unt=2;$pim='none';}

            if($cekdata>0){
                $key            =   UnitKerja::where('kode',$o['ObjectID'])->first();
                $key->kode      =   $o['ObjectID'];
                $key->nama      =   $o['Objectname'];
                $key->kode_unit =   $o['Objectabbr'];
                $key->unit_id   =   $unt;
                $key->pimpinan  =   $pim;
                $key->save();
                
            }else{
                $key            =   New UnitKerja;
                $key->kode      =   $o['ObjectID'];
                $key->nama      =   $o['Objectname'];
                $key->kode_unit =   $o['Objectabbr'];
                $key->unit_id   =   $unt;
                $key->pimpinan  =   $pim;
                $key->save();
            }
           
           
            
        }
        //$items = UnitKerja::where('unit_id',$id)->get();
        var_dump($item);
       
    }

    public function popobyek($id){
        //$obyeks = Obyek::with(['divisi'])->where('id',$id)->get();
        
        //Tampilkan data obyek
        $items = Obyek::all();
        return view('popup.popupobyek',compact('items'));
       
   }

    public function popobyekaudit(){
        $items = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',2)->get();
        return view('popup.popupobyek',compact('items'));
    }

    public function popanggota($id){
        
        $items = Karyawan::with(['posisi','unit_kerja'])->where('posisi_id',3)->get();
        return view('popup.popanggota',compact('items'));
       
   }

   public function popkaryawan(){
    $items = Karyawan::with(['posisi'])->get();
    return view('popup.popkaryawan',compact('items'));
   
   }
}
