<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karyawan;
use App\UnitKerja;
use App\Obyek;
use App\Unit;
use App\Temuan;
use App\KategoriAudit;
use App\Imports\TemuanImport;
use Maatwebsite\Excel\Facades\Excel;

class MasterController extends Controller
{
    public function session_tahun(Request $request){
        session(['tahun' => $request->tahun]);
        
        return redirect()->back();
    }

    public function subdit(){
        $items = UnitKerja::where('unit_id',1)->orderBy('id','DESC')->get();
        return view('master.subdit',compact('items'));
    }

    public function direktorat(){
        $items = UnitKerja::where('unit_id',5)->orderBy('id','DESC')->get();
        return view('master.direktorat',compact('items'));
    }

    public function dinas(){
        $items = UnitKerja::where('unit_id',4)->orderBy('id','DESC')->get();
        return view('master.dinas',compact('items'));
    }

    public function seksi(){
        $items = UnitKerja::where('unit_id',2)->orderBy('id','DESC')->get();
        return view('master.seksi',compact('items'));
    }

    public function perusahaan(){
        $items = UnitKerja::where('unit_id',6)->orderBy('id','DESC')->get();
        return view('master.perusahaan',compact('items'));
    }

    public function divisi(){
        $items = UnitKerja::where('unit_id',3)->orderBy('id','DESC')->get();
        return view('master.divisi',compact('items'));
    }

    public function kirimemail(request $request){
        $file = $request->filetemuan->store('images');
        echo $file;
    }

    public function formemail(){
        
        $items = Temuan::select('pic')->where('sts',0)->groupBy('pic')->get();
        $email=collect([]);
        $email2=collect([]);
        $cc=collect([]);
        $temuan=Temuan::all();
        foreach($items as $o){
            $em= Temuan::where('pic',$o->pic)->first();
            $email->push([
                'email' => $em->email
            ]);
            $email2->push([
                'email' => $em->email
            ]);
            $cc->push([
                'cc' => $em->cc
            ]);

            
        }
        //dd($temuan);
       return view('mailer.form',compact('items','email','email2','cc','temuan'));
    }
    public function his(){
        
        $items = Temuan::select('pic')->where('sts',1)->groupBy('pic')->get();
        $email=collect([]);
        $cc=collect([]);
        $temuan=Temuan::all();
        foreach($items as $o){
            $em= Temuan::where('pic',$o->pic)->first();
            $email->push([
                'email' => $em->email
            ]);
            $cc->push([
                'cc' => $em->cc
            ]);

            
        }
        //dd($temuan);
       return view('mailer.his',compact('items','email','cc','temuan'));
    }

    public function importtemuan(Request $request) 
	{
		$import = Excel::import(new TemuanImport, request()->file('fileupload'));
        
        if($import){
            return redirect('/broadcase');
           
        }else{
             return redirect('/broadcase');
           
            
        }
	}
}
