<?php

namespace App\Http\Controllers;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Validator;
use Carbon\Carbon;
use Session;
use App\Obyek;
use App\Tahapan;
use App\Sasaran;
use App\Risiko;
use App\RuangLingkup;
use App\ObyekTim;
use App\Unit;
use App\UnitKerja;
use App\Tujuan;
use App\Jadwal;
use App\Posisi;
use App\KategoriAudit;
use App\Karyawan;
class ObyekController extends Controller
{
    public function obyekform(){
        $items = Unit::all();
        $kategoris = KategoriAudit::all();
        $anggotas = Karyawan::with(['posisi','unit_kerja'])->whereIn('posisi_id',['3','2','5'])->get();
        return view('obyek.form',compact('items','anggotas','kategoris'));
    }

    public function obyeklist(){

        //dd(Auth::user()->nama);
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','kategori_audit'])->where('tahun_pkat',$tahun)->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'obyek.index', compact('obyeks','obyektims','tujuans') );

       
    }
    

    public function obyekedit($id)
    {
        //Tampilkan data unit    
            $items = Unit::all();
        //Tampilkan data Obyek 
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan data kategori
            $kategoris = KategoriAudit::all();
        //Tampilkan data anggota tim
            $anggotas = Karyawan::with(['posisi','unit_kerja'])->whereIn('posisi_id',['3','2','5'])->get();
        //Tampilkan data anggota lama 
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampil tujuan
            $tujuans = Tujuan::where('obyek_id',$id)->get();
            return view( 'obyek.edit',compact('obyeks','kategoris','obyekstim','anggotas','items','tujuans'));
    }

    public function detail($id)
    {
         //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit'])->where('id',$id)->first();
         //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
            // dd($obyekstim);
         //Tampilkan data Tujuan
             $tujuans = Tujuan::where('obyek_id',$id)->get();
        //Tampilkan data sasaran
            $sasaran = Sasaran::where('obyek_id',$id)
                               ->where('sts',1)->first();
            $subsasarans = Sasaran::where('obyek_id',$id)
                                  ->where('sts',2)->get();
        //Tampilkan Risiko
            $risiko = Risiko::where('obyek_id',$id)
                                ->where('sts',1)->first();
            $subrisikos = Risiko::where('obyek_id',$id)
                                ->where('sts',2)->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->where('sts',2)->get();
            $ruling = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->where('sts',1)->first();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $tahapans1 = Jadwal::with(['tahapan'])->where('kategori',1)->where('obyek_id',$id)->get();
              $selisih1=[];
                foreach($tahapans1 as $x=> $tahapans01){
                    $dt = $tahapans01->tgl_mulai;
                    $dt2 = $tahapans01->tgl_sampai;
                    $selisih = $dt->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                    }, $dt2);
        
                 array_push($selisih1,$selisih);   
                        $sel1[]=$selisih+1;
                 }
               $jumsel1=array_sum($sel1);

            $tahapans2 = Jadwal::with(['tahapan'])->whereIn('tahapan_id',['5','6','7'])->where('obyek_id',$id)->get();
                $selisih2=[];
                foreach($tahapans2 as $tahapans02){
                    $dt = $tahapans02->tgl_mulai;
                    $dt2 = $tahapans02->tgl_sampai;
                    $selisih = $dt->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                    }, $dt2);
        
                array_push($selisih2,$selisih);
                    $sel2[]=$selisih+1;   
                }
                $jumsel2=array_sum($sel2);

            $tahapans2a = Jadwal::with(['tahapan'])->whereIn('tahapan_id',['3','4'])->where('obyek_id',$id)->get();
                $selisih2a=[];
                foreach($tahapans2a as $tahapans02a){
                    $dt = $tahapans02a->tgl_mulai;
                    $dt2 = $tahapans02a->tgl_sampai;
                    $selisih = $dt->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                    }, $dt2);
        
                array_push($selisih2a,$selisih);
                    $sel2[]=$selisih+1;   
                }
                $jumsel2=array_sum($sel2);

            $tahapans3 = Jadwal::with(['tahapan'])->where('kategori',3)->where('obyek_id',$id)->get();
                $selisih3=[];
                foreach($tahapans3 as $tahapans03){
                    $dt = $tahapans03->tgl_mulai;
                    $dt2 = $tahapans03->tgl_sampai;
                    $selisih = $dt->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                    }, $dt2);
        
                array_push($selisih3,$selisih);
                $sel3[]=$selisih+1;   
                }
                $jumsel3=array_sum($sel3);
                $totalselisih=$jumsel1+$jumsel2+$jumsel3;
            $sampai = Jadwal::where('tahapan_id',9)->where('obyek_id',$id)->first();

       
            
        return view( 'obyek.detail', compact('tahapans2a','selisih2a','ruling','totalselisih','sampai','tahapans1','selisih1','tahapans2','selisih2','selisih3','tahapans3','obyekstim','obyeks','obyektims','tujuans','sasaran','subsasarans','risiko','subrisikos','ruanglingkups') );
    }

   
    public function memo($id)
    {
         //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit'])->where('id',$id)->first();
           // dd($obyeks);
         //Tampilkan data kategori
            $kategori = KategoriAudit::where('id',$obyeks->kategori_audit_id)->first();
            //dd( $kategori);
         //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->get();
            
         //Tampilkan data Tujuan
            $setujui=Karyawan::where('jabatan','Head of Internal Audit')->first();
         //Tampilkan data Tujuan
             $tujuans = Tujuan::where('obyek_id',$id)->get();
        //Tampilkan data sasaran
            $sasaran = Sasaran::where('obyek_id',$id)
                               ->where('sts',1)->first();
            $subsasarans = Sasaran::where('obyek_id',$id)
                                  ->where('sts',2)->get();
        //Tampilkan Risiko
            $risiko = Risiko::where('obyek_id',$id)
                                ->where('sts',1)->first();
            $subrisikos = Risiko::where('obyek_id',$id)
                                ->where('sts',2)->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->get();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $tahapans1 = Tahapan::with(['jadwal'])->where('kategori',1)->get();
            $tahapans2 = Tahapan::with(['jadwal'])->where('kategori',2)->get();
            $tahapans3 = Tahapan::with(['jadwal'])->where('kategori',3)->get();
        return view( 'obyek.memo', compact('setujui','tahapans1','tahapans2','tahapans3','kategori','obyekstim','obyeks','obyektims','tujuans','sasaran','subsasarans','risiko','subrisikos','ruanglingkups') );
        
        //Untuk PDF
        //  $pdf = PDF::loadView('obyek.sr', compact('setujui','tahapans1','tahapans2','tahapans3','kategori','obyekstim','obyeks','obyektims','tujuans','sasaran','subsasarans','risiko','subrisikos','ruanglingkups') );
        //  return $pdf->download('surat_tugas-' . $obyeks->nama . '.pdf');
        }
    

    public function insert(Request $request){
        
       
        $rules = [
            'nama'              => 'required|max:255',
            'unit_kerja_id'     => 'required',
            'tahun_audit'           => 'required',
            'unit_id'           => 'required',
            'kategori_audit_id' => 'required',
            'nomor_surat_tugas' => 'required',
            'kode'              => 'required',
            'ttd'              => 'required',
            'nik_pengawas'      => 'required',
            'nik_ketua_tim'     => 'required'
            
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('obyek/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        $nikanggota='';
        foreach ($request->anggota as $a) {
            $nikanggota.= $a.',';
            
        }
        $urut=Obyek::where('ttd','!=',2)->where('unit_id','!=',6)->count();
        if($request->ttd==2 || $request->unit_id==6 ){
            $urutan=0;
        }else{
            $urutan=$urut;
        }
        $data   = New Obyek;
        $data->nama             =$request->nama;
        $data->unit_kerja_id    =$request->unit_kerja_id;
        $data->unit_id          =$request->unit_id;
        $data->nomor_surat_tugas=$request->nomor_surat_tugas;
        $data->ttd              =$request->ttd;
        $data->kode             =$request->kode;
        $data->tahun_pkat      =$request->tahun_audit;
        $data->waktu_audit      =$request->waktu_audit;
        $data->urutan           =($urutan +1);
        $data->sts              =1;
        $data->nik_anggota      =$nikanggota;
        $data->sts_pengantar    =0;
        $data->sts_memo         =0;
        $data->kategori_audit_id=$request->kategori_audit_id;
        $data->nik_pengawas     =$request->nik_pengawas;
        $data->nik_ketua_tim    =$request->nik_ketua_tim;
        $data->waktu            =date('Y-m-d');
        $data->save();

        

        // ---- save tujuan
        $tujs = new \DOMDocument();
        $tujuans = $request->tujuan;
        @$tujs->loadHTML($tujuans);
        $tujuan = $tujs->getElementsByTagName('li');
        $counter = 0;

        foreach ($tujuan as $key => $value) {
            $key            =   New Tujuan;
            $key->obyek_id  =   $data->id;
            $key->tujuan    =   $value->nodeValue;
            $key->save();
        }

       
        //-- save anggota
        // if(is_null($request->anggota)){}
        foreach ($request->anggota as $a) {
            $anggota            = New ObyekTim;
            $anggota->nik       = $a;
            $anggota->posisi_id = 3;
            $anggota->obyek_id  = $data->id;
            $anggota->save();
        }
        if($data){
            return redirect('/obyek');
        }
     }




     public function updatemenyetujui(Request $request , $id){
        // -- save obyek
        $data   = New Obyek;
        $data =Obyek::find($id);
        $data->menyetujui                 =$request->menyetujui;
        $data->sebagai_menyetujui        =$request->sebagai_menyetujui;
        $data->save();

        if($data){
            return redirect('/obyek/memo/'.$id);
        }
     }
     public function update(Request $request , $id){
        
        
        $rules = [
            'nama'              => 'required|max:255',
            'unit_kerja_id'     => 'required',
            'tahun_audit'       => 'required',
            'ttd'               => 'required',
            'unit_id'           => 'required',
            'kategori_audit_id' => 'required',
            'kode'              => 'required',
            'nik_pengawas'      => 'required',
            'nik_ketua_tim'     => 'required'
            
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        $this->validate($request, $rules, $customMessages);

        $nikanggota='';
        if (is_null($request->anggota)) {
            foreach ($request->anggotalama as $a) {
                $nikanggota.= $a.',';
                
            }
        }else{
            foreach ($request->anggota as $a) {
                $nikanggota.= $a.',';
                
            }
        }
        // -- save obyek
            //$urutan=Obyek::where('ttd','!=',2)->where('unit_id','!=',6)->count();
            $data   = New Obyek;
            $data =Obyek::find($id);
            $data->nama                 =$request->nama;
            $data->ttd                  =$request->ttd;
            $data->unit_kerja_id        =$request->unit_kerja_id;
            $data->nomor_surat_tugas    =$request->nomor_surat_tugas;
            $data->unit_id              =$request->unit_id;
            $data->kode                 =$request->kode;
            $data->tahun_pkat          =$request->tahun_audit;
            $data->nik_anggota          =$nikanggota;
            $data->waktu_audit          =$request->waktu_audit;
            $data->kategori_audit_id    =$request->kategori_audit_id;
            $data->nik_pengawas         =$request->nik_pengawas;
            $data->nik_ketua_tim        =$request->nik_ketua_tim;
            $data->waktu                =date('Y-m-d');
            $data->save();
       
        

            //-- delete Tujuan
            $deltujuan=Tujuan::where('obyek_id',$id)->delete();

            $tujs = new \DOMDocument();
            $tujuans = $request->tujuan;
            @$tujs->loadHTML($tujuans);
            $tujuan = $tujs->getElementsByTagName('li');
            $counter = 0;
            
            //-- save Tujuan
            foreach ($tujuan as $key => $value) {
                $key            =   New Tujuan;
                $key->obyek_id  =   $data->id;
                $key->tujuan    =   $value->nodeValue;
                $key->save();
            }

        
            //delete anggota
            
            //-- save anggota
            if (is_null($request->anggota)) {
                
            } else {
                $jumanggota = count($request->anggota);
                foreach ($request->anggota as $a) {
                    $anggota            = New ObyekTim;
                    $anggota->nik       = $a;
                    $anggota->posisi_id = 3;
                    $anggota->obyek_id  = $data->id;
                    $anggota->save();
                }

            }
            
            
        
        if($data){
            return redirect('/obyek');
        }
     }

     public function delete($id){
        //delete obyek
        $obyek=Obyek::where('id',$id)->delete();
        $obyektim=ObyekTim::where('obyek_id',$id)->delete();
        $tujuan=Tujuan::where('obyek_id',$id)->delete();
        if($obyek){
            return redirect('/obyek');
        }
     }

     public function deltim($id){
        //delete obyek
        $obyektim=ObyekTim::where('id',$id)->first();
        $obyektimdel=ObyekTim::where('id',$id)->delete();
        if($obyektimdel){
            return redirect('/obyek/edit/'.$obyektim->obyek_id);
        }
     }

    
    
}
