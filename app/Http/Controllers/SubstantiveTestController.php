<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Obyek;
use App\Unit;
use App\UnitKerja;
use App\Karyawan;
use App\ObyekTim;
use App\ApproveDeskaudit;
use App\LangkahKerja;
use App\DeskAudit;
use App\SubstantiveTest;
use App\LangkahSubstantive;
use App\Tujuan;
use App\FileLangkahKerja;
class SubstantiveTestController extends Controller
{
    public function form($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'substantive.form', compact('obyeks') );
    }

    public function formrevisi($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'substantive.formrevisi', compact('obyeks') );
    }

    public function formrevisipencatatan($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'substantive.formrevisipencatatan', compact('obyeks') );
    }

    public function edit($id){
        $SubstantiveTest = SubstantiveTest::where('id',$id)->first();
        $langkahkerja = LangkahSubstantive::where('substantive_test_id',$id)->get();
        return view( 'substantive.edit', compact('SubstantiveTest','langkahkerja') );
    }

    public function obyeklist(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->get();
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'compliance.index', compact('obyeks','obyektims','tujuans') );

    
    }

    public function index(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts_pencatatan_compliance',1)->where('nik_ketua_tim',Auth::user()->nik)->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',5)->orderBy('id','DESC')->get();
        return view( 'substantive.index', compact('revisis','obyeks','obyektims','tujuans') );

    
    }

    public function indexapp(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])
                ->whereIn('sts_substantive_id',[0,1,2])->where('nik_pengawas','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
    
        return view( 'substantive.indexapp', compact('obyeks','obyektims','tujuans') );

    
    }

    public function indexadmin(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_substantive_id',1)->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',6)->orderBy('id','DESC')->get();
        return view( 'substantive.indexadmin', compact('obyeks','obyektims','tujuans','revisis') );

    
    }

    public function indexpencatatan(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_substantive_id',1)->where('nik_anggota','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',6)->orderBy('id','DESC')->get();
        return view( 'substantive.pencatatan', compact('obyeks','obyektims','tujuans','revisis') );

    
    }

    public function indexpencatatanpeng(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->whereIn('sts_pencatatan_substantive',[0,1,2])->where('sts_deskaudit_id',1)->where('nik_pengawas','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
    
        return view( 'substantive.pencatatanpeng', compact('obyeks','obyektims','tujuans') );

    
    }

    public function indexpencatatanket(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_substantive_id',1)->where('nik_ketua_tim','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
        // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',6)->orderBy('id','DESC')->get();
        return view( 'substantive.pencatatanket', compact('obyeks','obyektims','tujuans','revisis') );

    
    }



    public function kirimsubstantive($id){
        // ---- update Obyek
            $substantive                =  Obyek::find($id);
            $substantive->sts_substantive_id  =  0;
            $substantive->tgl_sts_substantive_id  =  date('Y-m-d');
            $substantive->save();
            if($substantive){
                return redirect('/substantive/');
            }
    }

    public function kirimpencatatansubstantive($id){
        // ---- update Obyek
            $substantive                =  Obyek::find($id);
            $substantive->sts_pencatatan_substantive  =  0;
            $substantive->tgl_sts_pencatatan_substantive  =  date('Y-m-d');
            $substantive->save();
            if($substantive){
                return redirect('/substantive/pencatatanket');
            }
    }
    
    public function setujui($id){
        // ---- update Obyek
        $substantive                =  Obyek::find($id);
        $substantive->sts_substantive_id  =  1;
        $substantive->tgl_sts_substantive_id  =  date('Y-m-d');
        $substantive->save();
        if($substantive){
            return redirect('/substantive/indexapp');
        }
    }

    public function setujuipencatatan($id){
        // ---- update Obyek
        $compliance                            =  Obyek::find($id);
        $compliance->sts_pencatatan_substantive  =  1;
        $compliance->tgl_sts_pencatatan_substantive  =  date('Y-m-d');
        $compliance->save();
        if($compliance){
            return redirect('/substantive/pencatatanpeng');
        }
    }


    public function indexsubstantive($id){
        //Tampilkan data obyek
            $substantive = SubstantiveTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahSubstantive::all();
        // dd($obyekstim);
        return view( 'substantive.detailsubstantive', compact('substantive','langkahkerjas') );

    
    }

    public function detailpencatatan($id){
        //Tampilkan data obyek
            $obyeks = Obyek::where('id',$id)->first();
        //Tampilkan data deskaudit
            $SubstantiveTest = SubstantiveTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahSubstantive::all();
        // dd($obyekstim);
    
        return view( 'substantive.detailpencatatan', compact('obyeks','SubstantiveTest','langkahkerjas') );
    }

    public function detailpencatatanpeng($id){
        //Tampilkan data obyek
            $obyeks = Obyek::where('id',$id)->first();
        //Tampilkan data deskaudit
            $SubstantiveTest = SubstantiveTest::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahSubstantive::all();
        // dd($obyekstim);
        return view( 'substantive.detailpencatatanpeng', compact('obyeks','SubstantiveTest','langkahkerjas') );
    }

    public function indexpengawas($id){
        //Tampilkan data saubstantive
            $substantive = SubstantiveTest::where('Obyek_id',$id)->get();
        //Tampilkan data langkahkerja
            $langkahkerjas  = LangkahSubstantive::all();
        // dd($obyekstim);
        return view( 'substantive.detailpengawas', compact('substantive','langkahkerjas') );

    
    }

    public function insert_revisi(Request $request , $id){
        $rules = [
            'keterangan'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules,$customMessages); 
    
        if ($validator->fails()) {
            return redirect('substantive/formrevisi')
                        ->withErrors($validator)
                        ->withInput();
        }
        // ---- update Obyek
            $obyek                    =  Obyek::find($id);
            $obyek->sts_substantive_id  =  2;
            $obyek->save();
        // ---- save Revisi
            $revisi                   =  New ApproveDeskaudit;
            $revisi->keterangan       =  $request->keterangan;
            $revisi->obyek_id         =  $request->id;
            $revisi->tanggal          =  date('Y-m-d');
            $revisi->sts              =  5;
            $revisi->save();

        if($revisi){
            return redirect('/substantive/indexapp');
        }
    }

    public function insert_revisipencatatan(Request $request , $id){
        $rules = [
            'keterangan'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules,$customMessages); 
    
        if ($validator->fails()) {
            return redirect('substantive/formrevisi')
                        ->withErrors($validator)
                        ->withInput();
        }
        // ---- update Obyek
            $obyek                    =  Obyek::find($id);
            $obyek->sts_pencatatan_substantive  =  2;
            $obyek->save();
        // ---- save Revisi
            $revisi                   =  New ApproveDeskaudit;
            $revisi->keterangan       =  $request->keterangan;
            $revisi->obyek_id         =  $request->id;
            $revisi->tanggal          =  date('Y-m-d');
            $revisi->sts              =  6;
            $revisi->save();

        if($revisi){
            return redirect('/substantive/pencatatanpeng');
        }
    }

    public function insert(Request $request){
        
    
        $rules = [
            'pokok_materi'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
    
        if ($validator->fails()) {
            return redirect('substantive/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        // ---- save compliancetest
            $SubstantiveTest                   = New SubstantiveTest;;
            $SubstantiveTest->pokok_materi     =  $request->pokok_materi;
            $SubstantiveTest->obyek_id         =  $request->id;
            $SubstantiveTest->tgl_substantive    =  date('Y-m-d');
            $SubstantiveTest->sts              =  1;
            $SubstantiveTest->save();

        // save Langkah Kerja
            for ($x=0;$x < sizeof($request->langkahkerja);$x++) {
                $langkahkerja                = New LangkahSubstantive;
                $langkahkerja->obyek_id      = $request->id;
                $langkahkerja->urut          = $x;
                $langkahkerja->substantive_test_id  = $SubstantiveTest->id;
                $langkahkerja->sts           = 1;
                $langkahkerja->langkah_kerja = $request->langkahkerja[$x];
                $langkahkerja->tgl_rencana   = date('Y-m-d',strtotime($request->tgl_rencana[$x]));
                $langkahkerja->save();
            }
        if($SubstantiveTest){
            return redirect('/substantive/');
        }
    }

    public function update(Request $request , $id){
        
    
        $rules = [
            'pokok_materi'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
    
        if ($validator->fails()) {
            return redirect('substantive/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        // ---- save substantive
            $SubstantiveTest                   =  SubstantiveTest::find($id);
            $SubstantiveTest->pokok_materi     =  $request->pokok_materi;
            $SubstantiveTest->save();

        // delete Langkah Kerja
            $dellangkah=LangkahSubstantive::where('substantive_test_id',$id)->delete();
        // save Langkah Kerja
            
            for ($x=0;$x < sizeof($request->langkahkerja);$x++) {
                $langkahkerja                       = New LangkahSubstantive;
                $langkahkerja->obyek_id             = $SubstantiveTest->obyek_id;
                $langkahkerja->substantive_test_id  = $request->id;
                $langkahkerja->urut                 = $x;
                $langkahkerja->sts                  = 1;
                $langkahkerja->langkah_kerja        = $request->langkahkerja[$x];
                $langkahkerja->tgl_rencana          = date('Y-m-d',strtotime($request->tgl_rencana[$x]));
                $langkahkerja->save();
            }
        if($SubstantiveTest){
            return redirect('/substantive/detailsubstantive/'.$SubstantiveTest->obyek_id);
        }
    }

    public function filesubstantive($id,$id1){
        $id=$id;
        $obyek_id=$id1;
        $obyekfile=Obyek::where('id',$id1)->first();
        $file=FileLangkahKerja::where('obyek_id',$obyek_id)->where('relasi_id',$id)->where('sts_catatan',3)->get();
        $files=FileLangkahKerja::where('obyek_id',$obyek_id)->where('relasi_id',$id)->where('sts_catatan',3)->get();
        return view( 'substantive.file', compact('id','obyek_id','file','files','obyekfile') );

    }

    public function insertfile(request $request){
        $rules = [
            'filepencatatan'        => 'required|max:200|mimes:jpeg,bmp,png,gif,pdf,xlsx,doc,docx,ppt',
            'nama'                  => 'required',
        ];

        $customMessages = [
            'nama.required'                 => '- Nama File Harus diisi ',
            'filepencatatan.required'       => '- Upload file terlebih dahulu ',
            'filepencatatan.max'            => '- Maximal file 200kb',
            'filepencatatan.mimes'          => '- Format file hanya jpeg,bmp,png,gif,pdf,xlsx,doc,docx,ppt',
        ];
    
        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('substantive/file/'.$request->id.'/'.$request->obyek_id)
                        ->withErrors($validator)
                        ->withInput();
        }
        $type = $request->filepencatatan->extension();
        $fileed = $request->filepencatatan->storeAs('images',$request->nama.'.'.$type);


        $file               =New FileLangkahKerja;
        $file->relasi_id    =$request->id;
        $file->obyek_id     =$request->obyek_id;
        $file->file         =$fileed;
        $file->type         =$type;
        $file->sts_catatan  =3;
        $file->save();
        
        return redirect('/substantive/file/'.$request->id.'/'.$request->obyek_id);

    }

    public function deletefile($kode,$id,$id1){
        //Tampilkan data langkahkerja
            $subs             =  FileLangkahKerja::where('id',$kode)->delete();
            return redirect('/substantive/file/'.$id.'/'.$id1);
    }


    public function insertpencatatan(Request $request , $id){
        
        // ---- save langkah kerja
        for ($x=0;$x < sizeof($request->id);$x++) {
            
                $langkahkerja                = New LangkahSubstantive;
                $langkahkerja                = LangkahSubstantive::find($id); 
                $langkahkerja->pencatatan    = $request->pencatatan[$x];
                $langkahkerja->tgl_aktual    =date('Y-m-d',strtotime($request->tgl_aktual[$x]));
                $langkahkerja->save();
                
           
        
                return redirect('/substantive/detailpencatatan/'.$request->obyek_id);
                
                            
        }
        
        
    }

    public function delete($id){
        //Tampilkan data langkahkerja
            $subs             =  SubstantiveTest::find($id);
            $substantive      =  SubstantiveTest::where('id',$id)->delete();
            $langkahkerja     =  LangkahSubstantive::where('compliance_test_id',$id)->delete();
            return redirect('/substantive/detailsubstantive/'.$subs->obyek_id);
    }

    public function delete_langkah($id){
        //Tampilkan dan delete langkah kerja
        $langkah                   =  LangkahSubstantive::find($id);
        $langkahkerja=LangkahSubstantive::where('id',$id)->delete();
            return redirect('/substantive/detailsubstantive/'.$langkah->obyek_id);
    }
}
