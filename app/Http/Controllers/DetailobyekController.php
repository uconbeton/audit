<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Validator;
use Carbon\Carbon;
use Session;
use App\Obyek;
use App\Tahapan;
use App\Sasaran;
use App\Risiko;
use App\RuangLingkup;
use App\Mengetahui;
use App\ObyekTim;
use App\Unit;
use App\UnitKerja;
use App\Tujuan;
use App\Jadwal;
use App\Posisi;
use App\KategoriAudit;
use App\Karyawan;
class DetailobyekController extends Controller
{
    public function index(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            if(Auth::user()->posisi_id==1){
                $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->where('nik_pengawas',Auth::user()->nik)->orderBy('id','DESC')->paginate(10);
            }else{   
                $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
            }
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'detailobyek.index', compact('obyeks','obyektims','tujuans') );

       
    }

    public function form($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        
        $tahapans = Tahapan::all();
        
        $unitkerjas = UnitKerja::all();
        return view('detailobyek.form',compact('unitkerjas','tahapans','obyeks'));
    }

    public function formtembusan($id){
        $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        
        $tahapans = Tahapan::all();
        
        $unitkerjas = UnitKerja::all();
        return view('detailobyek.formtembusan',compact('unitkerjas','tahapans','obyeks'));
    }

    public function formedit($id){
        //Tampilkan Obyek
            $obyeks = Obyek::with(['unit','unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        //Tampilkan Sasaran dan subsasaran    
            $sasaran = Sasaran::where('obyek_id',$id)
                            ->where('sts',1)->first();
            $subsasarans = Sasaran::where('obyek_id',$id)
                                  ->where('sts',2)->get();
       
        //Tampilkan tahapan  
            $jadwals = Jadwal::with(['tahapan'])->where('obyek_id',$id)->get();
       
        //Tampilkan tahapan
            $risiko = Risiko::where('obyek_id',$id)
                            ->where('sts',1)->first();
            $subrisikos = Risiko::where('obyek_id',$id)
                            ->where('sts',2)->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->where('sts',2)->get();
            $dasarruanglingkups = RuangLingkup::where('obyek_id',$id)->where('sts',1)->first();
                            
            $unitkerjas = UnitKerja::all();
            return view('detailobyek.edit',compact('dasarruanglingkups','unitkerjas','jadwals','obyeks','sasaran','subsasarans','risiko','subrisikos','ruanglingkups'));
    }

    public function insert(Request $request){
        
       
        $rules = [
            'sasaran'              => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('detobyek/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        
        // ---- save obyek
            $obyek          =   New Obyek;
            $obyek          =   Obyek::find($request->id);
            $obyek->sts     =   2;
            $obyek->save();
        
        // ---- save Sasaran
            $upsasaran                   = New Sasaran;
            $upsasaran->sasaran          =  $request->sasaran;
            $upsasaran->obyek_id         =  $request->id;
            $upsasaran->sts              =  1;
            $upsasaran->save();

            $sas = new \DOMDocument();
            $sasaran = $request->sub_sasaran;
            @$sas->loadHTML($sasaran);
            $sasaran = $sas->getElementsByTagName('li');
            $counter = 0;

            foreach ($sasaran as $key => $value) {
                $key            =   New Sasaran;
                $key->obyek_id  =   $request->id;
                $key->sts       =   2;
                $key->sasaran    =  $value->nodeValue;
                $key->save();
            }
        
        
        // ---- save Risiko

            $uprisiko                   =  New Risiko;
            $uprisiko->risiko           =  $request->risiko;
            $uprisiko->obyek_id         =  $request->id;
            $uprisiko->sts              =  1;
            $uprisiko->save();

            $ris = new \DOMDocument();
            $risiko = $request->sub_risiko;
            @$ris->loadHTML($risiko);
            $risiko = $ris->getElementsByTagName('li');
            $counter = 0;

            foreach ($risiko as $key => $value) {
                $key            =   New Risiko;
                $key->obyek_id  =   $request->id;
                $key->sts       =   2;
                $key->risiko    =  $value->nodeValue;
                $key->save();
            }

       
        // save RuangLingkup
                $ruangLingkup                = New RuangLingkup;
                $ruangLingkup->ruanglingkup = $request->ruanglingkup;
                $ruangLingkup->obyek_id      = $request->id;
                $ruangLingkup->sts           = 1;
                $ruangLingkup->save();
            foreach ($request->sub_ruanglingkup as $a) {
                $sub_ruangLingkup                = New RuangLingkup;
                $sub_ruangLingkup->unit_kerja_id = $a;
                $sub_ruangLingkup->obyek_id      = $request->id;
                $sub_ruangLingkup->sts           = 2;
                $sub_ruangLingkup->ruanglingkup  = '-';
                $sub_ruangLingkup->save();
            }
            
        // save Jadwal
            for ($x=0;$x < sizeof($request->tahapan_id);$x++) {
                $jadwal                   = New Jadwal;
                $jadwal->tahapan_id       = $request->tahapan_id[$x];
                $jadwal->kategori         = $request->kategori[$x];
                $jadwal->tgl_mulai        = date('Y-m-d',strtotime($request->tgl_mulai[$x]));
                $jadwal->tgl_sampai       = date('Y-m-d',strtotime($request->tgl_sampai[$x]));
                $jadwal->obyek_id         = $request->id;
                $jadwal->save();
            }
        if($upsasaran){
            return redirect('/detobyek/');
        }
     }

     public function inserttembusan(Request $request , $id){
        // ---- delete megetahui
            $delmengetahui  =   Mengetahui::where('obyek_id',$id)->delete();
        // ---- save megetahui
            $kepadyth            =   New Mengetahui;
            $kepadyth->obyek_id  =   $request->id;
            $kepadyth->sts       =   1;
            $kepadyth->kepada    =   $request->kepada_yth;
            $kepadyth->save(); 

            foreach ($request->kepada as $a) {
                $kepada                = New Mengetahui;
                $kepada->kepada        = $a;
                $kepada->obyek_id      = $request->id;
                $kepada->sts           = 2;
                $kepada->save();
            }
            
       
        if($kepadyth){
            return redirect('/detobyek/memodinas/'.$request->id);
        }
     }




     public function memodinas($id)
    {
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim','unit'])->where('id',$id)->first();
        //Tampilkan data obyektim
             $obyektims  = ObyekTim::with(['karyawan'])->get();
            // dd($obyekstim);
        //Tampilkan data Tujuan
             $tujuans = Tujuan::where('obyek_id',$id)->get();
        //Tampilkan data mengetahui
            $mengetahui = Mengetahui::where('obyek_id',$id)->
                                       where('sts',1)->first();
            $tembusan    = Mengetahui::where('obyek_id',$id)->
                                       where('sts',2)->get();
        //Tampilkan data sasaran
            $sasaran = Sasaran::where('obyek_id',$id)
                               ->where('sts',1)->first();
            $subsasarans = Sasaran::where('obyek_id',$id)
                                  ->where('sts',2)->get();
        //Tampilkan Risiko
            $risiko = Risiko::where('obyek_id',$id)
                                ->where('sts',1)->first();
            $subrisikos = Risiko::where('obyek_id',$id)
                                ->where('sts',2)->get();
        //Tampilkan ruanglingkup
            $ruanglingkups = RuangLingkup::with(['unit_kerja'])->where('obyek_id',$id)->get();
        //Tampilkan data anggota tim
            $obyekstim  = ObyekTim::with(['karyawan'])->where('obyek_id',$id)->get();
        //Tampilkan tahapan  
            $mulai = Jadwal::where('tahapan_id',1)->where('obyek_id',$id)->first();
            $sampai = Jadwal::where('tahapan_id',9)->where('obyek_id',$id)->first();
         return view( 'detailobyek.memodinas', compact('mengetahui','tembusan','mulai','sampai','obyekstim','obyeks','obyektims','tujuans','sasaran','subsasarans','risiko','subrisikos','ruanglingkups') );
    }
    
     public function approve($id){
        $obyek        = New Obyek;
        $obyek        = Obyek::where('id',$id)->first();

        $obyek->sts   = 3;
        $obyek->tgl_setujui   = date('Y-m-d');
        $obyek->save();
        
        if($obyek){
            return redirect('/detobyek/');
        }
     }


     public function update(Request $request , $id){
        
        //-- Proses  validation-----------------------------------------------------------------------------------
            $rules = [
                'sasaran'     => 'required'
                
            ];
        
            $customMessages = [
                'required' => ':attribute Harus diisi',
                'max' => ':attribute Harus diisi'
            ];
        
            $this->validate($request, $rules, $customMessages);

        //-- end  validation-----------------------------------------------------------------------------------
            
        //-- Proses  sasaran-----------------------------------------------------------------------------------
            //--update sasaran
            $sasar            = New Sasaran;
            $sasar            = Sasaran::where('obyek_id',$id)
                                            ->where('sts',1)->first();

            $sasar->sasaran   = $request->sasaran;
            $sasar->save();
            
        //-- delete sub sasaran-----------------------------------------------------------------------------------
            $delsasaran=Sasaran::where('obyek_id',$id)
                                ->where('sts','2')->delete();

            $sasr = new \DOMDocument();
            $subsasaran = $request->subsasaran;
            @$sasr->loadHTML($subsasaran);
            $subsasaran = $sasr->getElementsByTagName('li');
            $counter = 0;
            
        //-- save Sasaran
            foreach ($subsasaran as $key => $value) {
                $key            =   New Sasaran;
                $key->obyek_id  =   $request->id;
                $key->sts       =   2;
                $key->sasaran    =   $value->nodeValue;
                $key->save();
            }
        //--end sasaran--------------------------------------------------------------------------

        //-- Proses  Risiko-----------------------------------------------------------------------------------
            //--update risiko
            $risiko            = New Risiko;
            $risiko            = Risiko::where('obyek_id',$id)
                                         ->where('sts',1)->first();

            $risiko->risiko   = $request->risiko;
            $risiko->save();
        
            //-- delete sub Risiko-----------------------------------------------------------------------------------
                $delrisiko=Risiko::where('obyek_id',$id)
                                    ->where('sts','2')->delete();

                $risk = new \DOMDocument();
                $subrisiko = $request->subrisiko;
                @$risk->loadHTML($subrisiko);
                $subrisiko = $risk->getElementsByTagName('li');
                $counter = 0;
            
            //-- save Risiko
                foreach ($subrisiko as $key => $value) {
                    $key            =   New Risiko;
                    $key->obyek_id  =   $request->id;
                    $key->sts       =   2;
                    $key->risiko    =   $value->nodeValue;
                    $key->save();
                }
        //--end Risiko--------------------------------------------------------------------------
        
        //---Proses Ruang Lingkup
            //delete anggota
                $delruanglingkup=RuangLingkup::where('obyek_id',$id)->where('sts',2)->delete();
                    $ruanglingkup                  = New RuangLingkup;
                    $ruanglingkup                  = RuangLingkup::where('obyek_id',$id)->where('sts',1)->first();
                    $ruanglingkup->ruanglingkup    = $request->ruanglingkup;
                    $ruanglingkup->sts             = 1;
                    $ruanglingkup->save();
            //-- save anggota
                if (is_null($request->sub_ruanglingkup)) {
                    foreach ($request->sub_ruanglingkuplama as $a){
                        $sub_ruanglingkuplama                  = New RuangLingkup;
                        $sub_ruanglingkuplama->unit_kerja_id   = $a;
                        $sub_ruanglingkuplama->sts             = 2;
                        $sub_ruanglingkuplama->obyek_id        = $request->id;
                        $sub_ruanglingkuplama->save();
                    } 
                } else {
                    //$jumanggota = count($request->anggota);
                    foreach ($request->sub_ruanglingkup as $a) {
                        $sub_ruanglingkup                  = New RuangLingkup;
                        $sub_ruanglingkup->unit_kerja_id   = $a;
                        $sub_ruanglingkup->sts             = 2;
                        $sub_ruanglingkup->obyek_id        = $request->id;
                        $sub_ruanglingkup->save();
                    }

                }
        //--end ruanglingkup----------------------------------------------------------------    
        //--proses jadwal
            // delete Jadwal
                    $deljadwal=Jadwal::where('obyek_id',$id)->delete();
            // save Jadwal
                for ($x=0;$x < sizeof($request->tahapan_id);$x++) {
                    $jadwal                   = New Jadwal;
                    $jadwal->tahapan_id       = $request->tahapan_id[$x];
                    $jadwal->kategori         = $request->kategori[$x];
                    $jadwal->tgl_mulai        = date('Y-m-d',strtotime($request->tgl_mulai[$x]));
                    $jadwal->tgl_sampai       = date('Y-m-d',strtotime($request->tgl_sampai[$x]));
                    $jadwal->obyek_id         = $request->id;
                    $jadwal->save();
                }    
        //end jadwal---------------------------------------------------------------------
        if($sasar){
            return redirect('/detobyek');
        }
     }
}
