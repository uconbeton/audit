<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Obyek;
use App\Unit;
use App\UnitKerja;
use App\Karyawan;
use App\ObyekTim;
use App\ApproveDeskaudit;
use App\LangkahKerja;
use App\DeskAudit;
use App\Tujuan;
use App\FileLangkahKerja;
class DeskController extends Controller
{
    public function form($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'deskaudit.form', compact('obyeks') );
    }

    public function formrevisi($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'deskaudit.formrevisi', compact('obyeks') );
    }

    public function formrevisipencatatan($id){
        $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('id',$id)->first();
        return view( 'deskaudit.formrevisipencatatan', compact('obyeks') );
    }

    public function edit($id){
        $deskaudits = DeskAudit::where('id',$id)->first();
        $langkahkerja = LangkahKerja::where('deskaudit_id',$id)->get();
        return view( 'deskaudit.edit', compact('deskaudits','langkahkerja') );
    }

    public function obyeklist(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->get();
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'deskaudit.index', compact('obyeks','obyektims','tujuans') );

       
    }

    public function index(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek ketua tim
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('nik_ketua_tim',Auth::user()->nik)->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',1)->orderBy('id','DESC')->get();
        return view( 'deskaudit.index', compact('revisis','obyeks','obyektims','tujuans') );

       
    }

    public function indexapp(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('nik_pengawas','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
       
        return view( 'deskaudit.indexapp', compact('obyeks','obyektims','tujuans') );

       
    }

    public function indexpencatatan(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            //$obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_deskaudit_id',1)->orderBy('id','DESC')->paginate(10);
        //==========================================================//    
           $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_deskaudit_id',1)->where('nik_anggota','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //=============================================================================
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',2)->orderBy('id','DESC')->get();
        return view( 'deskaudit.pencatatan', compact('obyeks','obyektims','tujuans','revisis') );

       
    }

    public function indexpencatatanpeng(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_deskaudit_id',1)->where('nik_pengawas','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
       
        return view( 'deskaudit.pencatatanpeng', compact('obyeks','obyektims','tujuans') );

       
    }


    public function indexpencatatanket(){
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts',3)->where('sts_deskaudit_id',1)->where('nik_ketua_tim',Auth::user()->nik)->where('tahun_pkat',$tahun)->orderBy('id','DESC')->paginate(10);
        //Tampilkan data obyektim
            $obyektims  = ObyekTim::with(['karyawan'])->get();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        //Tampilkan data approveaudit
            $revisis = ApproveDeskaudit::where('sts',2)->orderBy('id','DESC')->get();
        return view( 'deskaudit.pencatatanket', compact('obyeks','obyektims','tujuans','revisis') );

       
    }

   

    public function kirimdeskaudit($id){
        // ---- update Obyek
            $deskaudit                =  Obyek::find($id);
            $deskaudit->sts_deskaudit_id  =  0;
            $deskaudit->tgl_sts_deskaudit_id  =  date('Y-m-d');
            $deskaudit->save();
            if($deskaudit){
                return redirect('/deskaudit/');
            }
    }

    public function kirimpencatatandeskaudit($id){
        // ---- update Obyek
            $deskaudit                =  Obyek::find($id);
            $deskaudit->sts_pencatatan_deskaudit  =  0;
            $deskaudit->tgl_sts_pencatatan_deskaudit  =  date('Y-m-d');
            $deskaudit->save();
            if($deskaudit){
                return redirect('/deskaudit/pencatatanket');
            }
    }
    
    public function setujui($id){
        // ---- update Obyek
        $deskaudit                =  Obyek::find($id);
        $deskaudit->sts_deskaudit_id  =  1;
        $deskaudit->tgl_sts_deskaudit_id  =  date('Y-m-d');
        $deskaudit->save();
        if($deskaudit){
            return redirect('/deskaudit/indexapp');
        }
    }

    public function setujuipencatatan($id){
        // ---- update Obyek
        $deskaudit                            =  Obyek::find($id);
        $deskaudit->sts_pencatatan_deskaudit  =  1;
        $deskaudit->tgl_sts_pencatatan_deskaudit  =  date('Y-m-d');
        $deskaudit->save();
        if($deskaudit){
            return redirect('/deskaudit/pencatatanpeng');
        }
    }
    public function indexdesk($id){
        //Tampilkan data obyek
            $deskaudit = DeskAudit::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahKerja::all();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'deskaudit.detaildesk', compact('deskaudit','langkahkerjas') );

       
    }

    public function indexdeskpeng($id){
        //Tampilkan data obyek
            $deskaudit = DeskAudit::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahKerja::all();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'deskaudit.detaildeskpeng', compact('deskaudit','langkahkerjas') );

       
    }

    public function detailpencatatan($id){
        //Tampilkan data obyek
            $obyek = Obyek::where('id',$id)->first();
        //Tampilkan data deskaudit
            $deskaudit = DeskAudit::where('obyek_id',$id)->get();
            $ajaxdeskaudit = DeskAudit::where('obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahKerja::all();
            $ajaxlangkahkerjas  = LangkahKerja::all();
           // dd($obyektim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
            $ajaxtujuans = Tujuan::all();
        return view( 'deskaudit.detailpencatatan', compact('obyek','deskaudit','langkahkerjas','ajaxdeskaudit','ajaxlangkahkerjas') );
    }

    public function detailpencatatanpeng($id){
        error_reporting(0);
        //Tampilkan data obyek
            $obyeks = Obyek::where('id',$id)->first();
        //Tampilkan data deskaudit
            $deskaudit = DeskAudit::where('obyek_id',$id)->get();
            $ajaxdeskaudit = DeskAudit::where('obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahKerja::all();
            $ajaxlangkahkerjas  = LangkahKerja::all();
           // dd($obyektim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        
        return view( 'deskaudit.detailpencatatanpeng', compact('obyeks','deskaudit','langkahkerjas','ajaxdeskaudit','ajaxlangkahkerjas') );
    }

    public function indexpengawas($id){
        //Tampilkan data obyek
            $deskaudit = DeskAudit::where('Obyek_id',$id)->get();
        //Tampilkan data obyektim
            $langkahkerjas  = LangkahKerja::all();
           // dd($obyekstim);
        //Tampilkan data Tujuan
            $tujuans = Tujuan::all();
        return view( 'deskaudit.detailpengawas', compact('deskaudit','langkahkerjas') );

       
    }

    public function insert_revisi(Request $request , $id){
        $rules = [
            'keterangan'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('deskaudit/formrevisi')
                        ->withErrors($validator)
                        ->withInput();
        }
        // ---- update Obyek
            $obyek                    =  Obyek::find($id);
            $obyek->sts_deskaudit_id  =  2;
            $obyek->save();
        // ---- save Revisi
            $revisi                   =  New ApproveDeskaudit;
            $revisi->keterangan       =  $request->keterangan;
            $revisi->obyek_id         =  $request->id;
            $revisi->tanggal          =  date('Y-m-d');
            $revisi->sts              =  1;
            $revisi->save();

        if($revisi){
            return redirect('/deskaudit/indexapp');
        }
    }

    public function insert_revisipencatatan(Request $request , $id){
        $rules = [
            'keterangan'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('deskaudit/formrevisi')
                        ->withErrors($validator)
                        ->withInput();
        }
        // ---- update Obyek
            $obyek                    =  Obyek::find($id);
            $obyek->sts_pencatatan_deskaudit  =  2;
            $obyek->save();
        // ---- save Revisi
            $revisi                   =  New ApproveDeskaudit;
            $revisi->keterangan       =  $request->keterangan;
            $revisi->obyek_id         =  $request->id;
            $revisi->tanggal          =  date('Y-m-d');
            $revisi->sts              =  2;
            $revisi->save();

        if($revisi){
            return redirect('/deskaudit/pencatatanpeng');
        }
    }

    public function insert(Request $request){
        
       
        $rules = [
            'pokok_materi'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('deskaudit/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        // ---- save deskaudit
            $deskaudit                   = New DeskAudit;
            $deskaudit->pokok_materi     =  $request->pokok_materi;
            $deskaudit->obyek_id         =  $request->id;
            $deskaudit->tgl_deskaudit    =  date('Y-m-d');
            $deskaudit->sts              =  1;
            $deskaudit->save();

        // save Langkah Kerja
            for ($x=0;$x < sizeof($request->langkahkerja);$x++) {
                $langkahkerja                = New LangkahKerja;
                $langkahkerja->obyek_id      = $request->id;
                $langkahkerja->deskaudit_id  = $deskaudit->id;
                $langkahkerja->sts           = 1;
                $langkahkerja->urut           = $x;
                $langkahkerja->langkah_kerja = $request->langkahkerja[$x];
                $langkahkerja->tgl_rencana   = date('Y-m-d',strtotime($request->tgl_rencana[$x]));
                $langkahkerja->save();
            }
        if($deskaudit){
            return redirect('/deskaudit/');
        }
     }

     public function update(Request $request , $id){
        
       
        $rules = [
            'pokok_materi'   => 'required'
        ];
    
        $customMessages = [
            'required' => ':attribute Harus diisi',
            'max' => ':attribute Harus diisi'
        ];
    
        

        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('deskaudit/form')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        // ---- save deskaudit
            $deskaudit                   =  DeskAudit::find($id);
            $deskaudit->pokok_materi     =  $request->pokok_materi;
            $deskaudit->save();

        // delete Langkah Kerja
            $dellangkah=LangkahKerja::where('deskaudit_id',$id)->delete();
        // save Langkah Kerja
            
            for ($x=0;$x < sizeof($request->langkahkerja);$x++) {
                $langkahkerja                = New LangkahKerja;
                $langkahkerja->obyek_id      = $deskaudit->obyek_id;
                $langkahkerja->deskaudit_id  = $request->id;
                $langkahkerja->sts           = 1;
                $langkahkerja->urut           = $x;
                $langkahkerja->langkah_kerja = $request->langkahkerja[$x];
                $langkahkerja->tgl_rencana   = date('Y-m-d',strtotime($request->tgl_rencana[$x]));
                $langkahkerja->save();
            }
        if($deskaudit){
            return redirect('/deskaudit/detaildesk/'.$deskaudit->obyek_id);
        }
     }

     public function insertpencatatan(Request $request , $id){
        
        // ---- save langkah kerja
        for ($x=0;$x < sizeof($request->id);$x++) {
            
                $langkahkerja                = New LangkahKerja;
                $langkahkerja                = LangkahKerja::find($id); 
                $langkahkerja->pencatatan    = $request->pencatatan[$x];
                $langkahkerja->tgl_aktual    =date('Y-m-d',strtotime($request->tgl_aktual[$x]));
                $langkahkerja->save();
                
            
            }
           
                return redirect('/deskaudit/detailpencatatan/'.$request->obyek_id);
                 
        
     }

     public function delete($id){
        //Tampilkan data langkahkerja
            $desk           =  DeskAudit::find($id);
            dd($desk->obyek_id);
            $deskaudit      =  DeskAudit::where('id',$id)->delete();
            $langkahkerja   =  LangkahKerja::where('deskaudit_id',$id)->delete();
            return redirect('/deskaudit/detaildesk/'.$desk->obyek_id);
     }

     public function delete_langkah($id){
        //Tampilkan dan delete langkah kerja
           $langkah                   =  LangkahKerja::find($id);
           $langkahkerja=LangkahKerja::where('id',$id)->delete();
            return redirect('/deskaudit/detaildesk/'.$langkah->obyek_id);
    }

    public function filedeskaudit($id,$id1){
        $id=$id;
        $obyek_id=$id1;
        $obyekfile=Obyek::where('id',$id1)->first();
        $file=FileLangkahKerja::where('obyek_id',$obyek_id)->where('relasi_id',$id)->where('sts_catatan',1)->get();
        $files=FileLangkahKerja::where('obyek_id',$obyek_id)->where('relasi_id',$id)->where('sts_catatan',1)->get();
        return view( 'deskaudit.file', compact('id','obyek_id','file','files','obyekfile') );

    }

    public function insertfile(request $request){
        $rules = [
            'filepencatatan'        => 'required|max:200|mimes:jpeg,bmp,png,gif,pdf,xlsx,doc,docx,ppt',
            'nama'                  => 'required',
        ];

        $customMessages = [
            'nama.required'                 => '- Nama File Harus diisi ',
            'filepencatatan.required'       => '- Upload file terlebih dahulu ',
            'filepencatatan.max'            => '- Maximal file 200kb',
            'filepencatatan.mimes'          => '- Format file hanya jpeg,bmp,png,gif,pdf,xlsx,doc,docx,ppt',
        ];
    
        $validator = Validator::make($request->all(), $rules,$customMessages); 
       
        if ($validator->fails()) {
            return redirect('deskaudit/file/'.$request->id.'/'.$request->obyek_id)
                        ->withErrors($validator)
                        ->withInput();
        }
        $type = $request->filepencatatan->extension();
        $fileed = $request->filepencatatan->storeAs('images',$request->nama.'.'.$type);


        $file               =New FileLangkahKerja;
        $file->relasi_id    =$request->id;
        $file->obyek_id     =$request->obyek_id;
        $file->file         =$fileed;
        $file->type         =$type;
        $file->sts_catatan  =1;
        $file->save();
        
        return redirect('/deskaudit/file/'.$request->id.'/'.$request->obyek_id);

    }

    public function deletefile($kode,$id,$id1){
        //Tampilkan data langkahkerja
            $subs             =  FileLangkahKerja::where('id',$kode)->delete();
            return redirect('/deskaudit/file/'.$id.'/'.$id1);
    }
}
