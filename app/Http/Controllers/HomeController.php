<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Obyek;
use App\Tahapan;
use App\Sasaran;
use App\Risiko;
use App\DeskAudit;
use App\RuangLingkup;
use App\ObyekTim;
use App\Unit;
use App\UnitKerja;
use App\Tujuan;
use App\Jadwal;
use App\Posisi;
use App\KategoriAudit;
use App\Karyawan;
use App\LhaAudit;
use App\Rekomendasi;
use App\SubRekomendasi;
use App\SubSubRekomendasi;
use App\Kodifikasi;
class HomeController extends Controller
{
    public function index(){
            if(is_null(session('tahun'))){
                $tahun=date('Y');
            }else{
                $tahun=session('tahun');
            }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->get();
            $obyekspeng = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('nik_pengawas','REGEXP','[[:<:]]'.Auth::user()->nik.'[[:>:]]')->get();
            $inlis="2,3";
            
            $jumobyek=Obyek::where('tahun_pkat',$tahun)->count();
            $jumperesen=Obyek::whereIn('sts',explode(',', $inlis))->where('tahun_pkat',$tahun)->count();
            $deskok=Obyek::where('sts_deskaudit_id',1)->count();
            $deskcatatok=Obyek::where('sts_pencatatan_deskaudit',1)->where('tahun_pkat',$tahun)->count();
            $comok=Obyek::where('sts_compliance_id',1)->where('tahun_pkat',$tahun)->count();
            $comcatatok=Obyek::where('sts_pencatatan_compliance',1)->where('tahun_pkat',$tahun)->count();
            $subsok=Obyek::where('sts_substantive_id',1)->where('tahun_pkat',$tahun)->count();
            $subscatatok=Obyek::where('sts_pencatatan_substantive',1)->where('tahun_pkat',$tahun)->count();
            $obyekss = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->whereIn('sts',explode(',', $inlis))->where('tahun_pkat',$tahun)->get();
            $obyekss->map(function ($item, $key){
                if($item->sts_pengantar==1){$pengantar=10;}else{$pengantar=0;}
                if($item->sts_setujui1==1){$setujui1=20;}else{$setujui1=0;}
                if($item->sts_setujui2==1){$setujui2=10;}else{$setujui2=0;}
                if($item->sts_deskaudit_id==1){$prodeskaudit=5;}else{$prodeskaudit=0;}
                if($item->sts_pencatatan_deskaudit==1){$pendeskaudit=10;}else{$pendeskaudit=0;}
                if($item->sts_compliance_id==1){$procomaudit=5;}else{$procomaudit=0;}
                if($item->sts_pencatatan_compliance==1){$pencomaudit=10;}else{$pencomaudit=0;}
                if($item->sts_substantive_id==1){$prosubsaudit=5;}else{$prosubsaudit=0;}
                if($item->sts_pencatatan_substantive==1){$pensubsaudit=25;}else{$pensubsaudit=0;}
                $prsn=100/9;
                
                $hslper=$pengantar+$setujui1+$setujui2+$prodeskaudit+$pendeskaudit+$procomaudit+$pencomaudit+$prosubsaudit+$pensubsaudit;
                $item['total']=$hslper;
                
                return $item;
            });
            
            foreach($obyekss as $oby){
                if($oby->sts_setujui2==1){$pengantar=20;}else{$pengantar=0;}
                if($oby->sts_setujui1==1){$setujui1=10;}else{$setujui1=0;}
                if($oby->sts_setujui2==1){$setujui2=10;}else{$setujui2=0;}
                if($oby->sts_deskaudit_id==1){$prodeskaudit=5;}else{$prodeskaudit=0;}
                if($oby->sts_pencatatan_deskaudit==1){$pendeskaudit=10;}else{$pendeskaudit=0;}
                if($oby->sts_compliance_id==1){$procomaudit=5;}else{$procomaudit=0;}
                if($oby->sts_pencatatan_compliance==1){$pencomaudit=10;}else{$pencomaudit=0;}
                if($oby->sts_substantive_id==1){$prosubsaudit=5;}else{$prosubsaudit=0;}
                if($oby->sts_pencatatan_substantive==1){$pensubsaudit=25;}else{$pensubsaudit=0;}
                $obytoto[]=$pengantar+$setujui1+$setujui2+$prodeskaudit+$pendeskaudit+$procomaudit+$pencomaudit+$prosubsaudit+$pensubsaudit;
            }
            if($jumperesen>0){
				$totalpresentase=round(array_sum($obytoto)/$jumperesen,0);
			}else{
				$totalpresentase=0;
			}
            $kodifikasi = Kodifikasi::all();
            $subrekomendasi = collect([]);
            $subsubrekomendasi = collect([]);
            $ssubrekomendasi = collect([]);
            $ssubsubrekomendasi = collect([]);

            foreach($obyeks as $obyek){
                $kodi = Kodifikasi::all();
                foreach($kodi as $r=> $kode){
                    $s=SubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->count();
                    
                    $subrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $s
                    ]);

                    $t=SubSubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->count();
                    
                    $subsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $t
                    ]);
                    
                    $ss=SubRekomendasi::where('kodifikasi',$kode->kodifikasi)->count();
                    
                    $ssubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ss
                    ]);

                    $ts=SubSubRekomendasi::where('kodifikasi',$kode->kodifikasi)->count();
                    
                    $ssubsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ts
                    ]);
                }
                
            }
            if($jumobyek>0){
                $persen=100/$jumobyek;
            }else{
                $persen=0;
            }
            $prodesk=$deskok*$persen;
            $prodesnnot=100-$prodesk;
            $pendesk=$deskcatatok*$persen;
            $pendesknot=100-$pendesk;

            $procom=$comok*$persen;
            $procomnot=100-$procom;
            $pencom=$comcatatok*$persen;
            $pencomnot=100-$pencom;

            $prosubs=$subsok*$persen;
            $prosubsnot=100-$prosubs;
            $pensubs=$subscatatok*$persen;
            $pensubsnot=100-$pensubs;
            
            $kategori=KategoriAudit::all();
            $jumket=[];
            foreach($kategori as $as=>$kate){
                $jumlahkat=Obyek::where('kategori_audit_id',$kate->id)->count();
                array_push($jumket,$jumlahkat);
            }
            
         return view( 'index', compact('totalpresentase','obyekspeng','jumket','prosubs','prosubsnot','pensubs','kategori','pensubsnot','prodesk','prodesnnot','pendesk','pendesknot','procom','procomnot','pencom','pencomnot','obyeks','obyekss','kodifikasi','subrekomendasi','subsubrekomendasi','ssubrekomendasi','ssubsubrekomendasi','persen') );

    
    }

    public function tahapan(){
            if(is_null(session('tahun'))){
                $tahun=date('Y');
            }else{
                $tahun=session('tahun');
            }
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->get();
            $jumobyek=Obyek::where('tahun_pkat',$tahun)->count();
            $deskok=Obyek::where('sts_deskaudit_id',1)->where('tahun_pkat',$tahun)->count();
            $deskcatatok=Obyek::where('sts_pencatatan_deskaudit',1)->where('tahun_pkat',$tahun)->count();
            $comok=Obyek::where('sts_compliance_id',1)->where('tahun_pkat',$tahun)->count();
            $comcatatok=Obyek::where('sts_pencatatan_compliance',1)->where('tahun_pkat',$tahun)->count();
            $subsok=Obyek::where('sts_substantive_id',1)->where('tahun_pkat',$tahun)->count();
            $subscatatok=Obyek::where('sts_pencatatan_substantive',1)->where('tahun_pkat',$tahun)->count();
            $inlis="2,3";
            $obyekss = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->whereIn('sts',explode(',', $inlis))->where('tahun_pkat',$tahun)->get();
            $kodifikasi = Kodifikasi::all();
            $subrekomendasi = collect([]);
            $subsubrekomendasi = collect([]);
            $ssubrekomendasi = collect([]);
            $ssubsubrekomendasi = collect([]);

            foreach($obyeks as $obyek){
                $kodi = Kodifikasi::all();
                foreach($kodi as $r=> $kode){
                    $s=SubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->count();
                    
                    $subrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $s
                    ]);

                    $t=SubSubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->count();
                    
                    $subsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $t
                    ]);
                    
                    $ss=SubRekomendasi::where('kodifikasi',$kode->kodifikasi)->count();
                    
                    $ssubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ss
                    ]);

                    $ts=SubSubRekomendasi::where('kodifikasi',$kode->kodifikasi)->count();
                    
                    $ssubsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ts
                    ]);
                }
                
            }
           
            if($jumobyek>0){
                $persen=100/$jumobyek;
            }else{
                $persen=0;
            }
            $prodesk=$deskok*$persen;
            $prodesnnot=100-$prodesk;
            $pendesk=$deskcatatok*$persen;
            $pendesknot=100-$pendesk;

            $procom=$comok*$persen;
            $procomnot=100-$procom;
            $pencom=$comcatatok*$persen;
            $pencomnot=100-$pencom;

            $prosubs=$subsok*$persen;
            $prosubsnot=100-$prosubs;
            $pensubs=$subscatatok*$persen;
            $pensubsnot=100-$pensubs;
            
            $kategori=KategoriAudit::all();
            $jumket=[];
            foreach($kategori as $as=>$kate){
                $jumlahkat=Obyek::where('kategori_audit_id',$kate->id)->count();
                array_push($jumket,$jumlahkat);
            }
            
         return view( 'tahapan', compact('jumket','prosubs','prosubsnot','pensubs','kategori','pensubsnot','prodesk','prodesnnot','pendesk','pendesknot','procom','procomnot','pencom','pencomnot','obyeks','obyekss','kodifikasi','subrekomendasi','subsubrekomendasi','ssubrekomendasi','ssubsubrekomendasi','persen') );

    
    }

    public function kategori(){
        //Tampilkan data obyek
        if(is_null(session('tahun'))){
            $tahun=date('Y');
        }else{
            $tahun=session('tahun');
        }
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('tahun_pkat',$tahun)->get();
            $jumobyek=Obyek::where('tahun_pkat',$tahun)->count();
            $deskok=Obyek::where('sts_deskaudit_id',1)->where('tahun_pkat',$tahun)->count();
            $deskcatatok=Obyek::where('sts_pencatatan_deskaudit',1)->where('tahun_pkat',$tahun)->count();
            $comok=Obyek::where('sts_compliance_id',1)->where('tahun_pkat',$tahun)->count();
            $comcatatok=Obyek::where('sts_pencatatan_compliance',1)->where('tahun_pkat',$tahun)->count();
            $subsok=Obyek::where('sts_substantive_id',1)->where('tahun_pkat',$tahun)->count();
            $subscatatok=Obyek::where('sts_pencatatan_substantive',1)->where('tahun_pkat',$tahun)->count();
            $inlis="2,3";
            $obyekss = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->whereIn('sts',explode(',', $inlis))->get();
            $kodifikasi = Kodifikasi::all();
            $subrekomendasi = collect([]);
            $subsubrekomendasi = collect([]);
            $ssubrekomendasi = collect([]);
            $ssubsubrekomendasi = collect([]);

            foreach($obyeks as $obyek){
                $kodi = Kodifikasi::all();
                foreach($kodi as $r=> $kode){
                    $s=SubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->count();
                    
                    $subrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $s
                    ]);

                    $t=SubSubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->count();
                    
                    $subsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $t
                    ]);
                    
                    $ss=SubRekomendasi::where('kodifikasi',$kode->kodifikasi)->count();
                    
                    $ssubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ss
                    ]);

                    $ts=SubSubRekomendasi::where('kodifikasi',$kode->kodifikasi)->count();
                    
                    $ssubsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ts
                    ]);
                }
                
            }
            if($jumobyek>0){
                $persen=round(100/$jumobyek,0);
                $persenkat=100/$jumobyek;
            }else{
                $persen=0;
                $persenkat=0;
            }
            
            $prodesk=$deskok*$persen;
            $prodesnnot=100-$prodesk;
            $pendesk=$deskcatatok*$persen;
            $pendesknot=100-$pendesk;

            $procom=$comok*$persen;
            $procomnot=100-$procom;
            $pencom=$comcatatok*$persen;
            $pencomnot=100-$pencom;

            $prosubs=$subsok*$persen;
            $prosubsnot=100-$prosubs;
            $pensubs=$subscatatok*$persen;
            $pensubsnot=100-$pensubs;

            
            $kategori=KategoriAudit::all();
            $jumket=[];
            foreach($kategori as $as=>$kate){
                $jumlahkat=Obyek::where('kategori_audit_id',$kate->id)->count();
                array_push($jumket,$jumlahkat);
            }
            //dd($jumket);
         return view( 'kategori', compact('persenkat','jumket','prosubs','prosubsnot','pensubs','kategori','pensubsnot','prodesk','prodesnnot','pendesk','pendesknot','procom','procomnot','pencom','pencomnot','obyeks','obyekss','kodifikasi','subrekomendasi','subsubrekomendasi','ssubrekomendasi','ssubsubrekomendasi','persen') );

    
    }

    public function kodifikasi(){
        //Tampilkan data obyek
            $obyeks = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts_pengantar',1)->get();
            $obyeksss = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts_pengantar',1)->get();
            $jumobyek=Obyek::where('sts_pengantar',1)->count();
            $deskok=Obyek::where('sts_deskaudit_id',1)->where('sts_pengantar',1)->count();
            $deskcatatok=Obyek::where('sts_pencatatan_deskaudit',1)->where('sts_pengantar',1)->count();
            $comok=Obyek::where('sts_compliance_id',1)->where('sts_pengantar',1)->count();
            $comcatatok=Obyek::where('sts_pencatatan_compliance',1)->where('sts_pengantar',1)->count();
            $subsok=Obyek::where('sts_substantive_id',1)->where('sts_pengantar',1)->count();
            $subscatatok=Obyek::where('sts_pencatatan_substantive',1)->where('sts_pengantar',1)->count();
            $inlis="2,3";
            $obyekss = Obyek::with(['unit_kerja','pengawas','ketua_tim'])->where('sts_pengantar',1)->whereIn('sts',explode(',', $inlis))->orderBy('id', 'desc')->get();
            
            $kodifikasi = Kodifikasi::where('kodifikasi','!=',0)->get();
            $kodifikasis = Kodifikasi::where('kodifikasi','!=',0)->get();
            $temuan = collect([]);
            $ceksubrekomendasi = collect([]);
            $subrekomendasi = collect([]);
            $subrekomendasi = collect([]);
            $subsubrekomendasi = collect([]);
            $ssubrekomendasi = collect([]);
            $ssubsubrekomendasi = collect([]);
            $subtotal=SubRekomendasi::count();
            $subsubtotal=SubSubRekomendasi::count();
            //dd($subtotal);
            
            foreach($obyeks as $obyek){
                $kodi = Kodifikasi::where('kodifikasi','!=',0)->get();
                foreach($kodi as $r=> $kode){
                    $tm=Rekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->where('sts_progres',1)
                        ->count();
                    
                    $temuan->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $tm
                    ]);

                    //===========================================






                    $s=SubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->where('sts_progres',1)
                        ->count();
                    $cekseb=SubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->where('sts_progres',1)
                        ->first();
                    
                    $ceksubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi
                        //'totalsub' => $cekseb->totalsub
                    ]);

                    $subrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'cek' => $cekseb['totalsub'],
                        'count' => $s
                    ]);



                    
                    //=================================================
                    $t=SubSubRekomendasi::where('obyek_id',$obyek->id)
                        ->where('kodifikasi',$kode->kodifikasi)
                        ->where('sts_progres',1)
                        ->count();
                    
                    $subsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $t
                    ]);
                    
                    $ss=SubRekomendasi::where('kodifikasi',$kode->kodifikasi)->where('sts_progres',1)->count();
                    
                    $ssubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ss
                    ]);

                    $ts=SubSubRekomendasi::where('kodifikasi',$kode->kodifikasi)->where('sts_progres',1)->count();
                    
                    $ssubsubrekomendasi->push([
                        'obyek_id' => $obyek->id,
                        'kodifikasi' => $kode->kodifikasi,
                        'count' => $ts
                    ]);
                }
                
            }

            //dd($subrekomendasi);
            
            $kodif = Kodifikasi::where('kodifikasi','!=',0)->get();
            $kodif->map(function ($item, $key){
                $item['nilkodif'] = SubRekomendasi::where('kodifikasi',$item->kodifikasi)->where('sts_progres',1)->sum('nilai');
                $item['nilsubkodif'] = SubSubRekomendasi::where('kodifikasi',$item->kodifikasi)->where('sts_progres',1)->sum('nilai');
                $item['jumkodif'] = SubRekomendasi::where('kodifikasi',$item->kodifikasi)->where('sts_progres',1)->count('kodifikasi');
                $item['jumsubkodif'] = SubSubRekomendasi::where('kodifikasi',$item->kodifikasi)->where('sts_progres',1)->count('kodifikasi');
                $jumlah1 = SubRekomendasi::where('kodifikasi',$item->kodifikasi)->where('sts_progres',1)->count('kodifikasi');
                $jumlah2 = SubSubRekomendasi::where('kodifikasi',$item->kodifikasi)->where('sts_progres',1)->count('kodifikasi');
                $totalkode=$jumlah1+$jumlah2;
                return $item;
            });

            
            
            if($jumobyek>0){
                $persen=100/$jumobyek;
                $persenkat=100/$jumobyek;
                $total=$subtotal+$subsubtotal;
				
				
            }else{
                $persen=0;
                $persenkat=0;
                $total=0;
				
            }
           
            if($total>0){
				 $totalkode=100/$total; 
			}else{
				 $totalkode=0; 
			}
           

            $prodesk=$deskok*$persen;
            $prodesnnot=100-$prodesk;
            $pendesk=$deskcatatok*$persen;
            $pendesknot=100-$pendesk;

            $procom=$comok*$persen;
            $procomnot=100-$procom;
            $pencom=$comcatatok*$persen;
            $pencomnot=100-$pencom;

            $prosubs=$subsok*$persen;
            $prosubsnot=100-$prosubs;
            $pensubs=$subscatatok*$persen;
            $pensubsnot=100-$pensubs;
            
            $kategori=KategoriAudit::all();
            $jumket=[];
            foreach($kategori as $as=>$kate){
                $jumlahkat=Obyek::where('kategori_audit_id',$kate->id)->count();
                array_push($jumket,$jumlahkat);
            }
            
         return view( 'kode', compact('ceksubrekomendasi','totalkode','jumket','nilkodif','prosubs','kodif','prosubsnot','pensubs','kategori','pensubsnot','prodesk','prodesnnot','pendesk','pendesknot','procom','procomnot','pencom','pencomnot','obyeks','obyeksss','kodifikasi','kodifikasis','subrekomendasi','subsubrekomendasi','ssubrekomendasi','ssubsubrekomendasi','temuan','persen') );

    
    }
}
