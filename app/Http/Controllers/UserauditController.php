<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\UserAudit;
use App\akses;
use App\UnitKerja;
use App\Karyawan;

class UserauditController extends Controller
{
    public function index(){
        //Tampilkan data useraudit
            $useraudit = Karyawan::paginate(10);
        
        return view( 'user.index', compact('useraudit') );

       
    }
    
    public function form(){
        $akses = akses::all();
        $karyawan = akses::all();
        
        return view('user.form',compact('akses','karyawan'));
    }
}
