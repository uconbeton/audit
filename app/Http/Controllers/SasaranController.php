<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Obyek;
use App\ObyekTim;
use App\Divisi;
use App\Sasaran;
use App\Tim;

class SasaranController extends Controller
{

    public function sasaranform(){
        $items=New Divisi;
        $items = $items::all();
        $anggotas = Tim::with(['posisi','divisi'])->where('posisi_id',3)->get();
        return view('sasaran.form',compact('items'),compact('anggotas'));
    }


    public function sasaranlist(){

        $obyeks = Obyek::with(['divisi'])->get();
        return view( 'sasaran.index', compact('obyeks') );

       
    }

    public function insert(Request $request){
        $dom = new \DOMDocument();
        $html = $request->sasaran;
        @$dom->loadHTML($html);
        $rows = $dom->getElementsByTagName('li');
        $counter = 0;

      
        foreach ($rows as $key => $value) {
            $key   = New Sasaran;
            $key->obyek_id =  $request->obyek_id;
            $key->sasaran =$value->nodeValue;
            $key->save();
        }
        
        
        if($key){
            return redirect('/sasaran');
        }
     }

}
