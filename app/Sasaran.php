<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sasaran extends Model
{
    protected $table = 'sasaran';
    public $timestamps = false;
    public function unitkerja()
    {
        return $this->belongsTo('App\UnitKerja');
    }
}
