<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriSubstantive extends Model
{
    protected $table = 'materi_substantive';
    public $timestamps = false;
}
