<?php

function get_deskaudit($id){
    $data=App\DeskAudit::where('obyek_id',$id)->get();
    return $data;
}
function get_langkahkerja($id){
    $data=App\LangkahKerja::where('deskaudit_id',$id)->get();
    return $data;
}

function get_substantive($id){
    $data=App\SubstantiveTest::where('obyek_id',$id)->get();
    return $data;
}
function get_langkahkerja_substantive($id){
    $data=App\LangkahSubstantive::where('substantive_test_id',$id)->get();
    return $data;
}

function get_compliance($id){
    $data=App\ComplianceTest::where('obyek_id',$id)->get();
    return $data;
}
function get_langkahkerja_compliance($id){
    $data=App\LangkahCompliance::where('compliance_test_id',$id)->get();
    return $data;
}




?>