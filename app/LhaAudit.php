<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LhaAudit extends Model
{
    protected $table = 'lha_audit';
    public $timestamps = false;
    public function karyawan()
    {
        return $this->hasOne('App\Karyawan', 'nik', 'nik_create');
    }
    public function loadKaryawan()
    {
        return $this->karyawan()->where('nik', 'nik_create')->first();
    }
}
