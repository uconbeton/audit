<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubRekomendasi extends Model
{
    protected $table = 'sub_rekomendasi';
    public $timestamps = false;
    public function subsubrekomendasi()
    {
        return $this->hasMany('App\SubSubRekomendasi', 'sub_rekomendasi_id', 'id');
    }
    public function loadJumRek()
    {
        return $this->load(['subsubrekomendasi'])->subsubrekomendasi()->count();
    }
    public function pic()
    {
        return $this->belongsTo('App\Pic', 'pic_id', 'id');
    }
}
