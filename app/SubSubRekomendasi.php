<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSubRekomendasi extends Model
{
    protected $table = 'sub_subrekomendasi';
    public $timestamps = false;
    
}
