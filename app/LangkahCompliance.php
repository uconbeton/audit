<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LangkahCompliance extends Model
{
    protected $table = 'langkah_kerja_compliance';
    public $timestamps = false;
}
