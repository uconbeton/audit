<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubstantiveTest extends Model
{
    protected $table = 'substantive_test';
    public $timestamps = false;
}
