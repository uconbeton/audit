<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HisPic extends Model
{
    protected $table = 'his_pic';
    public $timestamps = false;
}
