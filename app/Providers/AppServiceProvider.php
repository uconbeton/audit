<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Obyek;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(config('app.env') === 'production'){
            \Illuminate\Support\Facades\URL::forceScheme('https');
        }
        $viewobyek = Obyek::selectRaw('tahun_pkat')
            ->groupBy('tahun_pkat')
            ->get();
        
        View::share('viewobyek', $viewobyek);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('Helpers/function.php');
    }
}
