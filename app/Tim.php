<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tim extends Model
{
    protected $table = 'tim';
    public $timestamps = false;
    public function divisi()
    {
        return $this->belongsTo('App\Divisi');
    }

    public function posisi()
    {
        return $this->belongsTo('App\Posisi');
    }

    public function obyektim()
    {
        return $this->belongsTo('App\Obyektim');
    }
}
