@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


  
  <form method="post" action="{{ url('/obyek/update/'.$obyeks->id)}}">
    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header" style="background: #e8e8de">
          <B>FORM OBYEK</B>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label>Unit Kerja:</label>
            <select class="form-control" style="width: 100%;" id="unit_id" name="unit_id"  tabindex="-1" aria-hidden="true">
              @foreach($items as $item)
                <option value="{{ $item->id }}" @if ( $obyeks->unit_id ==$item->id) {{ 'selected' }} @endif>{{ $item->nama }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Area Kerja:</label>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-danger" onclick="unitkerja()">Pilih</button>
              </div>
              <input type="hidden" id="unit_kerja_id" name="unit_kerja_id" value="{{ $obyeks->unit_kerja_id }}"class="form-control">
              <input type="text" id="nama_unit_kerja" name="nama_unit_kerja" value="{{ $obyeks->unit_kerja['nama'] }}" class="form-control">
              <input type="hidden" id="kode" name="kode" value="{{ $obyeks->kode }}"class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label>Nama Obyek:</label>
              <input class="form-control pull-right" type="text" name="nama"  value="{{ $obyeks->nama }}" placeholder="Nama Obyek">
          </div>

          <div class="form-group">
            <label>Kategori Audit:</label>
            <select class="form-control" style="width: 100%;" id="kategori_audit_id" name="kategori_audit_id"  tabindex="-1" aria-hidden="true">
              @foreach($kategoris as $kategori)
                <option value="{{ $kategori->id }}" @if ( $obyeks->kategori_audit_id ==$kategori->id) {{ 'selected' }} @endif>[ {{ $kategori->kode }} ] {{ $kategori->keterangan }}</option>
              @endforeach
            </select>
          </div>


            <label>Tujuan :</label>
            <textarea id="editor1" name="tujuan" rows="10" cols="80">
              @foreach($tujuans as $tujuan)
                <li>{{ $tujuan->tujuan }}</li>
              @endforeach
            </textarea>
          <!-- Date range -->
          <div class="form-group">
            <label>Ditanda Tangani:</label>
            <select class="form-control" style="width: 100%;" id="ttd" name="ttd"  tabindex="-1" aria-hidden="true">
             
                <option value="1" @if ($obyeks->ttd =='1') {{ 'selected' }} @endif>General Manager</option>
                <option value="2" @if ($obyeks->ttd =='2') {{ 'selected' }} @endif>Direktur Utama</option>
              
            </select>
          </div>
          <div class="form-group">
            <label>Pengawas:</label>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-danger" onclick="showpeng()">Pilih</button>
              </div>
              <input type="hidden" id="nik_pengawas" name="nik_pengawas" value="{{ $obyeks->nik_pengawas }}" class="form-control">
              <input type="text" id="nama_pengawas" name="nama_pengawas" value="{{ $obyeks->pengawas['nama'] }}" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label>Ketua Tim:</label>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-danger" onclick="show()">Pilih</button>
              </div>
              <input type="hidden" id="nik_ketua_tim" name="nik_ketua_tim" value="{{ $obyeks->nik_ketua_tim }}"class="form-control">
              <input type="text" id="nama_ketua_tim" name="nama_ketua_tim" value="{{ $obyeks->ketua_tim['nama'] }}" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label>Anggota:</label>
            <select class="form-control select2" multiple="multiple" data-placeholder="Pilih Anggota" name="anggota[]" style="color:#000">
                @php $no = 1; @endphp
                @foreach($anggotas as $anggota)
                    <option value="{{ $anggota->nik }}">{{ $anggota->nama }}</option>
                @endforeach
            </select>
           
              @foreach($obyekstim as $urut =>$anggotalama)
                <input type="hidden" name="anggotalama[]" value="{{ $anggotalama->nik }}">
                <span class="btn btn-success">{{ $urut+1}}.{{ $anggotalama->karyawan['nama'] }}  (anggota)</span>
                <a href="{{ url('obyek/deltim/'.$anggotalama->id)}}"><i class="fa fa-remove"></i></a>
                
              @endforeach
           
          </div>
          <div class="form-group">
            <label>Waktu:</label>
              <input class="form-control pull-right" type="number" name="waktu_audit"  value="{{ $obyeks->waktu_audit }}" placeholder="">
          </div>
          <div class="form-group">
            <label>Tahun Audit:</label>
              <input class="form-control pull-right" type="text" name="tahun_audit"  value="{{ $obyeks->tahun_pkat }}" placeholder="9999">
          </div>
          <div class="form-group">
            <label>Nomor Surat Tugas:</label>
              <input class="form-control pull-right" type="text" name="nomor_surat_tugas"  value="{{  $obyeks->nomor_surat_tugas }}" placeholder="9999">
          </div>
          <br>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
  </div>
</form>
@endsection
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  function unitkerja() 
    {
      var unit=$('#unit_id').val();
      window.open("../../popunitkerja/"+unit+"", "list", "width=800,height=420");
    }

  function show() 
    {
      window.open("{{ url('popketuatim')}}", "list", "width=800,height=500");
    }
    
  function showpeng() 
    { var unit=$('#kategori_audit_id').val();
      if(unit==3 || unit==4 || unit==7){
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }else{
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }
      
    }

  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }

  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>