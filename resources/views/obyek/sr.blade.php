
  
 
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
            <tr>
              <td colspan="3" align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="100px"><br>
                 <h2 style="margin-bottom:4px"><u><b>&nbsp;&nbsp;SURAT&nbsp;&nbsp;&nbsp;&nbsp;TUGAS&nbsp;&nbsp;</b></u></h2><h4 style="margin-top:4px">Nomor : @if($obyeks->id>9){{ $obyeks->id }}@else 0{{ $obyeks->id }} @endif/IA-ST/{{ date('m',strtotime($obyeks->waktu))}}.{{ date('y',strtotime($obyeks->waktu))}}</h4>                  
              </td>
            </tr>
            <tr>
              <td class="tdr"></td>
              <td class="tdr"><br>Head of Internal Audit memberi tugas kepada :<br><br></td>
              <td class="tdr" width="15%"></td>
            </tr>
            <tr>
              <td width="12%" class="tdr"></td>
              <td class="tdr"></td>
              <td class="tdr" width="15%"></td>
            </tr>
          </table>
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma;">
            <tr>
              <td  class="tdr" width="2%">1.</td>
              <td class="tdr" width="40%">{{ $obyeks->pengawas['nama'] }}</td>
              <td class="tdr" width="6%">NIK</td>
              <td class="tdr" width="10%">{{ $obyeks->pengawas['nik'] }}</td>
              <td class="tdr">(Pengawas)</td>
            </tr>
            <tr>
              <td  class="tdr" width="2%">2.</td>
              <td class="tdr">{{ $obyeks->ketua_tim['nama'] }}.</td>
              <td class="tdr">NIK</td>
              <td class="tdr">{{ $obyeks->ketua_tim['nik'] }}</td>
              <td class="tdr">(Ketua Tim)</td>
            </tr>
              @foreach($obyekstim as $key => $anggota)
                <tr>
                  <td  class="tdr" width="2%">{{ $key+3}}.</td>
                  <td class="tdr">{{ $anggota->karyawan['nama']}}</td>
                  <td class="tdr">NIK</td>
                  <td class="tdr">{{ $anggota->karyawan['nik'] }}</td>
                  <td class="tdr">(Anggota)</td>
                </tr>
              @endforeach
                    
          </table><br>
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
              <tr>
                <td class="tdr" colspan="3">Untuk melaksanakan audit terhadap  :</td>
              </tr>
              <tr>
                <td width="20%" class="tdr">Obyek Audit</td>
                <td class="tdr" width="3%">:</td>
                <td class="tdr" >{{ $obyeks->nama}}</td>
              </tr>
              <tr>
                  <td width="15%" class="tdr">Lokasi</td>
                  <td class="tdr" width="3%">:</td>
                  <td class="tdr" >Cilegon</td>
                </tr>
              <tr>
                <td class="tdr">Kode Audit</td>
                <td class="tdr">:</td>
              <td class="tdr">{{ $kategori->kode}}/{{ $obyeks->kode }}/{{ date('m/Y',strtotime($obyeks->waktu)) }}</td>
              </tr>
              <tr>
                <td class="tdr">Jumlah Hari Audit</td>
                <td class="tdr">:</td>
                <td class="tdr">{{ $obyeks->waktu_audit}}Hari</td>
              </tr>
              <tr>
                <td class="tdr">Catatan Penting</td>
                <td class="tdr">:</td>
                <td class="tdr">..........................................................................</td>
              </tr>
              <tr>
                <td class="tdr" colspan="3"><br><br>Demikian untuk dilaksanakan</td>
              </tr>
            </table>
            <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
            @if($obyeks->unit_id=='6')
              <tr align="center">
                <td class="tdr"width="60%"></td>
                <td class="tdr">Cilegon, {{ date('d F Y') }}<br>
                 
                    PT KRAKATAUSTEEL (Persero) Tbk.
                  
                    INTERNAL AUDIT
                  <br><br><br><br><br>
                  <h3 style="margin-bottom:4px"><u><b>&nbsp&nbsp;Direkturutama&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">{{ Auth::user()->jabatan}}</h4>
                </td>
              </tr>
            @else
              <tr align="center">
                <td class="tdr"width="60%"></td>
                <td class="tdr">Cilegon, {{ date('d F Y') }}<br>
                
                    INTERNAL AUDIT
                  <br><br><br><br><br>
                  <h4 style="margin-bottom:4px"><u><b>&nbsp&nbsp;{{$setujui->nama}}&nbsp&nbsp;</b></u></h4><h4 style="margin-top:4px">{{ $setujui->jabatan}}</h4>
                </td>
              </tr>

            @endif
            </table>
          <hr>
      
      </div>
      <table width="100%">
        <tr>
         <td align="center"> <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Surat tugas</button></td>
        </tr>
      </table><br><br>
    
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:12;text-transform:capitalize;}
</style>