@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              
              {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
              <a href="{{ url('obyek/form') }}"><button class="btn btn-primary"> <span class="fa fa-plus"></span> Tambah</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1"  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th >No</th>
                  <th>Kode<br>Obyek</th>
                  <th>Nama Obyek</th>
                  <th>Tujuan</th>
                  <th>Tim Audit</th>
                  <th>Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->unit_kerja['kode_unit'] }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            @php $x = 1; @endphp
                            @foreach( $tujuans as $tujuan )
                              @if($o->id==$tujuan->obyek_id)
                                {{ $x++ }}.{{ $tujuan->tujuan }}<br>
                              @endif
                            @endforeach
                          </td>
                          <td>
                            <ul>
                              <li>{{ $o->pengawas['nama'] }} (Pengawas)</li>
                              <li>{{ $o->ketua_tim['nama'] }} (Ketua Tim)</li>
                              
                              @foreach( $obyektims as $obyektim )
                                @if($o->id == $obyektim->obyek_id)
                                  <li>{{ $obyektim->karyawan['nama'] }} (anggota) </li>
                                @endif
                                
                              @endforeach
                            </ul>
                          </td>
                          <td>
                            <form action="" method="post">
                              
                              <a href="{{ url('obyek/memo/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-search-plus">Surat Tugas</span></a>
                              <a href="{{ url('obyek/edit/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="fa fa-gear">Update</span></a>
                              <a href="{{ url('obyek/delete/'.$o->id.'') }}" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-sm btn-danger"><span class="fa fa-remove">Delete</span></a>
                              
                            </form>  
                          </td>
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
              {{ $obyeks->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
  .btn{margin:3px;}
</style>
@push('link-java')
  <script>
  
    $(function () {
      $('#example1').DataTable({
        'lengthChange' : false,
        'autoWidth': false,
        'paging': false,
        "columnDefs": [
          { "width": "25%", "targets": 2 },
          { "width": "30%", "targets": 3},
          { "width": "24%", "targets": 4 },
          { "width": "10%", "targets": 5 }
        ]      
      })
      
    })
  </script>
@endpush