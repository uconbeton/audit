@extends('layout.app')
@section('konten')
<style>
    th{text-align:center;background: #e8e8f5;}
    td{padding:5px;}
</style>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header" style="background: #e8e8de">
            <B>Dashboard</B>
            </div>
            <div class="box-body">
                <table width="100%" border="1">
                    <tr>
                        <th rowspan="2" width="5%">No</th>
                        <th rowspan="2">Nama Obyek</th>
                        <th width="20%" colspan="3">Program</th>
                        <th width="20%" colspan="3">Pencatatan</th>
                        <th width="10%" rowspan="2">Persen</th>
                    </tr>
                    <tr>
                        <th>Desk</th>
                        <th>Subs</th>
                        <th>Comp</th>
                        <th>Desk</th>
                        <th>Subs</th>
                        <th>Comp</th>
                    </tr>

                    @foreach($obyeks as $no=> $obyek)
                        <tr>
                            <td align="center">{{ $no+1 }}</td>
                            <td>{{ $obyek->nama}}</td>
                            <td align="center">
                                @if($obyek->sts_deskaudit_id==1)
                                    @if($obyek->tgl_sts_deskaudit_id >= $obyek->loadDeskAuditProgram()->tgl_sampai)
                                        <img src="{{url('/img/cek2.png')}}" widt="15px" height="15px">
                                    @else
                                        <img src="{{url('/img/cek.png')}}" widt="15px" height="15px">
                                    @endif
                                    
                                @else
                                    <img src="{{url('/img/min.png')}}" widt="15px" height="15px">
                                @endif
                            </td>
                            <td align="center">
                                @if($obyek->sts_compliance_id==1)
                                    @if($obyek->tgl_sts_pencatatan_compliance >= $obyek->loadComplianceProgram()->tgl_sampai)
                                        <img src="{{url('/img/cek2.png')}}" widt="15px" height="15px">
                                    @else
                                        <img src="{{url('/img/cek.png')}}" widt="15px" height="15px">
                                    @endif
                                @else
                                    <img src="{{url('/img/min.png')}}" widt="15px" height="15px">
                                @endif
                            </td>
                            <td align="center">
                                @if($obyek->sts_substantive_id==1)
                                    @if($obyek->tgl_sts_substantive_id >= $obyek->loadSubstantiveeProgram()->tgl_sampai)
                                        <img src="{{url('/img/cek2.png')}}" widt="15px" height="15px">
                                    @else
                                        <img src="{{url('/img/cek.png')}}" widt="15px" height="15px">
                                    @endif
                                @else
                                    <img src="{{url('/img/min.png')}}" widt="15px" height="15px">
                                @endif
                            </td>

                            <td align="center">
                                @if($obyek->sts_pencatatan_deskaudit==1)
                                    @if($obyek->tgl_sts_pencatatan_deskaudit >= $obyek->loadDeskAuditCatatan()->tgl_sampai)
                                        <img src="{{url('/img/cek2.png')}}" widt="15px" height="15px">
                                    @else
                                        <img src="{{url('/img/cek.png')}}" widt="15px" height="15px">
                                    @endif
                                @else
                                    <img src="{{url('/img/min.png')}}" widt="10px" height="10px">
                                @endif
                            </td>
                            <td align="center">
                                @if($obyek->sts_pencatatan_compilance==1)
                                    @if($obyek->tgl_sts_pencatatan_compliance >= $obyek->loadComplianceCatatan()->tgl_sampai)
                                        <img src="{{url('/img/cek2.png')}}" widt="15px" height="15px">
                                    @else
                                        <img src="{{url('/img/cek.png')}}" widt="15px" height="15px">
                                    @endif
                                @else
                                    <img src="{{url('/img/min.png')}}" widt="10px" height="10px">
                                @endif
                            </td>
                            <td align="center">
                                @if($obyek->sts_pencatatan_substantive==1)
                                    @if($obyek->tgl_sts_pencatatan_substantive >= $obyek->loadSubstantiveCatatan()->tgl_sampai)
                                        <img src="{{url('/img/cek2.png')}}" widt="15px" height="15px">
                                    @else
                                        <img src="{{url('/img/cek.png')}}" widt="15px" height="15px">
                                    @endif
                                @else
                                    <img src="{{url('/img/min.png')}}" widt="10px" height="10px">
                                @endif
                            </td>
                            <td align="center">
                                @if($obyek->sts_pencatatan_deskaudit==1 && $obyek->sts_pencatatan_deskaudit==1 && $obyek->sts_pencatatan_compliance==1)
                                   100%
                               

                                @elseif($obyek->sts_pencatatan_deskaudit==1 && $obyek->sts_pencatatan_deskaudit==1 && $obyek->sts_pencatatan_compliance!=1)
                                    66%
                                

                                @elseif($obyek->sts_pencatatan_deskaudit==1 && $obyek->sts_pencatatan_deskaudit!=1 && $obyek->sts_pencatatan_compliance!=1)
                                    33%
                               

                                @elseif($obyek->sts_pencatatan_deskaudit!=1 && $obyek->sts_pencatatan_deskaudit!=1 && $obyek->sts_pencatatan_compliance!=1)
                                    0%
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>      
    </div> 

@endsection


