@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1"  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th >No</th>
                  <th>Obyekid</th>
                  <th>Nama Unit</th>
                  <th>Keterangan</th>
                 
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($items as $o)
                   
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->kode }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            @if($o->unit_id==1)Subdit
                            @elseif($o->unit_id==3)Divisi
                            @elseif($o->unit_id==4)Dinas
                            @elseif($o->unit_id==5)Direktorat
                            @else Seksi
                            @endif

                          </td>
                          
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
  .btn{margin:3px;}
</style>
@push('link-java')
  <script>
  
    $(function () {
      $('#example1').DataTable({
        'lengthChange' : true,
        'autoWidth': false,
        'paging': true,
        "columnDefs": [
          { "width": "5%", "targets":0 },
          { "width": "10%", "targets":1 },
          { "width": "55%", "targets": 2 },
          { "width": "10%", "targets": 3},
        ]      
      })
      
    })
  </script>
@endpush