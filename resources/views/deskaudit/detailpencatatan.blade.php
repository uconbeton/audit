@extends('layout.app')
@section('konten')

<div class="col-xs-12">
    <div class="box">
        <div class="box-header"></div>
        <!-- /.box-header -->
            <div class="box-body">
                
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="15%">Pokok Materi</th>
                    <th>Langkah Kerja</th>
                </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @foreach(get_deskaudit($obyek['id']) as $det_deskaudit=> $o)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $o->pokok_materi }}</td>
                            <td>
                              <table width="100%">
                                  <tr >
                                    <td style="background: #ccccef" width="10%" rowspan="2"></td>
                                    <td style="background: #ccccef" rowspan="2"  align="center"><h5><b>Langkah Kerja</b></h5></td>
                                    <td style="background: #ccccef" rowspan="2" align="center" width="35%"><h5><b>Catatan</b></h5></td>
                                    <td style="background: #ccccef" colspan="2" align="center" width="20%"><h5><b>Tanggal</b></h5></td>
                                    <td style="background: #ccccef" rowspan="2" align="center" width="8%"><h5><b>File</b></h5></td>
                                  </tr>
                                  <tr>
                                    <td style="background: #ccccef" align="center" width="12%"><b>Rencana</b></td>
                                    <td style="background: #ccccef" align="center" width="12%"><b>Aktual</b></td>
                                  </tr>
                                @php $x = 1; @endphp
                                @foreach( get_langkahkerja($o['id']) as $det_langkahkerja => $langkahkerja )
                                    
                                    
                                    <tr>
                                        <td width="5%" valign="top" style="padding-top:8px;padding-left:3px;background:#dae2ec;border:solid 2px #fff"><span class="btn btn-sm btn-warning" id="set" data-toggle="modal" data-target="#modal{{$det_deskaudit}}{{ $det_langkahkerja }}"><i class="glyphicon glyphicon-search"></i>Proses</span></td>
                                        <td valign="top"  style="padding-top:8px;padding-left:10px;background:#dae2ec;border:solid 1px #fff">{{ $x++ }}.{{ $langkahkerja->langkah_kerja }}</td>
                                        <td valign="top" style="padding-top:8px;padding-left:10px;background:#dae2ec;border:solid 1px #fff">
                                        @if(is_null($langkahkerja->pencatatan))
                                            <i class="ii">Belum ada catatan</i>
                                        @else
                                            {!! $langkahkerja->pencatatan !!}
                                        @endif
                                        </td>
                                        <td valign="top" style="padding-top:8px;padding-left:4px;background:#dae2ec;border:solid 1px #fff"><span class="btn btn-sm btn-info">{{ $langkahkerja->tgl_rencana }}</span></td>
                                        <td valign="top" style="padding-top:8px;padding-left:4px;background:#dae2ec;border:solid 1px #fff">
                                            
                                            @if(is_null($langkahkerja->tgl_aktual))
                                                <i class="ii">Belum ada </i>
                                            @else
                                                <span class="btn btn-sm btn-info">
                                                    {{ $langkahkerja->tgl_aktual }}
                                                </span>
                                            @endif
                                        </td>
                                        <td valign="top" style="padding-top:8px;padding-left:4px;background:#dae2ec;border:solid 1px #fff">
                                            <a href="{{url('/deskaudit/file/'.$langkahkerja->id.'/'.$langkahkerja->obyek_id)}}"><span class="btn btn-sm btn-success"><i class="glyphicon glyphicon-zoom-out"></i> File</span></a>
                                        </td>
                                     </tr>
                                    
                                        <div class="modal fade" id="modal{{$det_deskaudit}}{{ $det_langkahkerja }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Pencatatan Langkah {{ $langkahkerja->langkah_kerja }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" enctype="multipart/form-data" action="{{ url('/deskaudit/insertpencatatan/'.$langkahkerja->id)}}">
                                                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                        <input class="form-control pull-right" type="hidden" name="id[]" readonly  value="{{ $langkahkerja->id }}" placeholder="Nama Obyek">
                                                        <input class="form-control pull-right" type="hidden" name="obyek_id" readonly  value="{{ $o->obyek_id }}" placeholder="Nama Obyek">
                                                        <label>Keterangan :</label>
                                                        <textarea id="editor{{$det_deskaudit}}{{ $det_langkahkerja }}" name="pencatatan[]" rows="10" cols="80">{!! $langkahkerja->pencatatan !!}</textarea>
                                                        <label>Tanggal :</label>
                                                        <div class="input-group date">
                                                          @if(is_null($langkahkerja->tgl_aktual))
                                                            <input type="text"   id="datepicker{{ $det_deskaudit }}{{ $det_langkahkerja }}" class="form-control pull-right" value="{{ date('m/d/Y') }}" name="tgl_aktual[]" >
                                                          @else
                                                          <input type="text"   id="datepicker{{ $det_deskaudit }}{{ $det_langkahkerja }}" class="form-control pull-right" value="{{ date('m/d/Y',strtotime($langkahkerja->tgl_aktual)) }}" name="tgl_aktual[]" >
                                                          @endif
                                                        </div>
                                                        @if($obyek->sts_pencatatan_deskaudit==1)
                                                            
                                                        @else
                                                            <div class="box-footer">
                                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                            </div>
                                                        @endif
                                                    </form>
                                                </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="modalview{{$det_deskaudit}}{{ $det_langkahkerja }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">File</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="{{Storage::disk('public')->url($langkahkerja->files) }}" width="100%">
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                            
                                @endforeach
                              </table>
                            </td>
                           
                        </tr>
                        
                    @endforeach
                </tbody>
                
                </table>
            </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection
<style>
    .btn {
        padding:1px;
        margin:5px;
    }
    .ii{
        color:red;
    }
</style>
@push('link-java')
    <script>
        @foreach(get_deskaudit($obyek['id']) as $det_deskaudit=> $o)
            @foreach( get_langkahkerja($o['id']) as $det_langkahkerja => $langkahkerja )
               
                
                    
               
                    $(function (){
                        CKEDITOR.replace('editor{{ $det_deskaudit }}{{ $det_langkahkerja }}')
                        //bootstrap WYSIHTML5 - text editor
                        $('.textarea').wysihtml5()

                        $('#datepicker{{ $det_deskaudit }}{{ $det_langkahkerja }}').datepicker({
                            autoclose: true
                        });   
                    });
               

                
            @endforeach
        @endforeach

       $('#datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
        })
        
    </script>
@endpush
