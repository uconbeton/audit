@extends('layout.app')
@section('konten')

<div class="col-xs-12">
    <div class="box">
        <div class="box-header"></div>
        <!-- /.box-header -->
            <div class="box-body">
                
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="20%">Pokok Materi</th>
                    <th>Langkah Kerja dan Pencatatan</th>
                    <th>File</th>
                </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @foreach(get_deskaudit($obyeks['id']) as $det_deskaudit=> $o)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $o->pokok_materi }}</td>
                            <td>
                              <table width="100%">
                                    <tr >
                                        <td style="background: #ccccef" rowspan="2"  align="center"  width="30%"><h5><b>Langkah Kerja</b></h5></td>
                                        <td style="background: #ccccef" rowspan="2" align="center"><h5><b>Catatan</b></h5></td>
                                        <td style="background: #ccccef" colspan="2" align="center" width="20%"><h5><b>Tanggal</b></h5></td>
                                        <td style="background: #ccccef" rowspan="2" align="center" width="8%"><h5><b>File</b></h5></td>
                                    </tr>
                                    <tr>
                                        <td style="background: #ccccef" align="center" width="12%"><b>Rencana</b></td>
                                        <td style="background: #ccccef" align="center" width="12%"><b>Aktual</b></td>
                                    </tr>
                                @php $x = 1; @endphp
                                @foreach( get_langkahkerja($o['id']) as $det_langkahkerja => $langkahkerja )
                                    
                                    <tr>
                                        <td valign="top"  style="padding-top:8px;padding-left:10px;background:#dae2ec;border:solid 1px #fff">{{ $x++ }}.{{ $langkahkerja->langkah_kerja }}</td>
                                        <td valign="top" style="padding-top:8px;padding-left:10px;background:#dae2ec;border:solid 1px #fff">
                                            @if(is_null($langkahkerja->pencatatan))
                                                <i class="ii">Belum ada catatan</i>
                                            @else
                                                {!! $langkahkerja->pencatatan !!}
                                            @endif
                                        </td>
                                        <td valign="top" style="padding-top:8px;padding-left:4px;background:#dae2ec;border:solid 1px #fff"><span class="btn btn-sm btn-info">{{ $langkahkerja->tgl_rencana }}</span></td>
                                        <td valign="top" style="padding-top:8px;padding-left:4px;background:#dae2ec;border:solid 1px #fff">
                                            
                                            @if(is_null($langkahkerja->tgl_aktual))
                                                <i class="ii">Belum ada </i>
                                            @else
                                                <span class="btn btn-sm btn-info">
                                                    {{ $langkahkerja->tgl_aktual }}
                                                </span>
                                            @endif
                                        </td>
                                        <td valign="top" style="padding-top:8px;padding-left:4px;background:#dae2ec;border:solid 1px #fff">
                                            <a href="{{url('/deskaudit/file/'.$langkahkerja->id.'/'.$langkahkerja->obyek_id)}}"><span class="btn btn-sm btn-success"><i class="glyphicon glyphicon-zoom-out"></i> File</span></a>
                                        </td>
                                    </tr>
                                    
                                        <div class="modal fade" id="modal{{$det_deskaudit}}{{ $det_langkahkerja }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Pencatatan Langkah {{ $langkahkerja->langkah_kerja }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" action="{{ url('/deskaudit/insertpencatatan/'.$langkahkerja->id)}}">
                                                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                        <input class="form-control pull-right" type="hidden" name="id[]" readonly  value="{{ $langkahkerja->id }}" placeholder="Nama Obyek">
                                                        <input class="form-control pull-right" type="hidden" name="obyek_id" readonly  value="{{ $o->obyek_id }}" placeholder="Nama Obyek">
                                                        <label>Keterangan :</label>
                                                        <textarea id="editor{{$det_deskaudit}}{{ $det_langkahkerja }}" name="pencatatan[]" rows="10" cols="80">{!! $langkahkerja->pencatatan !!}</textarea>
                                                        @if($obyeks->sts_pencatatan_deskaudit==1)
                                                            
                                                        @else
                                                            <div class="box-footer">
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                            </div>
                                                        @endif
                                                    </form>
                                                </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="modalview{{$det_deskaudit}}{{ $det_langkahkerja }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">File</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="{{Storage::disk('public')->url($langkahkerja->files)}}" width="100%">
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                @endforeach
                              </table>
                            </td>
                           
                        </tr>
                        
                    @endforeach
                </tbody>
                
                </table>
            </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection
<style>
    .btn {
        padding:1px;
        margin:5px;
    }
    .ii{
        color:red;
    }
</style>
@push('link-java')
    <script>
        @foreach(get_deskaudit($obyeks['id']) as $getdet_deskaudit=> $o)
            @foreach( get_langkahkerja($o['id']) as $getdet_langkahkerja => $getlangkahkerja )
                @if($o->id==$getlangkahkerja->deskaudit_id)
                    $(function (){
                        CKEDITOR.replace('editor{{ $getdet_deskaudit }}{{ $getdet_langkahkerja }}')
                        //bootstrap WYSIHTML5 - text editor
                        $('.textarea').wysihtml5()
                       
                    });
                @endif
            @endforeach
        @endforeach
    </script>
@endpush
