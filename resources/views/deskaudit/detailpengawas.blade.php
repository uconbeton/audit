@extends('layout.app')
@section('konten')
<div class="col-xs-12">
    <div class="box">
        <div class="box-header"></div>
        <!-- /.box-header -->
            <div class="box-body">
                
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="20%">Pokok Materi</th>
                    <th>Langkah Kerja</th>
                   
                </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @foreach($deskaudit as $o)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $o->pokok_materi }}</td>
                            <td>
                             <table>
                                @php $x = 1; @endphp
                                @foreach( $langkahkerjas as $langkahkerja )
                                    @if($o->id==$langkahkerja->deskaudit_id)
                                    <tr>
                                        <td width="5%">{{ $x++ }}.&nbsp;</td>
                                        <td>{{ $langkahkerja->langkah_kerja }}</td>
                                    </tr>
                                    @endif
                                @endforeach
                             </table>
                            </td>
                            
                            
                        </tr>
                    @endforeach
                </tbody>
                
                </table>
            </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection
<style>
    .btn {
        padding:1px;
        margin:5px;
    }
</style>