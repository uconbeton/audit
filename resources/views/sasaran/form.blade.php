@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="/sasaran/insert">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>FORM SASARAN</B>
      </div>
      <div class="box-body">

        <div class="form-group">
            <label>Kode Obyek:</label>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-danger" onclick="show()">Pilih</button>
              </div>
              <input type="hidden" id="obyek_id" name="obyek_id" readonly value="{{ old('obyek_id') }}" class="form-control">
              <input type="text" id="kode_obyek" name="kode_obyek" readonly value="{{ old('kode_obyek') }}" class="form-control">
            </div>
          </div>

        <div class="form-group">
            <label>Nama Obyek:</label>
            <input class="form-control pull-right" type="text" readonly id="nama_obyek" name="nama_obyek" value="{{ old('nama_obyek') }}" placeholder="Nama Obyek">
        </div>
          
        <label>Dasar Sasaran :</label>
        <textarea id="editor1" name="resiko" rows="1" cols="80">{{ old('resiko') }}</textarea>
        <!-- Date range -->

        
        

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
    </div>
</div>
</form>
@endsection


<script src="{{url('/bower_components/jquery.min.js')}}"></script>



<script>
  function show() 
    {
      window.open("{{ url('popupobyek/2')}}", "list", "width=800,height=600");
    }
  function showpeng() 
    {
      window.open("{{ url('popuppj/3')}}", "list", "width=800,height=420");
    }
  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }
  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
