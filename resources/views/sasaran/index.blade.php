@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              
              {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
              <a href="{{ url('sasaran/form') }}"><button class="btn btn-primary"> <span class="fa fa-plus"></span> Tambah</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Nama Obyek</th>
                  <th>Tujuan</th>
                  <th>Waktu</th>
                  <th>Divisi</th>
                  <th width="13%">Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>{{ $o->tujuan }}</td>
                          <td>{{ $o->waktu }}</td>
                          <td>
                          {{ $o->divisi['nama'] }}
                        </td>
                          <td>
                            <form action="" method="post">
                              
                            <a href="{{ url('obyek/edit/'.$o->id.'') }}" class=" btn btn-sm btn-primary">Edit</a>
                              <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                            </form>  
                          </td>
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection