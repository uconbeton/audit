@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="post" action="{{ url('/compliance/insert_revisipencatatan/'.$obyeks->id)}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>REVISI NO: {{ $obyeks->kode }}</B>
              </div>
              <div class="box-body">
                <div class="form-group">
                    <label>Unit:</label>
                    <input class="form-control pull-right" type="hidden" name="id"  readonly value="{{ $obyeks->id }}" placeholder="Nama Obyek">
                      <input class="form-control pull-right" type="text"  readonly value="{{ $obyeks->unit['nama'] }}" placeholder="Nama Obyek">
                </div>
                <div class="form-group">
                    <label>Unit Kerja:</label>
                      <input class="form-control pull-right" type="text" readonly  value="{{ $obyeks->unit_kerja['nama'] }}" placeholder="Nama Obyek">
                </div>
                <div class="form-group">
                    <label>Nama Obyek:</label>
                      <input class="form-control pull-right" type="text" readonly  value="{{ $obyeks->nama }}" placeholder="Nama Obyek">
                </div>
                <label>Deskripsi Revisi :</label>
                <textarea id="editor2" name="keterangan" rows="10" cols="80">{{ old('keterangan') }}</textarea>

                
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
  </div>
</form>
@endsection


<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  $(document).ready(function(){
      $(".nav-tabs a").click(function(){
          $(this).tab('show');
      });
  });
  </script>

<script>
  

    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
