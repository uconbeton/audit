@extends('layout.app')
@section('konten')
<div class="col-xs-12">
    <div class="box">
        <div class="box-header"></div>
        <!-- /.box-header -->
            <div class="box-body">
                
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="20%">Pokok Materi</th>
                    <th>Langkah Kerja</th>
                    <th width="7%">Act</th>
                </tr>
                </thead>
                <tbody>
                    @php $no = 1; @endphp
                    @foreach($compliances as $o)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $o->pokok_materi }}</td>
                            <td>
                              <table>
                                @php $x = 1; @endphp
                                @foreach( $langkahkerjas as $langkahkerja )
                                    @if($o->id==$langkahkerja->compliance_test_id)
                                    <tr>
                                        <td width="5%"><a href="{{ url('compliance/delete_langkah/'.$langkahkerja->id.'') }}" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-sm btn-danger" style="padding: 3px;font-size: 10px;margin: 3px;"><span class="fa fa-remove"></span></a></td>
                                        <td width="5%">{{ $x++ }}.&nbsp;</td>
                                        <td>{{ $langkahkerja->langkah_kerja }}</td>
                                    </tr>
                                    @endif
                                @endforeach
                              </table>
                            </td>
                            
                            <td>
                                <form action="" method="post">
                                    <a href="{{ url('compliance/edit/'.$o->id.'') }}" class="btn btn-sm btn-info">Edit</a>
                                    <a href="{{ url('compliance/delete/'.$o->id.'') }}" class="btn btn-sm btn-danger">Delete</a>
                                </form>  
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
                </table>
            </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection
<style>
    .btn {
        padding:1px;
        margin:5px;
    }
</style>