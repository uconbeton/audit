@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<form method="post" action="{{ url('/compliance/update/'.$ComplianceTest->id)}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>FORM DESKAUDIT</B>
              </div>
              <div class="box-body">
                <label>Pokok Materi:</label><br>'
                <textarea  name="pokok_materi" rows="4" cols="100%" style="width:100%">{{ $ComplianceTest->pokok_materi }}</textarea>
                <br>
                <br>
                <span class="btn btn-primary" id="add">Tambah Langkah Kerja</span><input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                <br>
                
                <div id="container" style="margin-left:20px;">
                    @foreach($langkahkerja as $key=>$langkah)
                        <label>Langkah Kerja :</label><br>
                        <textarea  name="langkahkerja[]" rows="2" cols="100%" style="width:100%">{{ $langkah->langkah_kerja }}</textarea>
                        <div class="input-group date">
                          <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" value="{{ date('m/d/Y',strtotime($langkah->tgl_rencana)) }}" name="tgl_rencana[]" id="datepickers{{$key}}">
                        </div>
                        <script>
                          $('#datepickers'{{$key}}).datepicker({
                            autoclose: true
                          }) 
                        </script>
                    @endforeach
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <input type="hidden" name="id" value="{{ $ComplianceTest->id }}">
                </div>
              </div>
            </div>
        </div>
  </div>
  

</form>
@endsection


<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function() {
     var count = 0;
     $("#add").click(function(){
        count += 1;
          $('#container').append(
                '<br><label>Langkah Kerja dan Tanggal Rencana:</label><br>'
                +'<textarea  name="langkahkerja[]" rows="2" cols="100%" style="width:100%"></textarea>'
                +'<div class="input-group date">'
                    +'<div class="input-group-addon">'
                        +'<i class="fa fa-calendar"></i>'
                    +'</div>'
                    +'<input type="text" class="form-control pull-right" value="{{ date('m/d/Y') }}" name="tgl_rencana[]" id="datepicker'+count+'">'
                +'</div>'
                
          );
            //alert(count);
            $('#jum').val(count);
            $('#datepicker'+count).datepicker({
              autoclose: true
            }) 
            
    });

    // $('#add').click();
    // $('#add').click();
  
                        
    $(".remove_item").live('click', function (ev) {
      if (ev.type == 'click') {
        $(this).parents(".records").fadeOut();
        $(this).parents(".records").remove();
        count -= 1;
        $('#jum').val(count);
      }
    });
  });
       
                    
</script>

<script>
  function obyekaudit() 
    {
      window.open("{{ url('popobyekaudit')}}", "list", "width=800,height=420");
    }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
