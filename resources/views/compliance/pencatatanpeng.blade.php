@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
             </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Kode<br>Obyek</th>
                  <th>Nama Obyek</th>
                  <th width="20%">Tujuan</th>
                  <th>Tim Audit</th>
                  <th width="7%">Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->kode }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            @php $x = 1; @endphp
                            @foreach( $tujuans as $tujuan )
                              @if($o->id==$tujuan->obyek_id)
                                {{ $x++ }}.{{ $tujuan->tujuan }}<br>
                              @endif
                            @endforeach
                          </td>
                          <td>
                            <ul>
                              <li>{{ $o->pengawas['nama'] }} (Pengawas)</li>
                              <li>{{ $o->ketua_tim['nama'] }} (Ketua Tim)</li>
                              
                              @foreach( $obyektims as $obyektim )
                                @if($o->id == $obyektim->obyek_id)
                                  <li>{{ $obyektim->karyawan['nama'] }} (anggota) </li>
                                @endif
                                
                              @endforeach
                              </ul>
                          </td>
                          <td style="vertical-align: top">
                                <form action="" method="post" style="margin-top: 0em;">
                                    @if($o->sts_pencatatan_compliance=='0')
                                      <a href="{{ url('compliance/setujuipencatatan/'.$o->id.'') }}" onclick="return confirm('Yakin ingin menyetujui audit ini?')" class=" btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span>&nbsp;Setujui</a>
                                      <a href="{{ url('compliance/formrevisipencatatan/'.$o->id.'') }}" class=" btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp;Revisi</a>
                                    @endif
                                    @if($o->sts_pencatatan_compliance=='1')
                                      <span class=" btn btn-sm btn-success">Disetujui</span>
                                    @endif
      
                                    @if($o->sts_pencatatan_compliance=='2')
                                      <br><span class="btn btn-sm btn-warning" id="set">Menunggu Revisi</span>
                                    @endif
                                    <a href="{{ url('compliance/detailpencatatanpeng/'.$o->id.'') }}" class=" btn btn-sm btn-info" title="Detail Pokok Materi"><span class="glyphicon glyphicon-search"></span>&nbsp;Detail Pencatatan</a>
                                </form>  
                              </td>
                      </tr>
                        
                  @endforeach
                </tbody>
                
              </table>
              {{ $obyeks->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
    .btn{margin:3px;}
  </style>
  @push('link-java')
    <script>
    
      $(function () {
        $('#example1').DataTable({
          'lengthChange' : false,
          'autoWidth': false,
          'paging': false,
          "columnDefs": [
            { "width": "25%", "targets": 2 },
            { "width": "30%", "targets": 3},
            { "width": "24%", "targets": 4 },
            { "width": "10%", "targets": 5 }
          ]      
        })
        
      })
    </script>
  @endpush
