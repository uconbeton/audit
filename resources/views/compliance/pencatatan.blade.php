@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
             </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Kode<br>Obyek</th>
                  <th>Nama Obyek</th>
                  <th width="20%">Tujuan</th>
                  <th>Tim Audit</th>
                  <th width="7%">Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $key=> $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->kode }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            @php $x = 1; @endphp
                            @foreach( $tujuans as $tujuan )
                              @if($o->id==$tujuan->obyek_id)
                                {{ $x++ }}.{{ $tujuan->tujuan }}<br>
                              @endif
                            @endforeach
                          </td>
                          <td>
                            <ul>
                              <li>{{ $o->pengawas['nama'] }} (Pengawas)</li>
                              <li>{{ $o->ketua_tim['nama'] }} (Ketua Tim)</li>
                              
                              @foreach( $obyektims as $obyektim )
                                @if($o->id == $obyektim->obyek_id)
                                  <li>{{ $obyektim->karyawan['nama'] }} (anggota) </li>
                                @endif
                                
                              @endforeach
                              </ul>
                          </td>
                          <td style="vertical-align: top">
                                <form action="" method="post" style="margin-top: 0em;">
                                      @if(is_null($o->sts_pencatatan_compliance))
                                        <a href="{{ url('compliance/detailpencatatan/'.$o->id.'') }}" class=" btn btn-sm btn-info" title="Detail Pokok Materi"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Catatan Compliance</a>
                                      @endif

                                      @if($o->sts_pencatatan_compliance=='0')
                                      <a href="{{ url('compliance/detailpencatatanpeng/'.$o->id.'') }}" class=" btn btn-sm btn-info" title="Detail Pokok Materi"><span class="glyphicon glyphicon-search"></span>&nbsp;view Catatan</a>
                                      <span class=" btn btn-sm btn-warning">Menunggu<br>Persetujuan</span>
                                      @endif

                                      @if($o->sts_pencatatan_compliance=='1')
                                        <a href="{{ url('compliance/detailpencatatanpeng/'.$o->id.'') }}" class=" btn btn-sm btn-info" title="Detail Pokok Materi"><span class="glyphicon glyphicon-search"></span>&nbsp;view Catatan</a>
                                        <span class=" btn btn-sm btn-success">Disetujui</span>
                                      @endif
        
                                      @if($o->sts_pencatatan_compliance=='2')
                                        <span class="btn btn-sm btn-warning" id="set" data-toggle="modal" data-target="#modal{{$key}}"><span class="glyphicon glyphicon-search"></span>&nbsp;View Revisi</span>
                                        <a href="{{ url('compliance/detailpencatatan/'.$o->id.'') }}" class=" btn btn-sm btn-info" title="Detail Pokok Materi"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Catatan Compliance</a>
                                      @endif
                                   
                                </form>  
                              </td>
                      </tr>
                      <div class="modal fade" id="modal{{$key}}">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Revisi Pencatatan Compliance</h4>
                            </div>
                            <div class="modal-body">
                              @foreach($revisis as $revisi)
                                @if($revisi->obyek_id==$o->id)
                                  <div style="width:100%;height:20px;"><b>Tanggal : {{ $revisi->tanggal }}</b></div>
                                  <div style="width:100%;background:#f8f8fb;padding:2px;"> {!! $revisi->keterangan !!}</div>
                                @endif
                              @endforeach
                            </div>
                            
                          </div>
                        </div>
                      </div> 
                  @endforeach
                </tbody>
                
              </table>
              {{ $obyeks->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
    .btn{margin:3px;}
  </style>
  @push('link-java')
    <script>
    
      $(function () {
        $('#example1').DataTable({
          'lengthChange' : false,
          'autoWidth': false,
          'paging': false,
          "columnDefs": [
            { "width": "25%", "targets": 2 },
            { "width": "30%", "targets": 3},
            { "width": "24%", "targets": 4 },
            { "width": "10%", "targets": 5 }
          ]      
        })
        
      })
    </script>
  @endpush
