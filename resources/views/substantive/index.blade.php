@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
             </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1" style="font-size:13px" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th >No</th>
                  <th>Kode<br>Obyek</th>
                  <th>Nama Obyek</th>
                  <th >Tujuan</th>
                  <th>Tim Audit</th>
                  <th valign="top">Status</th>
                  <th>Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $key=> $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->kode }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            @php $x = 1; @endphp
                            @foreach( $tujuans as $tujuan )
                              @if($o->id==$tujuan->obyek_id)
                                {{ $x++ }}.{{ $tujuan->tujuan }}<br>
                              @endif
                            @endforeach
                          </td>
                          <td><ul>
                              <li>{{ $o->pengawas['nama'] }} (Pengawas)</li>
                              <li>{{ $o->ketua_tim['nama'] }} (Ketua Tim)</li>
                              
                              @foreach( $obyektims as $obyektim )
                                @if($o->id == $obyektim->obyek_id)
                                  <li>{{ $obyektim->karyawan['nama'] }} (anggota) </li>
                                @endif
                                
                              @endforeach
                              </ul>
                          </td>
                          <td>
                            <form action="" method="post">
                              @if($o->sts_substantive_id=='1')
                                <span class=" btn btn-sm btn-success">Disetujui</span>
                              @endif

                              @if($o->sts_substantive_id=='0')
                                <span class=" btn btn-sm btn-warning">Menunggu<br>Persetujuan</span>
                              @endif

                              @if(is_null($o->sts_substantive_id))
                              <a href="{{ url('substantive/kirimsubstantive/'.$o->id.'') }}"><span class="btn btn-sm btn-success">Proses</span></a>
                              @endif

                              @if($o->sts_substantive_id=='2')
                               <span class="btn btn-sm btn-warning" id="set" data-toggle="modal" data-target="#modal{{$key}}"><span class="glyphicon glyphicon-search"></span>&nbsp;View Revisi</span>
                               <a href="{{ url('substantive/kirimsubstantive/'.$o->id.'') }}"><span class="btn btn-sm btn-success">Proses</span></a>
                              @endif
                            </form>  
                              
                           
                          </td>
                          <td>
                              <form action="" method="post">
                                @if(is_null($o->sts_substantive_id))
                                  <a href="{{ url('substantive/form/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbspTambah Materi</a>
                                @endif
                                @if($o->sts_substantive_id=='0' || $o->sts_substantive_id=='2')
                                <a href="{{ url('substantive/form/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbspTambah Materi</a>
                                @endif
                                <a href="{{ url('substantive/detailsubstantive/'.$o->id.'') }}" class=" btn btn-sm btn-info" title="Detail Pokok Materi"><span class="glyphicon glyphicon-search"></span>&nbsp;Detail Substantive</a>
                              </form> 
                          </td>
                      </tr>
                    
                   
                      <div class="modal fade" id="modal{{$key}}">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Revisi Substantive</h4>
                            </div>
                            <div class="modal-body">
                              @foreach($revisis as $revisi)
                                @if($revisi->obyek_id==$o->id)
                                  <div style="width:100%;height:20px;"><b>Tanggal : {{ $revisi->tanggal }}</b></div>
                                  <div style="width:100%;background:#f8f8fb;padding:2px;"> {!! $revisi->keterangan !!}</div>
                                @endif
                              @endforeach
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      
                  @endforeach
                </tbody>
                
              </table>
              {{ $obyeks->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        
@endsection
<style>
  .btn{font-size:8px;margin:2px;height:30px;}
  
</style>
@push('link-java')
  <script>
  
    $(function () {
      $('#example1').DataTable({
        'lengthChange' : false,
        'autoWidth': false,
        'paging': false,
        "columnDefs": [
          { "width": "20%", "targets": 2 },
          { "width": "35%", "targets": 3 },
          { "width": "40%", "targets": 4 }
        ]      
      })
      
    })
  </script>
@endpush


