@extends('layout.app')
@section('konten')
<style>
    th{text-align:center;background: #e8e8f5;}
    td{padding:5px;}
</style>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
    
@include('layout.dashboard')
    
    <div class="col-md-12" id="tampilshow2">
        <div class="box box-primary">
          <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Sebaran PKAT Berdasarkan Kode Audit</h3>

              <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
          </div>
          <div class="box-body">
                  <div id="deskaudit" style="height: 300px;color:#000"></div>
          </div>
     
      </div>
    </div>

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header" style="background: #e8e8de">
            <B>Keterangan Kode Audit </B>
            </div>
            <div class="box-body">
                <table width="100%" border="0" >
                    
                        @foreach($kategori as $r=>$kate)
                        <tr>
                            <td width="3%"><b>{{ $r+1 }}.</b></td>
                            <td width="5%"><b>{{ $kate->kode }}</b></td>
                            <td  width="60%">= {{ $kate->keterangan }} </td>
                            <td><b>{{ $jumket[$r]}}</b></td>
                        </tr>   
                        @endforeach
                    
                    
                </table>
            </div>
            
        </div> 
    </div>
    

    


@endsection
<!-- jQuery 3 -->
<script src="{{url('/bower_components/jquery/dist/jquery.min.js')}}"></script>




<script>
  
  $(function () {
    
    /*
     * Flot Interactive Chart
     * -----------------------
     */
    // We use an inline data source in the example, usually data would
    // be fetched from a server
    var data = [], totalPoints = 100

    function getRandomData() {

      if (data.length > 0)
        data = data.slice(1)

      // Do a random walk
      while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y    = prev + Math.random() * 10 - 5

        if (y < 0) {
          y = 0
        } else if (y > 100) {
          y = 100
        }

        data.push(y)
      }

      // Zip the generated y values with the x values
      var res = []
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
      }

      return res
    }

    var interactive_plot = $.plot('#interactive', [getRandomData()], {
      grid  : {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color     : '#3c8dbc'
      },
      lines : {
        fill : true, //Converts the line chart to area chart
        color: '#3c8dbc'
      },
      yaxis : {
        min : 0,
        max : 100,
        show: true
      },
      xaxis : {
        show: true
      }
    })

    var updateInterval = 500 //Fetch data ever x milliseconds
    var realtime       = 'on' //If == to on then fetch data every x seconds. else stop fetching
    function update() {

      interactive_plot.setData([getRandomData()])

      // Since the axes don't change, we don't need to call plot.setupGrid()
      interactive_plot.draw()
      if (realtime === 'on')
        setTimeout(update, updateInterval)
    }

    //INITIALIZE REALTIME DATA FETCHING
    if (realtime === 'on') {
      update()
    }
    //REALTIME TOGGLE
    $('#realtime .btn').click(function () {
      if ($(this).data('toggle') === 'on') {
        realtime = 'on'
      }
      else {
        realtime = 'off'
      }
      update()
    })
    /*
     * END INTERACTIVE CHART
     */

    /*
     * LINE CHART
     * ----------
     */
    //LINE randomly generated data

    var sin = [], cos = []
    // for (var i = 0; i < 14; i += 0.5) {
    //   sin.push([i, Math.sin(i)])
    //   cos.push([i, Math.cos(i)])
    // }



    @foreach($kategori as $kat=>$kate)
      sin.push([{{$kat++}}, Math.sin('{{$kat++}}')])
      cos.push([{{$kat++}}, Math.cos({{$kat++}})])

    @endforeach
    var line_data1 = {
      data : sin,
      color: '#3c8dbc'
    }
    var line_data2 = {
      data : cos,
      color: '#00c0ef'
    }

    //----------------------------------------------------------kategori--------------------------------

    $.plot('#kategoriii', [line_data1], {
      grid  : {
        hoverable  : true,
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0,
        lines     : {
          show: true
        },
        points    : {
          show: true
        }
      },
      lines : {
        fill : false,
        color: ['#3c8dbc', '#f56954']
      },
      yaxis : {
        show: true
      },
      xaxis : {
        show: true
      }
    })




    //Initialize tooltip on hover
    $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
      position: 'absolute',
      display : 'none',
      opacity : 0.8
    }).appendTo('body')
    $('#line-chart').bind('plothover', function (event, pos, item) {

      if (item) {
        var x = item.datapoint[0].toFixed(2),
            y = item.datapoint[1].toFixed(2)

        $('#line-chart-tooltip').html(item.series.label + ' of ' + x + ' = ' + y)
          .css({ top: item.pageY + 5, left: item.pageX + 5 })
          .fadeIn(200)
      } else {
        $('#line-chart-tooltip').hide()
      }

    })
    /* END LINE CHART */

    /*
     * FULL WIDTH STATIC AREA CHART
     * -----------------
     */
    var areaData = [[2, 88.0], [3, 93.3], [4, 102.0], [5, 108.5], [6, 115.7], [7, 115.6],
      [8, 124.6], [9, 130.3], [10, 134.3], [11, 141.4], [12, 146.5], [13, 151.7], [14, 159.9],
      [15, 165.4], [16, 167.8], [17, 168.7], [18, 169.5], [19, 168.0]]
    $.plot('#area-chart', [areaData], {
      grid  : {
        borderWidth: 0
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color     : '#00c0ef'
      },
      lines : {
        fill: true //Converts the line chart to area chart
      },
      yaxis : {
        show: false
      },
      xaxis : {
        show: false
      }
    })

    /* END AREA CHART */

    /*
     * BAR CHART
     * ---------
     */
//-----------------------------------------bar chart--------------------------------------------------------------
var kateg = {
      //data : [['January', 10], ['February', 8], ['March', 4], ['April', 13], ['May', 17], ['June', 9]],
      data : [
                    @foreach($kategori as $kat=>$kate)
                                
                         ['{{$kate->kode}}',{{ round($jumket[$kat]*$persenkat,0)}}],
                                
                          
                    @endforeach
      ],
      color: 'green'
    }
    $.plot('#kategori', [kateg], {
      grid  : {
        borderWidth: 1,
        borderColor: 'red',
        tickColor  : 'red'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */




//-----------------------------------------bar chart--------------------------------------------------------------
    var bar_data = {
      //data : [['January', 10], ['February', 8], ['March', 4], ['April', 13], ['May', 17], ['June', 9]],
      data : [
                    @foreach($obyeks as $no=> $obyek)
                        
                            @foreach($kodifikasi as $kode)
                                
                                    ['{{$kode->kodifikasi}}',{{ ($ssubrekomendasi->shift()['count'] + $ssubsubrekomendasi->shift()['count'])}}],
                                
                            @endforeach
                    
                    @endforeach
      ],
      color: '#3c8dbc'
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */

    /*
     * DONUT CHART
     * -----------
     */
    //-------------------------- deskaudit-------------------------------------------------------------------------
    var deskData = [
      @foreach($kategori as $kit=>$kat)
        @if($kat->kode=='RP')
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: '#3c8dbc' },
        @elseif($kat->kode=='NN')
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: 'yellow' },
        @elseif($kat->kode=='KP' || $kat->kode=='KN')
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: 'red' },
        @elseif($kat->kode=='MN')
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: 'green' },
        @elseif($kat->kode=='IN')
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: 'aqua' },
        @elseif($kat->kode=='AP')
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: 'grey' },
        @else
          { label: '{{$kat->kode}}', data: {{round($jumket[$kit]*$persenkat,0)}}, color: 'blue' },
        @endif
     
      @endforeach
    ]
    $.plot('#deskaudit', deskData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

    var deskcatData = [
      { label: 'Selesai', data: {{$pendesk}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$pendesknot}}, color: 'red' }
    ]
    $.plot('#deskcatatan', deskcatData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })
    

    //---------------------------------------compliance-----------------------------------------------------------
    var comData = [
      { label: 'Selesai', data: {{$procom}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$procomnot}}, color: 'red' }
    ]
    $.plot('#compliance', comData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })


    var comcatData = [
      { label: 'Selesai', data: {{$pencom}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$pencomnot}}, color: 'red' }
    ]
    $.plot('#comcatatan', comcatData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

    //---------------------------------------substantive-----------------------------------------------------------
    var comData = [
      { label: 'Selesai', data: {{$prosubs}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$prosubsnot}}, color: 'red' }
    ]
    $.plot('#substantive', comData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })


    var comcatData = [
      { label: 'Selesai', data: {{$pensubs}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$pensubsnot}}, color: 'red' }
    ]
    $.plot('#subsmcatatan', comcatData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

  })

  /*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #000; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
</script>