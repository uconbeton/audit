@extends('layout.app')
@section('konten')
<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Date picker</h3>
      </div>
      <div class="box-body">
        <!-- Date -->
        <div class="form-group">
          <label>Nama Obyek</label>

          <div class="input-group date">
            <div class="input-group-addon">
             
            </div>
            <input type="text" class="form-control pull-right">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->
        <textarea id="editor1" name="editor1" rows="10" cols="80">
            This is my textarea to be replaced with CKEditor.
        </textarea>
        <!-- Date range -->
        <div class="form-group">
          <label>Date range:</label>

          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="reservation">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date and time range -->
        <div class="form-group">
          <label>Date and time range:</label>

          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-clock-o"></i>
            </div>
            <input type="text" class="form-control pull-right" id="reservationtime">
          </div>
          <!-- /.input group -->
        </div>
        <!-- /.form group -->

        <!-- Date and time range -->
        <div class="form-group">
          <label>Date range button:</label>

          <div class="input-group">
            <button type="button" class="btn btn-default pull-right" id="daterange-btn">
              <span>
                <i class="fa fa-calendar"></i> Date range picker
              </span>
              <i class="fa fa-caret-down"></i>
            </button>
          </div>
        </div>
        <!-- /.form group -->

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- iCheck -->
    <div class="box box-success">
      <div class="box-header">
        <h3 class="box-title">iCheck - Checkbox &amp; Radio Inputs</h3>
      </div>
      <div class="box-body">
        <!-- Minimal style -->

        <!-- checkbox -->
        <div class="form-group">
          <label>
            <input type="checkbox" class="minimal" checked>
          </label>
          <label>
            <input type="checkbox" class="minimal">
          </label>
          <label>
            <input type="checkbox" class="minimal" disabled>
            Minimal skin checkbox
          </label>
        </div>

        <!-- radio -->
        <div class="form-group">
          <label>
            <input type="radio" name="r1" class="minimal" checked>
          </label>
          <label>
            <input type="radio" name="r1" class="minimal">
          </label>
          <label>
            <input type="radio" name="r1" class="minimal" disabled>
            Minimal skin radio
          </label>
        </div>

        <!-- Minimal red style -->

        <!-- checkbox -->
        <div class="form-group">
          <label>
            <input type="checkbox" class="minimal-red" checked>
          </label>
          <label>
            <input type="checkbox" class="minimal-red">
          </label>
          <label>
            <input type="checkbox" class="minimal-red" disabled>
            Minimal red skin checkbox
          </label>
        </div>

        <!-- radio -->
        <div class="form-group">
          <label>
            <input type="radio" name="r2" class="minimal-red" checked>
          </label>
          <label>
            <input type="radio" name="r2" class="minimal-red">
          </label>
          <label>
            <input type="radio" name="r2" class="minimal-red" disabled>
            Minimal red skin radio
          </label>
        </div>

        <!-- Minimal red style -->

        <!-- checkbox -->
        <div class="form-group">
          <label>
            <input type="checkbox" class="flat-red" checked>
          </label>
          <label>
            <input type="checkbox" class="flat-red">
          </label>
          <label>
            <input type="checkbox" class="flat-red" disabled>
            Flat green skin checkbox
          </label>
        </div>

        <!-- radio -->
        <div class="form-group">
          <label>
            <input type="radio" name="r3" class="flat-red" checked>
          </label>
          <label>
            <input type="radio" name="r3" class="flat-red">
          </label>
          <label>
            <input type="radio" name="r3" class="flat-red" disabled>
            Flat green skin radio
          </label>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Many more skins available. <a href="http://fronteed.com/iCheck/">Documentation</a>
      </div>
    </div>
    <!-- /.box -->
  </div>
@endsection