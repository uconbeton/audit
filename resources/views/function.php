<?
class Database{
    private $host='localhost';
    private $user='root';
    private $password='';
    private $db='neti';

    function __construct() {
        $this->mysqli   = new mysqli('localhost','root','','neti');
    }
	public function query($query){
		return $this->mysqli->query($query);
	}

    public function sel_produk(){
        $query="SELECT * from produk ";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_produkpop(){
        $query="SELECT distinct(nama_produk) as nama from produk ";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_jenis(){
        $query="SELECT * from jenis_produk ";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_jum_produk(){
        $query="SELECT count(nama_produk) as nama from relokasi ";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_jum_produkpop($nm,$id){
        $query="SELECT sum(qty) as jum from relokasi where nama_produk='".$nm."' and id_lokasi='".$id."'";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_relokasi($qw){
        $query="SELECT * from lokasi where nama_jenis='".$qw."'";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_pemesanan(){
        $query="SELECT * from pemesanan a, jenis_produk b where a.id_jenis=b.id_jenis";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_pengiriman(){
        $query="SELECT * from pemesanan a, jenis_produk b where a.id_jenis=b.id_jenis";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_lokasi($nm){
        $query="SELECT * from lokasi where nama_jenis='".$nm."'";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_mlokasi(){
        $query="SELECT * from lokasi ";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_jumlokasi($qw){
        $query="SELECT sum(Qty) as jum from relokasi where id_lokasi='".$qw."'";
        return $this->mysqli->query($query);
		
    }
	
	public function sel_jumlokasipop($qw,$nm){
        $query="SELECT sum(Qty) as jum from relokasi where nama_lokasi='".$qw."' and nama_produk='".$nm."'";
        return $this->mysqli->query($query);
		
    }

}