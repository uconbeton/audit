@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>Substantive Program</B>
      </div>
      <div class="box-body" id="prin" >

        <table   width="100%" style="border-collapse:collapse">
            <tr>
                <td  class="tds" align="left" width="5%">
                <img src="{{url('/img/ks.png')}}" width="50px" height="60px">
                
                </td>
                <td  class="tds" align="center" width="1%">
                
                </td>
                <td  class="tds"  align="left" valign="top"><br>
                <H3 style="margin-top:0px">PT. KRAKATAU STEEL (Persero) Tbk<br>INTERNAL AUDIT </H3><br>
                </td>
            </tr>
            <tr>
                <td  class="tds"  colspan="3" align="center" style="border:solid 1px #000">
                    <b>SUBSTANTIVE TEST PROGRAM</b>
                </td>
            </tr>
            <tr>
                <td  class="tds"  colspan="3" align="center" >
                    <br><br>
                </td>
            </tr>
        </table>
        <table   width="100%" style="border-collapse:collapse">
            <tr>
                <td  class="tds"  align="left" width="15%">Unit Organisasi</td>
                <td  class="tds"  align="center" width="5%" ><b>:</b></td>
                <td  class="tds"  align="left" valign="top"><b>{{ $obyeks->unit_kerja['nama']}}</b></td>
                <td  class="tds"  align="left" width="15%" valign="top">No KKA : III-4</td>
                
            </tr>
            <tr>
                <td  class="tds"  align="left">Obyek Audit</td>
                <td  class="tds"  align="center" ><b>:</b></td>
                <td  class="tds"  align="left" colspan="2" valign="top"><b>{{ $obyeks->nama}}</b></td>
            </tr>
            <tr>
                <td  class="tds"  align="left">Priode Audit</td>
                <td  class="tds"  align="center" ><b>:</b></td>
                <td  class="tds"  align="left" colspan="2" valign="top"><b>
                    @if(date('m',strtotime($obyeks->waktu))=='01' || date('m',strtotime($obyeks->waktu))=='02' || date('m',strtotime($obyeks->waktu))=='03')
                        Triwulan I
                    @elseif(date('m',strtotime($obyeks->waktu))=='04' || date('m',strtotime($obyeks->waktu))=='05' || date('m',strtotime($obyeks->waktu))=='06')
                        Triwulan II
                    @elseif(date('m',strtotime($obyeks->waktu))=='07' || date('m',strtotime($obyeks->waktu))=='08' || date('m',strtotime($obyeks->waktu))=='09')
                        Triwulan III
                    @else
                        Triwulan IV 
                    @endif
                   
                </b></td>
            </tr>
            <tr>
                <td  class="tds"  colspan="4" align="center" >
                    <br><br>
                </td>
            </tr>
            <tr>
                <td  class="tds"  colspan="4" align="left" >
                    <b>TUJUAN AUDIT :</b>
                </td>
            </tr>
            <tr>
                    <td  class="tds"  colspan="4" align="left" >
                       @foreach($tujuan as $x=>$tujuan)
                            {{$x+1}}. {{ $tujuan->tujuan}}<br>
                       @endforeach
                    </td>
                </tr>
          </table>

          <table   width="100%" border="1"style="border-collapse:collapse">
            <tr>
                <td class="tdsh" rowspan="2" width="3%">No</td>
                <td class="tdsh" rowspan="2" width="15%">TEMUAN</td>
                <td class="tdsh" rowspan="2">LANGKAH KERJA</td>
                <td class="tdsh" rowspan="2" width="15%">Dilaksanakan</td>
                <td class="tdsh" colspan="2" width="14%">Jangka Waktu</td>
           </tr>
            <tr>   
                <td class="tdsh" width="7%">Renc</td>
                <td class="tdsh" width="7%">Akt</td>
            </tr>
            @foreach($substantive as $ds=>$substantive)
                <tr>
                    <td class="tds" valign="top">{{ $ds+1}}</td>
                    <td class="tds" valign="top">{{ $substantive->pokok_materi}}</td>
                    <td class="tds" valign="top">
                        {{-- Dapatkan data-data / dokumen-dokumen terkait Kegiatan {{ $obyeks->nama }},sbb : --}}
                        <table width="100%">
                        @foreach($langkahkerja->where('substantive_test_id',$substantive->id) as $xl=>$langkahsubs)
                            <tr>
                                <td width="5%" valign="top">{{$langkahsubs->urut+1}}.</td>
                                <td>{{$langkahsubs->langkah_kerja}}</td>
                            </tr>
                        @endforeach
                        </table>
                    </td>
                    <td class="tds" valign="top">
                        @foreach($obyektims as $tim)
                            <b>-</b> {{ $tim->karyawan['nama']}}<br>
                        @endforeach
                    </td>
                    <td class="tds" valign="top">{{ date('m/d/y',strtotime($obyeks->loadSubstantiveeProgram()->tgl_sampai))}}</td>
                    <td class="tds" valign="top">{{ date('m/d/y',strtotime($obyeks->tgl_sts_substantive_id))}}</td>
                </tr>
            @endforeach
          </table>
          <table   width="100%" border="0"style="border-collapse:collapse">
            <tr>
                <td></td>
                <td  width="40%"> Create By {{ $obyeks->ketua_tim['nama']}} <br>
                                    Check/Approve by {{ $obyeks->pengawas['nama']}}  {{ date('d/m/Y',strtotime($obyeks->tgl_sts_substantive_id))}}
                </td>
            </tr>
          </table>
      </div>
      <hr>
      <div class="box-header" style="background: #e8e8de">
        <B>Substantive Pencatatan</B>
      </div>
      <div class="box-body" id="prin2" >
            <table   width="100%" style="border-collapse:collapse">
                <tr>
                    <td  class="tds" align="left" width="5%">
                    <img src="{{url('/img/ks.png')}}" width="50px" height="60px">
                    
                    </td>
                    <td  class="tds" align="center" width="1%">
                    
                    </td>
                    <td  class="tds"  align="left" valign="top"><br>
                    <H3 style="margin-top:0px">PT. KRAKATAU STEEL (Persero) Tbk<br>INTERNAL AUDIT </H3><br>
                    </td>
                </tr>
                <tr>
                    <td  class="tds"  colspan="3" align="center" style="border:solid 1px #000">
                            <b>CATATAN HASIL AUDIT<br>TAHAPAN AUDIT : FIELD AUDIT - SUBSTANTIVE TEST</b>
                    </td>
                </tr>
                <tr>
                    <td  class="tds"  colspan="3" align="center" >
                        <br><br>
                    </td>
                </tr>
            </table>
            <table   width="100%" style="border-collapse:collapse">
                
                <tr>
                    <td  class="tds"  align="left" width="15%">Unit Organisasi</td>
                    <td  class="tds"  align="center" width="5%"><b>:</b></td>
                    <td  class="tds"  align="left" valign="top"><b>{{ $obyeks->unit_kerja['nama']}}</b></td>
                    <td  class="tds"  align="left" width="15%" valign="top">No KKA : III-5</td>
                </tr>
          
                <tr>
                    <td  class="tds"  align="left">Obyek Audit</td>
                    <td  class="tds"  align="center" ><b>:</b></td>
                    <td  class="tds"  align="left" colspan="2"  valign="top"><b>{{ $obyeks->nama}}</b></td>
                </tr>
                <tr>
                    <td  class="tds"  align="left">Priode Audit</td>
                    <td  class="tds"  align="center" ><b>:</b></td>
                    <td  class="tds"  align="left" colspan="2" valign="top"><b>
                        @if(date('m',strtotime($obyeks->waktu))=='01' || date('m',strtotime($obyeks->waktu))=='02' || date('m',strtotime($obyeks->waktu))=='03')
                            Triwulan I
                        @elseif(date('m',strtotime($obyeks->waktu))=='04' || date('m',strtotime($obyeks->waktu))=='05' || date('m',strtotime($obyeks->waktu))=='06')
                            Triwulan II
                        @elseif(date('m',strtotime($obyeks->waktu))=='07' || date('m',strtotime($obyeks->waktu))=='08' || date('m',strtotime($obyeks->waktu))=='09')
                            Triwulan III
                        @else
                            Triwulan IV 
                        @endif
                    
                    </b></td>
                </tr>
                <tr>
                    <td  class="tds"  colspan="4" align="center" >
                        <br><br>
                    </td>
                </tr>
                
            </table>

          <table   width="100%" border="1"style="border-collapse:collapse">
            <tr>
                <td class="tdsh"  width="3%">No</td>
                <td class="tdsh"  width="40%">Pokok Materi</td>
                <td class="tdsh" >Keterangan</td>
                
            </tr>
                @foreach($langkahnya as $nol=>$langkahnya)
                    <tr>
                        <td class="tdrsr" width="5%">{{$nol+1}}.</td>
                        <td class="tdrsr">{{$langkahnya->langkah_kerja}}</td>
                        <td class="tdrsr">{!! $langkahnya->pencatatan !!}</td>
                    </tr>
                @endforeach
          </table>
          <table   width="100%" border="0"style="border-collapse:collapse">
            <tr>
                <td></td>
                <td  width="40%"> Create By 
                                @foreach($obyektims as $obyektim)
                                    {{$obyektim->karyawan['nama']}}, 
                                @endforeach
                                {{ $obyeks->ketua_tim['nama']}} <br>
                                    Check By {{ $obyeks->ketua_tim['nama']}} <br>
                                    Approve by {{ $obyeks->pengawas['nama']}}  {{ date('d/m/Y',strtotime($obyeks->tgl_sts_pencatatan_substantive))}}
                </td>
            </tr>
        </table>
      </div>
      <table width="100%">
     
        <tr>
          <td align="center" style="padding:10px"> 
            <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Substantive Program</button>
            <button   class="btn btn-sm btn-success" type="submit" onclick="print2('prin2');"><span class="fa fa-print"></span>Cetak Substantive Pencatatan</button>
          </td>
        </tr>
     
      </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title></title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      setTimeout(function(){
        mywindow.print();
        mywindow.close();
      },250);
      return true;
    }

    function print2(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title></title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      setTimeout(function(){
        mywindow.print();
        mywindow.close();
      },250);
      return true;
    }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:12;text-align: justify;line-height: 1.4em;}
.tdrs{padding:5px;font-size:12;text-transform:capitalize;text-align: justify;font-weight:bold;word-spacing: 3px;}
.tdrsr{padding:5px;font-size:12;text-transform:capitalize;text-align: justify;word-spacing: 3px;}
#prin{margin-left:10%;margin-right:10%;width:80%;}
#prin2{margin-left:10%;margin-right:10%;width:80%;}
.tds{padding:5px;}
.tdsh{padding:5px;font-weight:bold;}
</style>