@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1"  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th >No</th>
                  <th>Kode<br>Obyek</th>
                  <th>Nama Obyek</th>
                  <th>Tujuan</th>
                  <th>Tim Audit</th>
                  <th>Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->unit_kerja['kode_unit'] }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            
                            @php $x = 1; @endphp
                            @foreach( $tujuans as $tujuan )
                              @if($o->id==$tujuan->obyek_id)
                                {{ $x++ }}.{{ $tujuan->tujuan }}<br>
                              @endif
                            @endforeach
                          </td>
                          <td>
                            <ul>
                              <li>{{ $o->pengawas['nama'] }} (Pengawas)</li>
                              <li>{{ $o->ketua_tim['nama'] }} (Ketua Tim)</li>
                              
                              @foreach( $obyektims as $obyektim )
                                @if($o->id == $obyektim->obyek_id)
                                  <li>{{ $obyektim->karyawan['nama'] }} (anggota) </li>
                                @endif
                                
                              @endforeach
                              @if(is_null($o->Lha['nik_create']))

                              @else
                                <li><b>{{$o->nama_create['nama']}} (Last Update  {{ date('d/m/y h:i',strtotime($o->Lha['tanggal']))}})</b></li>
                              @endif
                            </ul>
                          </td>
                          <td>
                            <form action="" method="post">
                              @if(Auth::user()->posisi_id==1 || Auth::user()->posisi_id==5)
                                  @if(is_null($o->sts_lha))
                                    <a href="{{ url('lha/form/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-pencil">Create Laporan</span></a>
                                  @else
                                      @if($o->sts_pengantar==1)

                                      @else
                                          <a href="{{ url('lha/edit/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-pencil">Create Laporan</span></a>
                                          
                                      @endif
                                          <a href="{{ url('lha/laporan/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="fa fa-folder-open">View Laporan</span></a>
                                      
                                      @if(is_null($o->sts_setujui1))
                                          @if($o->nik_ketua_tim==Auth::user()->nik)
                                           <a href="{{ url('lha/prosessetujui1/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Review 1</span></a> 
                                          @endif
                                      @elseif($o->sts_setujui1==2)
                                          @if($o->nik_pengawas==Auth::user()->nik)
                                            <a href="{{ url('lha/setujui1/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Setujui Ke-1</span></a>
                                          @endif
                                      @else
                                          @if(is_null($o->sts_setujui2))
                                              @if($o->nik_ketua_tim==Auth::user()->nik)
                                                <a href="{{ url('lha/prosessetujui2/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Review 2</span></a> 
                                              @endif
                                          @elseif($o->sts_setujui2==2)
                                              @if($o->nik_pengawas==Auth::user()->nik)
                                                <a href="{{ url('lha/setujui2/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Setujui Ke-2</span></a>
                                              @endif
                                          @else
                                              @if(is_null($o->sts_setujui3))
                                                @if($o->nik_ketua_tim==Auth::user()->nik)
                                                  <a href="{{ url('lha/prosessetujui3/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Review 3</span></a> 
                                                @endif
                                              @elseif($o->sts_setujui3==2)
                                                @if($o->nik_pengawas==Auth::user()->nik)
                                                  <a href="{{ url('lha/tujuan/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Memo</span></a>
                                                  <a href="{{ url('lha/setujui3/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Setujui Ke-3</span></a>
                                                @endif
                                              @else
                                                @if(Auth::user()->posisi_id==5)
                                                <a href="{{ url('lha/edit/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-pencil">Create Laporan</span></a>
                                                  <a href="{{ url('lha/tembusan/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Pengantar LHA</span></a>
                                                  <a href="{{ url('lha/slip/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Slip</span></a>
                                                @endif 
                                              @endif
                                          @endif
                                        @endif
                                    @endif

                              @else
                                    @if($o->sts_lha=='')
                                       <a href="{{ url('lha/form/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-pencil">Create Laporan</span></a>
                                    @else
                                      <a href="{{ url('lha/edit/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-pencil">Create Laporan</span></a>
                                      <a href="{{ url('lha/laporan/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="fa fa-folder-open">View Laporan</span></a>
                                        @if(is_null($o->sts_setujui1))
                                          @if($o->nik_ketua_tim==Auth::user()->nik)
                                           <a href="{{ url('lha/prosessetujui1/'.$o->id.'') }}" class=" btn btn-sm btn-dangers"><span class="fa fa-check-square"> Review 1</span></a> 
                                          @endif
                                        @else
                                          @if(is_null($o->sts_setujui2))
                                            @if($o->nik_ketua_tim==Auth::user()->nik)
                                              <a href="{{ url('lha/prosessetujui2/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Review 2</span></a> 
                                            @endif
                                          @else
                                              @if(is_null($o->sts_setujui3))
                                                @if($o->nik_ketua_tim==Auth::user()->nik)
                                                  <a href="{{ url('lha/prosessetujui3/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Review 3</span></a> 
                                                @endif
                                                  <a href="{{ url('lha/tujuan/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Memo</span></a>
                                                  {{-- <a href="{{ url('lha/setujui3/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square"> Setujui Ke-3</span></a> --}}
                                              @else
                                                  {{-- <a href="{{ url('lha/tembusan/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Pengantar LHA</span></a> --}}
                                                  <a href="{{ url('lha/deskaudit/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Deskaudit</span></a>
                                                  <a href="{{ url('lha/compliance/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Compliance</span></a>
                                                  <a href="{{ url('lha/substantive/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Substantive</span></a>
                                                  {{-- <a href="{{ url('lha/compliance/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"> Compliance</span></a> --}}
                                              @endif
                                          @endif
                                        @endif
                                    @endif
                              @endif  
                            </form>  
                          </td>
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
              {{ $obyeks->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
  .btn{margin:3px;}
</style>
@push('link-java')
  <script>
  
    $(function () {
      $('#example1').DataTable({
        'lengthChange' : false,
        'autoWidth': false,
        'paging': false,
        "columnDefs": [
          { "width": "25%", "targets": 2 },
          { "width": "30%", "targets": 3},
          { "width": "24%", "targets": 4 },
          { "width": "10%", "targets": 5 }
        ]      
      })
      
    })
  </script>
@endpush