@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B></B>
      </div>
      <div class="box-body" id="prin">

          <table  border="0" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:5%;margin-right:5%">
            <tr>
              <td colspan="3" align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="110px">
                 <br><br>
              </td>
            </tr>
            <tr>
              <td class="tdr" width="20%"> </td>
              <td class="tdr"  width="60%"></td>
              <td class="tdr"> <b>No.KKA  :I-4</b></td>
            </tr>
            <tr>
                <td class="tdr" colspan="3" style="border:solid 2px #000;text-align:center"><b>SLIP PENYELESAIAN LHA</b> </td>
            </tr>
        </table>
        <table  border="0" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:5%;margin-right:5%">
            <tr>
              <td class="tdr" valign="top" width="20%">Unit Organisasi </td>
              <td class="tdr" valign="top"  width="4%">:</td>
              <td class="tdr" valign="top">{{$obyeks->unit_kerja['nama']}}</td>
            </tr>
            <tr>
              <td class="tdr" valign="top" >Obyek Audit </td>
              <td class="tdr" valign="top" >:</td>
              <td class="tdr" valign="top">{{$obyeks->nama}}</td>
            </tr>
            <tr>
              <td class="tdr" valign="top" >Kode Audit </td>
              <td class="tdr" valign="top" >:</td>
              <td class="tdr" valign="top">{{ $kategori->kode}}/{{ $obyeks->unit_kerja['kode_unit'] }}/{{ date('m/Y',strtotime($obyeks->waktu)) }}</td>
            </tr>
            <tr>
              <td class="tdr" valign="top" >Surat Tugas Nomor </td>
              <td class="tdr" valign="top" >:</td>
              <td class="tdr" valign="top">
                @if($obyeks->ttd==1)   
                 {{$obyeks->nomor_surat_tugas}}/IA-ST/{{ date('m',strtotime($obyeks->waktu))}}.{{ date('y',strtotime($obyeks->waktu))}}
                @else
                  <p style="margin-left:-20%"> </p>
                   
                @endif

              </td>
            </tr>
        </table>
        <table  border="0" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:5%;margin-right:5%">
          <tr>
            <td class="tdr" valign="top" width="25%">Tim Audit </td>
            <td class="tdr" valign="top"  width="15%">Pengawas </td>
            <td class="tdr" valign="top">: {{$obyeks->pengawas['nama']}}</td>
          </tr>
          <tr>
            <td class="tdr" valign="top"></td>
            <td class="tdr" valign="top">Ketua Tim </td>
            <td class="tdr" valign="top">: {{$obyeks->ketua_tim['nama']}}</td>
          </tr>
          @foreach($obyekstim as $tim)
            <tr>
              <td class="tdr" valign="top"></td>
              <td class="tdr" valign="top">Anggota </td>
              <td class="tdr" valign="top">: {{$tim->karyawan['nama']}}</td>
            </tr>

          @endforeach
          <tr>
            <td class="tdr" valign="top">Waktu Audit</td>
            <td class="tdr" valign="top">Rencana </td>
            <td class="tdr" valign="top">: {{date('d/m/y',strtotime($jadwal1->tgl_mulai))}} Sd {{date('d/m/y',strtotime($jadwal2->tgl_sampai))}}</td>
          </tr>
          <tr>
            <td class="tdr" valign="top"></td>
            <td class="tdr" valign="top">Realisasi   </td>
            <td class="tdr" valign="top">: {{date('d/m/y',strtotime($obyeks->tgl_sts_pencatatan_substantive))}} Sd {{date('d/m/y',strtotime($obyeks->tgl_sts_pengantar))}}</td>
          </tr>
        </table>
        <table  border="1" cellpadding="5" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:5%;margin-right:5%">   
          <tr >
            <td rowspan="2" class="tdre" align="center" valign="top" width="5%">No</td>
            <td rowspan="2" class="tdre" align="center" valign="top">URAIAN </td>
            <td rowspan="2" class="tdre" align="center" valign="top"  width="20%">NAMA<BR>PENANGGUNG<BR>JAWAB </td>
            <td  class="tdre" align="center" colspan="3" valign="top"  width="30%">JANGKA WAKTU </td>
            <td rowspan="2" class="tdre" align="center"  valign="top"  width="10%">Jumlah Hari</td>
          </tr>
          <tr >
            <td class="tdre" align="center" valign="top">Mulai </td>
            <td class="tdre" align="center" valign="top">Selesai </td>
            <td class="tdre" align="center" valign="top">Paraf </td>
          </tr>
          <tr>
              <td class="tdrrs" valign="top">1</td>
              <td class="tdrrs" valign="top">Penyusunan Konsep LHA</td>
              <td class="tdrrs" valign="top">Ketua Tim</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_sts_pencatatan_substantive))}}</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_revsetujui1))}}</td>
              <td class="tdrrs" valign="top"></td>
              <td class="tdrrs" valign="top">{{$selisih0}}</td>
          </tr>
          <tr>
              <td class="tdrrs" valign="top">2</td>
              <td class="tdrrs" valign="top">Review Oleh Pengawas</td>
              <td class="tdrrs" valign="top">Pengawas</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_revsetujui1))}}</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_sts_setujui1))}}</td>
              <td class="tdrrs" valign="top"></td>
              <td class="tdrrs" valign="top">{{$selisih}}</td>
          </tr>
          <tr>
              <td class="tdrrs" valign="top">3</td>
              <td class="tdrrs" valign="top">Review oleh Head of Internal Audit</td>
              <td class="tdrrs" valign="top">Head of Internal Audit</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_revsetujui2))}}</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_sts_setujui2))}}</td>
              <td class="tdrrs" valign="top"></td>
              <td class="tdrrs" valign="top">{{$selisih1}}</td>
          </tr>
          <tr>
              <td class="tdrrs" valign="top">4</td>
              <td class="tdrrs" valign="top">Finalisasi LHA</td>
              <td class="tdrrs" valign="top">Pengawas & Tim</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_revsetujui3))}}</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_sts_setujui3))}}</td>
              <td class="tdrrs" valign="top"></td>
              <td class="tdrrs" valign="top">{{$selisih2}}</td>
          </tr>
          <tr>
              <td class="tdrrs" valign="top">5</td>
              <td class="tdrrs" valign="top">Tanda Tangan Head of Internal Audit</td>
              <td class="tdrrs" valign="top">Head of Internal Audit</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_sts_setujui3))}}</td>
              <td class="tdrrs" valign="top">{{date('d/m/y',strtotime($obyeks->tgl_sts_pengantar))}}</td>
              <td class="tdrrs" valign="top"></td>
              <td class="tdrrs" valign="top">{{$selisih3}}</td>
          </tr>
          <tr>
              <td class="tdrrs" valign="top" colspan="6">JUMLAH HARI AUDIT</td>
              <td class="tdrrs" valign="top">{{$total}}</td>
          </tr>
          
        </table>
          
          
      
      </div>
      <hr>
      <table width="100%">
     
        <tr>
          <td align="center" style="padding:10px"> 
            <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Slip</button>
            
          </td>
        </tr>
     
      </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title></title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      setTimeout(function(){
        mywindow.print();
        mywindow.close();
      },250);
      return true;
  }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
 
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:16;text-align: justify;line-height: 1.4em;}
.tdre{padding:5px;font-size:16;text-align: center;line-height: 1.4em;}
.tdrr{padding:5px;font-size:16;text-align: justify;line-height: 1.4em;border:solid 1px #000;}
.tdrrs{padding:5px;font-size:16;text-align: justify;line-height: 1.4em;border-left:solid 1px #000;border-right:solid 1px #000;}
.tdrs{padding:5px;font-size:16;text-transform:capitalize;text-align: justify;font-weight:bold;word-spacing: 3px;}
#prin{margin-left:10%;margin-right:10%;width:80%;}
</style>