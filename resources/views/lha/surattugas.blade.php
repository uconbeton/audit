@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>SURAT TUGAS</B>
      </div>
      <div class="box-body" id="prin">
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
            <tr>
              <td colspan="3" align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="150px"><br>
                 <h2 style="margin-bottom:4px"><u><b>&nbsp&nbsp;SURAT&nbsp&nbsp&nbsp&nbsp;TUGAS&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">Nomor : {{ $obyeks->no_surattugas }}/ST/GM/IA-KS/{{ date('Y',strtotime($obyeks->waktu))}}</h4>                  
              </td>
            </tr>
            <tr>
              <td class="tdr"></td>
              <td class="tdr"><br>Head of Internal Audit memberi tugas kepada :<br><br></td>
              <td class="tdr" width="15%"></td>
            </tr>
            <tr>
              <td width="12%" class="tdr"></td>
              <td class="tdr"></td>
              <td class="tdr" width="15%"></td>
            </tr>
          </table>
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma;margin-left:100px">
            <tr>
              <td  class="tdr" width="2%">1.</td>
              <td class="tdr" width="30%">{{ $obyeks->pengawas['nama'] }}</td>
              <td class="tdr" width="6%">NIK</td>
              <td class="tdr" width="10%">{{ $obyeks->pengawas['nik'] }}</td>
              <td class="tdr">(Pengawas)</td>
            </tr>
            <tr>
              <td  class="tdr" width="2%">2.</td>
              <td class="tdr">{{ $obyeks->ketua_tim['nama'] }}.</td>
              <td class="tdr">NIK</td>
              <td class="tdr">{{ $obyeks->ketua_tim['nik'] }}</td>
              <td class="tdr">(Ketua Tim)</td>
            </tr>
              @foreach($obyekstim as $key => $anggota)
                <tr>
                  <td  class="tdr" width="2%">{{ $key+3}}.</td>
                  <td class="tdr">{{ $anggota->karyawan['nama']}}</td>
                  <td class="tdr">NIK</td>
                  <td class="tdr">{{ $anggota->karyawan['nik'] }}</td>
                  <td class="tdr">(Anggota)</td>
                </tr>
              @endforeach
                    
          </table><br>
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma;margin-left:100px">
              <tr>
                <td class="tdr" colspan="3">Untuk melaksanakan audit terhadap  :</td>
              </tr>
              <tr>
                <td width="20%" class="tdr">Obyek Audit</td>
                <td class="tdr" width="3%">:</td>
                <td class="tdr" >{{ $obyeks->nama}}</td>
              </tr>
              <tr>
                  <td width="15%" class="tdr">Lokasi</td>
                  <td class="tdr" width="3%">:</td>
                  <td class="tdr" >Cilegon</td>
                </tr>
              <tr>
                <td class="tdr">Kode Audit</td>
                <td class="tdr">:</td>
              <td class="tdr">{{ $kategori->kode}}/{{ $obyeks->kode }}/{{ date('m/Y',strtotime($obyeks->waktu)) }}</td>
              </tr>
              <tr>
                <td class="tdr">Jumlah Hari Audit</td>
                <td class="tdr">:</td>
                <td class="tdr">{{ $obyeks->waktu_audit}}Hari</td>
              </tr>
              <tr>
                <td class="tdr">Catatan Penting</td>
                <td class="tdr">:</td>
                <td class="tdr">..........................................................................</td>
              </tr>
              <tr>
                <td class="tdr" colspan="3"><br><br>Demikian untuk dilaksanakan</td>
              </tr>
            </table>
            <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
              <tr align="center">
                <td class="tdr"width="60%"></td>
                <td class="tdr">Cilegon, {{ date('d F Y') }}<br>
                  @if($obyeks->sebagai_menyetujui=='Direktur Utama')
                    PT KRAKATAUSTEEL (Persero) Tbk.
                  @else
                    INTERNAL AUDIT
                  @endif<br><br><br><br><br>
                  <h3 style="margin-bottom:4px"><u><b>&nbsp&nbsp;{{ $obyeks->menyetujui}}&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">{{ $obyeks->sebagai_menyetujui}}</h4>
                </td>
              </tr>
            </table>
          <hr>
      
      </div>
      <table width="100%">
      @if(is_null($obyeks->menyetujui))
        <tr>
          <td align="left" style="padding:10px">
              <form method="post" action="{{ url('/lha/updatemenyetujui/'.$obyeks->id)}}">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-cog"></span> Proses</button>
                <select class="form-control" style="width: 20%;display:inline" name="sebagai_menyetujui"  tabindex="-1" aria-hidden="true">
                  <option value="">Pilih</option>
                  <option value="General Manager" @if ($obyeks->sebagai_menyetujui =='Manager') {{ 'selected' }} @endif>General Manager</option>
                  <option value="Direktur Utama" @if ($obyeks->sebagai_menyetujui =='Direktur Utama') {{ 'selected' }} @endif>Direktur Utama</option>
                 </select>
                <input class="form-control" type="text" name="menyetujui"  style="width: 30%;display:inline;" placeholder="Nama Penyetuju">
                
              </form>
              
          </td>
        </tr>
      @else
        <tr>
          <td align="left" style="padding:10px"> 
              <form method="post" action="{{ url('/lha/updatemenyetujui/'.$obyeks->id)}}" style="display:inline">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-cog"></span> Proses</button>
                <span   class="btn btn-success" style="width: 20%;display:inline;padding:8px" type="submit" onclick="print('prin');"><span class="fa fa-print"></span> Cetak Surat Tugas</span>
                <select class="form-control" style="width: 20%;display:inline" name="sebagai_menyetujui"  tabindex="-1" aria-hidden="true">
                  <option value="">Pilih</option>
                  <option value="Manager" @if ($obyeks->sebagai_menyetujui =='Manager') {{ 'selected' }} @endif>Manager</option>
                  <option value="Direktur Utama" @if ($obyeks->sebagai_menyetujui =='Direktur Utama') {{ 'selected' }} @endif>Direktur Utama</option>
                </select>
                <input class="form-control" type="text" name="menyetujui"  style="width: 30%;display:inline" placeholder="Nama Penyetuju">
              </form>
            
          </td>
        </tr>
      @endif
      </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      setTimeout(function(){
        mywindow.print();
        mywindow.close();
      },250);
      return true;
  }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:14;text-transform:capitalize;}
</style>