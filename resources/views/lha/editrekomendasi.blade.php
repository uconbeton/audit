@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<form method="post" action="{{ url('/lha/updaterekomendasi')}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                
                <B>Form Rekomendasi</B>
              </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                @foreach($rekomendasi as $no=>$rek)
                <input name="rekomendasi[]" type="hidden" value="{{$rek->id}}">
                    @if($no==0)
                        <li class="active"><a href="#tab{{$no}}" data-toggle="tab">Temuan {{$rek->nomor}}</a></li>
                    @else
                        <li ><a href="#tab{{$no}}" data-toggle="tab">Temuan {{$rek->nomor}}</a></li>
                    @endif
                @endforeach
                  
                 
                </ul>
                <div class="tab-content">
                  <!------------------------------------------------tab 1---->
                  @foreach($rekomendasi as $no=>$rek)
                    
                    @if($no==0)
                        <div class="tab-pane active" id="tab{{$no}}">
                    @else
                        <div class="tab-pane" id="tab{{$no}}">
                    @endif
                        <span class="btn btn-success btn-sm" id="add{{$no}}" style="margin:15px 1px 15px 15px"><i class="glyphicon glyphicon-plus"></i> Tambah Rekomendasi {{ $rek->nomor}}</span>
                        <a href=" {{ url('lha/edit/'.$obyeks->id) }}"><span class="btn btn-primary btn-sm" style="margin:15px 2px 15px 0px"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</span></a>
                        <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                        <br>
                        @foreach($subrekomendasi->where('rekomendasi_id', $rek->id) as $nosub=>$subrek)
                        <br>
                            <input type="hidden" name="sub_rekomendasi_id{{$no}}[]" value="{{$subrek->id}}">
                            <div class="box-body" style="background:#f5f5f5;margin:5px;">
                                <div class="box-header" style="background: #e8e8de">
                                  <a href="{{ url('lha/deleterekomendasi/'.$subrek->id) }}"><span class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-remove"></i></span></a>
                                  <a href=" {{ url('lha/formsub/'.$subrek->id) }}"><span class="btn btn-info btn-sm"><i class="glyphicon glyphicon-folder-open"></i></span></a>
                                  <b>Rekomendasi {{ $subrek->subnomor}}</b>
                                </div>
                                <input type="hidden" size="10" name="subnomor{{$no}}[]" value="{{ $subrek->subnomor}}"><br>
                                <label style="width:8%">PIC :</label>
                                <select  class="form-control btn-sm" style="width: 50%;margin-bottom:2px;display:inline" name="pic_id{{$no}}[]">
                                    @foreach($pics as $pic)
                                        <option value="{{ $pic->kode }}" @if ( $subrek->pic_id ==$pic->kode) {{ 'selected' }} @endif>{{ $pic->pimpinan }} {{ $pic->nama }}</option>
                                    @endforeach
                                </select><br>
                                <label style="width:8%">Kodifikasi :</label>
                                  <select class="form-control" id="kodefikasi{{$no}}{{$nosub}}" style="width:50%;margin-bottom:2px;display:inline" name="kodefikasi{{$no}}[]"> 
                                  @foreach($kodifikasi as $kodif)
                                    <option value="{{$kodif->kodifikasi}}" @if ( $subrek->kodifikasi ==$kodif->kodifikasi) {{ 'selected' }} @endif>[{{$kodif->kodifikasi}}] {{$kodif->kategori}}</option>
                                  @endforeach
                                </select><br>
                                <label id="label{{$no}}{{$nosub}}" style="width:8%">Nilai :</label>
                                <input type="text" size="10" id="nilai{{$no}}{{$nosub}}" name="nilai{{$no}}[]" value="{{$subrek->nilai}}" style="width:15%;margin-bottom:2px;display:inline"  class="form-control">
                                <input type="text" size="20" id="nilaicuren{{$no}}{{$nosub}}"  value="{{number_format($subrek->nilai)}}"style="width:15%;margin-bottom:2px;display:inline;background:#e7e7f9"  readonly class="form-control">
                                <textarea class="form-control" name="subrekomendasi{{$no}}[]" placeholder="Isi Rekomendasi....." cols="100" >{!! $subrek->subrekomendasi !!}</textarea><br>
                                
                            </div>
                            
                            
                        @endforeach
                        <div id="container{{$no}}" ></div>
                        
                        
                        
                    </div>
                    
                  @endforeach
                   

                  
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <input type="hidden" name="id" value="{{ $obyeks->id }}">
                    </div>
                </div>
            </div>
        </div>
  
</form>
@endsection

<style>
  .latarbelakang{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .sasaran{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .ruanglingkup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .pelaksanaan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .penutup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .kesimpulan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  #penjelasan{width: 100%; height: 600px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">
 @foreach($rekomendasi as $no=>$rek)
    $(document).ready(function() {
        var count = {{ $rek->loadJumRek()}};
        $("#add{{$no}}").click(function(){
            count += 1;
            $('#container{{$no}}').append(
                '<div class="box-body" style="background:#f5f5f5;margin:2px;">'
                  +'<input type="hidden" name="sub_rekomendasi_id{{$no}}[]" value="0">'
                    +'<div class="box-header" style="background: #e8e8de"><b>Rekomendasi {{ $rek->nomor}}.'+count+'</b></div>'
                        +'<input type="hidden" size="10" name="subnomor{{$no}}[]" value="{{ $rek->nomor}}.'+count+'"><br>'
                        +'<label style="width:8%">PIC :</label>'
                        +'<select  class="form-control" style="width:30%;margin-bottom:2px;display:inline" name="pic_id{{$no}}[]">'
                            @foreach($pics as $pic)
                              +'<option value="{{ $pic->kode }}">{{ $pic->pimpinan }} {{ $pic->nama }}</option>'
                            @endforeach
                        +'</select><br>'
                        +'<label style="width:8%">Kodifikasi :</label>'
                        +'<select class="form-control" id="kodefikasi{{$no}}'+count+'" style="width:50%;margin-bottom:2px;display:inline" name="kodefikasi{{$no}}[]"> '
                            @foreach($kodifikasi as $kodif)
                              +'<option value="{{$kodif->kodifikasi}}">[{{$kodif->kodifikasi}}] {{$kodif->kategori}}</option>'
                            @endforeach
                        +'</select><br>'
                        +'<label id="label{{$no}}'+count+'" style="width:8%">Nilai :</label>'
                        +'<input type="text" size="20" id="nilai{{$no}}'+count+'" name="nilai{{$no}}[]" style="width:15%;margin-bottom:2px;display:inline"  class="form-control">&nbsp;'
                        +'<input type="text" size="20" id="nilaicuren{{$no}}'+count+'"  style="width:15%;margin-bottom:2px;display:inline;background:#e7e7f9"  readonly class="form-control">'
                        +'<textarea class="form-control" name="subrekomendasi{{$no}}[]" placeholder="Isi Rekomendasi....." cols="100" ></textarea><br>'
                       
                +'</div>'
                    
            );
            function formatNumber(n) {
              return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }
            $('#nilai{{$no}}'+count).on('keyup', function() {
                var nilainya=this.value;
                  nr=formatNumber(nilainya);
                $('#nilaicuren{{$no}}'+count).val(nr);
               
            });
            
            $('#label{{$no}}'+count).hide()
            $('#nilai{{$no}}'+count).hide()
            $('#nilaicuren{{$no}}'+count).hide()

            $("#kodefikasi{{$no}}"+count).change(function(){
               var value=$(this).val();
               
               if(value=='03.C.01' || value=='03.C.02' || value=='03.C.03' || value=='04.E.01' || value=='04.E.02' || value=='04.E.03' || value=='05.A.03' || value=='07.X.01' || value=='07.X.02' ){
                  $('#label{{$no}}'+count).show()
                  $('#nilai{{$no}}'+count).show()
                  $('#nilaicuren{{$no}}'+count).show()
               }else{
                  $('#label{{$no}}'+count).hide()
                  $('#nilai{{$no}}'+count).hide()
                  $('#nilaicuren{{$no}}'+count).hide()
               }
               
            })
        });

       
        
   

    
    
  }); 
  $(document).ready(function() {
      @foreach($subrekomendasi->where('rekomendasi_id', $rek->id) as $nosub=>$subrek)
        function formatNumber(n) {
                return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

        $('#nilai{{$no}}{{$nosub}}').on('keyup', function() {
            var nilainya=this.value;
              nr=formatNumber(nilainya);
            $('#nilaicuren{{$no}}{{$nosub}}').val(nr);
            
        });

        $("#kodefikasi{{$no}}{{$nosub}}").change(function(){
            var value=$(this).val();
            var nil=$("#nilai{{$no}}{{$nosub}}").val();
            if(value=='03.C.01' || value=='03.C.02' || value=='03.C.03' || value=='04.E.01' || value=='04.E.02' || value=='04.E.03' || value=='05.A.03' || value=='07.X.01' || value=='07.X.02' ){
              $('#label{{$no}}{{$nosub}}').show()
              $('#nilai{{$no}}{{$nosub}}').show()
              $('#nilaicuren{{$no}}{{$nosub}}').show()
            }else{
              $('#label{{$no}}{{$nosub}}').hide()
              $('#nilai{{$no}}{{$nosub}}').hide()
              $('#nilaicuren{{$no}}{{$nosub}}').hide()
            }
            
        })
    
      @endforeach 
  });  
  @endforeach    
                    
</script>

<script>
  $(function () {
    $('.latarbelakang').wysihtml5()
  })

  $(function () {
    $('.sasaran').wysihtml5()
  })

  $(function () {
    $('.ruanglingkup').wysihtml5()
  })

  $(function () {
   $('.pelaksanaan').wysihtml5()
  })

  $(function () {
   $('.penutup').wysihtml5()
  })

  $(function () {
   $('.kesimpulan').wysihtml5()
  })
  
  $(function () {
    CKEDITOR.replace('penjelasan')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  // $(function () {
  //   CKEDITOR.replace('kesimpulan')
    //bootstrap WYSIHTML5 - text editor
  //   $('.textarea').wysihtml5()
  // })
</script>

<script>
  function unitkerja() 
    {
      var unit=$('#unit_id').val();
      window.open("../popunitkerja/"+unit+"", "list", "width=800,height=420");
    }

  function show() 
    {
      window.open("{{ url('popketuatim')}}", "list", "width=800,height=500");
    }
    
  function showpeng() 
    { var unit=$('#kategori_audit_id').val();
      if(unit==3 || unit==4 || unit==7){
        window.open("{{ url('popuppj/4')}}", "list", "width=800,height=420");
      }else{
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }
      
    }

  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }

  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
