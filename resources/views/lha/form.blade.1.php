@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<ul class="nav nav-tabs" style="margin-bottom:5px">
  <li class="active"><a href="#home">Obyek Audit</a></li>
  <li><a href="#menu1">Jadwal Pelaksanaan</a></li>
 </ul>



<form method="post" action="/obyek/insert">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">


    
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>FORM OBYEK</B>
              </div>
              <div class="box-body">

                <div class="form-group">
                  <label>Kode:</label>
                <input class="form-control pull-right" onkeypress="return hanyaAngka(event)" type="text" maxlength="10" name="kode" value="{{ old('kode') }}" placeholder="KOde Obyek">
                </div>

                <div class="form-group">
                    <label>Nama Obyek:</label>
                      <input class="form-control pull-right" type="text" name="nama"  value="{{ old('nama') }}" placeholder="Nama Obyek">
                  </div>
                  <label>Tujuan :</label>
                  <textarea id="editor1" name="tujuan" rows="10" cols="80">{{ old('tujuan') }}</textarea>
                <!-- Date range -->

                <div class="form-group">
                  <label>Divisi:</label>
                  <select class="form-control" style="width: 100%;" name="divisi_id"  tabindex="-1" aria-hidden="true">
                  
                    @foreach($items as $item)
                      <option value="{{ $item->id }}" @if (old('divisi_id') ==$item->id) {{ 'selected' }} @endif>{{ $item->nama }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label>Waktu:</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" value="{{ old('waktu') }}" name="waktu" id="datepicker">
                  </div>
                </div>

                <div class="form-group">
                  <label>Penanggung Jawab:</label>
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger" onclick="show()">Pilih</button>
                    </div>
                    <input type="hidden" id="tim_id2" name="penanggung_jawab" value="{{ old('tim_id1') }}"class="form-control">
                    <input type="text" id="nama2" name="nama1" value="{{ old('nama1') }}" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label>Pengawas:</label>
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger" onclick="showpeng()">Pilih</button>
                    </div>
                    <input type="hidden" id="tim_id1" name="pengawas" value="{{ old('tim_id2') }}" class="form-control">
                    <input type="text" id="nama1" name="nama2" value="{{ old('nama2') }}" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label>Anggota:</label>
                  <select class="form-control select2" multiple="multiple" data-placeholder="Pilih Anggota" name="anggota[]" style="color:#000">
                      @php $no = 1; @endphp
                      @foreach($anggotas as $anggota)
                          <option value="{{ $anggota->nik }}">{{ $anggota->nama }}</option>
                      @endforeach
                  </select>
                </div>
                
                <div class="form-group">
                    <label>Dasar Sasaran:</label><br>
                    <textarea  name="dasar_sasaran" rows="4" cols="100%" style="width:100%">{{ old('dasar_sasaran') }}</textarea>
                </div>
                <label>Isi Sasaran :</label>
                <textarea id="editor2" name="sasaran" rows="10" cols="80">{{ old('sasaran') }}</textarea>

                <div class="form-group">
                    <label>Dasar Resiko:</label><br>
                    <textarea  name="dasar_risiko" rows="4" cols="100%" style="width:100%">{{ old('dasar_resiko') }}</textarea>
                </div>
                <label>Isi Resiko :</label>
                <textarea id="editor3" name="risiko" rows="10" cols="80">{{ old('sasaran') }}</textarea>
                
                <div class="form-group">
                    <label>Ruang Lingkup Audit:</label>
                    <select class="form-control select2" multiple="multiple" data-placeholder="Pilih Divisi" name="ruanglingkup[]" style="color:#000">
                        @php $no = 1; @endphp
                        @foreach($items as $divisi)
                            <option value="{{ $divisi->id }}">{{ $divisi->nama }}</option>
                        @endforeach
                    </select>
                  </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
        </div>
  </div>
  <div id="menu1" class="tab-pane fade">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header" style="background: #e8e8de">
              <B>JADWAL PENELITIAN</B>
            </div>
            <div class="box-body">
              @foreach($tahapans as $no=> $tahapan)
                
                <div class="form-group">
                    <label>{{ $no+1 }}.{{ $tahapan->tahapan}}:</label>
                    <input type="hidden" name="tahapan_id[]" value="{{$tahapan->id }}">
                    <input type="hidden" name="kategori[]" value="{{$tahapan->kategori }}">
                    <div class="input-group date">
                      
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" placeholder="Tanggal Mulai" value="{{ date('m/d/Y') }}" onclick="klikdate({{ $no+1 }});" name="tgl_mulai[]"  id="datepicker{{ $no+1 }}">
                      <input type="text" class="form-control pull-right" placeholder="Tanggal Selesai" value="{{ date('m/d/Y') }}"" onclick="klikdate2({{ $no+1 }});" name="tgl_sampai[]"  id="datepicker2{{ $no+1 }}">
                    </div>
                  </div>
               @endforeach
              
            </div>
        </div>
  </div>
  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</div>
</form>
@endsection


<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  $(document).ready(function(){
      $(".nav-tabs a").click(function(){
          $(this).tab('show');
      });
  });
  </script>

<script>
  function show() 
    {
      window.open("{{ url('popuppj/2')}}", "list", "width=800,height=420");
    }
  function showpeng() 
    {
      window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
    }
  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }
  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
