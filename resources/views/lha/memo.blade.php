@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>MEMO DINAS</B>
      </div>
      <div class="box-body" id="prin">

          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
            <tr>
              <td colspan="3" align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="110px"><br>
                @if($obyeks->unit_id==4)
                 <h2 style="margin-bottom:4px"><u><b>&nbsp&nbsp;MEMO&nbsp&nbsp&nbsp&nbsp;DINAS&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">No : &nbsp&nbsp&nbsp;/IA-KS/AP/{{ date('m.y')}}</h4>                  
                @else
                <h2 style="margin-bottom:4px"><u><b>&nbsp&nbsp;MEMO&nbsp&nbsp&nbsp&nbsp;DINAS&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">No : &nbsp&nbsp&nbsp;/IA-KS/{{ date('m.y')}}</h4>
                @endif
                 <br><br>
              </td>
            </tr>
            @foreach($mengetahui as $ke=> $kepada)
              @if($ke==0)
                <tr>
                  <td class="tdr" width="20%">Kepada Yth </td>
                  <td class="tdr">:</td>
                  <td class="tdr"><b> {{$ke+1}}. {{ $kepada->kepada}}</b></td>
                </tr>
              @else
                <tr>
                  <td class="tdr"></td>
                  <td class="tdr"></td>
                  <td class="tdr"><b> {{$ke+1}}. {{ $kepada->kepada}}</b></td>
                </tr>
              @endif
            @endforeach
            <tr>
              <td class="tdr">Dari </td>
              <td class="tdr">:</td>
              <td class="tdr"><b> Head of Internal Audit</b></td>
            </tr>
            <tr>
              <td class="tdr">Perihal </td>
              <td class="tdr">:</td>
              <td class="tdr" ><b> Permintaan Tanggapan dan Undangan Pembahasan atas 
                Laporan Hasil Audit atas Kegiatan {{ $obyeks->unit_kerja['nama']}}</b>
              </td>
            </tr>
            <tr>
                <td class="tdr">Lampiran </td>
                <td class="tdr">:</td>
                <td class="tdr" ><b> 1 (Satu) berkas</b>
                </td>
            </tr>
            <tr>
              <td class="tdr">Tanggal </td>
              <td class="tdr">:</td>
              <td class="tdr" ><b> {{ date('d F Y')}}</b>
              </td>
            </tr>
            <tr>
                <td colspan="3" class="tdr"><hr></td>
            </tr>
            <tr>
                <td colspan="3" class="tdr" align="justify" >Dengan hormat,<br><br>Sehubungan dengan {{ $obyeks->unit_kerja['nama']}} yang telah kami 
                  lakukan, bersama ini kami sampaikan undangan pembahasan atas <i>draft</i> LHA tersebut yang akan dilaksanakan pada: 
                </td>
            </tr>
            <tr>
                <td colspan="3" class="tdr" align="justify" style="padding-left:70px"><b>
                  <table width="100%" border="0">
                    <tr>
                      <td width="12%" class="tdrs"> Hari</td>
                      <td width="1%" class="tdrs">:</td>
                      <td  class="tdrs">{{ $seminggu[date('D',strtotime($obyeks->tgl_undangan_memo))]}}</td>
                    </tr>
                    <tr>
                      <td class="tdrs"> Tanggal</td>
                      <td class="tdrs">:</td>
                      <td  class="tdrs">{{ date('d M Y',strtotime($obyeks->tgl_undangan_memo)) }}</td>
                    </tr>
                    <tr>
                      <td class="tdrs"> Jam</td>
                      <td class="tdrs">:</td>
                      <td  class="tdrs"> {{ date('H:i',strtotime($obyeks->jam_undangan_memo)) }} WIB - selesai.</td>
                    </tr>
                    <tr>
                      <td class="tdrs"> Tempat</td>
                      <td class="tdrs">:</td>
                      <td  class="tdrs"> Gd. Produksi - Ruang Rapat Internal Audit</td>
                    </tr>
                    <tr>
                      <td class="tdrs"> Acara</td>
                      <td class="tdrs">:</td>
                      <td  class="tdrs"> Pembahasan <i>Draft</i> LHA {{ $obyeks->nama}}</td>
                    </tr>
                  </table></b><br>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="tdr" align="justify" > Apabila atas <i>Draft</i> LHA tersebut terdapat tanggapan, dapat disampaikan secara tertulis paling 
                  lambat kami terima pada saat dilakukan pembahasan.<br><br>
                  Mengingat pentingnya pembahasan tersebut mohon tidak diwakilkan, jika diwakilkan agar wakil tersebut dapat mengambil keputusan.<br>
                  <br>Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.<br><br>
                </td>
            </tr>
            
          </table>
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:Tahoma">
            <tr align="center">
              <td class="tdr"width="70%"></td>
              <td class="tdr">INTERNAL AUDIT<br><br><br><br><br>
                <h3 style="margin-bottom:4px"><u><b>&nbsp&nbsp;Haryanto&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">General Manager</h4>
              </td>
            </tr>
            
          </table>
          
          
      
      </div>
      <hr>
      <table width="100%">
     
        <tr>
          <td align="center" style="padding:10px"> 
            <a href="{{ url('lha/tujuan/'.$obyeks->id)}}"><span class="btn btn-sm btn-primary" ><i class="glyphicon glyphicon-arrow-left"></i> Ubah Tujuan</span></a>
            <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Memo</button>
          </td>
        </tr>
     
      </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title>Memodinas Audit Internal</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      setTimeout(function(){
        mywindow.print();
        mywindow.close();
      },250);
      return true;
  }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:16;text-align: justify;line-height: 1.4em;}
.tdrs{padding:5px;font-size:16;text-transform:capitalize;text-align: justify;font-weight:bold;word-spacing: 3px;}
#prin{margin-left:10%;margin-right:10%;width:80%;}
</style>