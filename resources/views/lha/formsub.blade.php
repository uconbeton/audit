@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<form method="post" action="{{ url('/lha/insertsub')}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>Form Sub Rekomendasi </B>
              </div>
              
            <div class="tab-content">
               
                <span class="btn btn-success btn-sm" id="add" style="margin:15px 1px 15px 15px"><i class="glyphicon glyphicon-plus"></i> Tambah</span>
                <a href=" {{ url('lha/editrekomendasi/'.$subrekomendasi->obyek_id) }}"><span class="btn btn-primary btn-sm"  style="margin:15px 2px 15px 0px"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</span></a>
                <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                <br>
                @foreach($subsubrekomendasi as $no=>$subrek)
                  <div class="box-body" style="background:#f5f5f5;margin:2px;">
                    <div class="box-header" style="background: #e8e8de;padding: 5px;">
                      <a href="{{ url('lha/deletesub/'.$subrek->id) }}"><span class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-remove"></i></span></a>
                      <b>No {{ $subrekomendasi->subnomor }}.{{$alphabet[$no]}}</b>
                    </div>
                    <input type="hidden" size="10" name="subnomor[]" value="{{ $subrekomendasi->subnomor }}"><br>
                    <label style="width:8%">Kodifikasi :</label>
                    <select class="form-control" id="kodefikasi{{$no}}" style="width:50%;margin-bottom:2px;display:inline" name="kodefikasi[]"> 
                      @foreach($kodifikasi as $kodif)
                        <option value="{{$kodif->kodifikasi}}" @if ( $subrek->kodifikasi ==$kodif->kodifikasi) {{ 'selected' }} @endif>[{{$kodif->kodifikasi}}] {{$kodif->kategori}}</option>
                      @endforeach
                    </select><br>
                    <label id="label{{$no}}" style="width:8%">Nilai :</label>
                    <input type="text" id="nilai{{$no}}" size="10" name="nilai[]" value="{{$subrek->nilai}}" style="width:10%;margin-bottom:2px;display:inline"  class="form-control">
                    <input type="text" size="20" id="nilaicuren{{$no}}"  value="{{number_format($subrek->nilai)}}"style="width:15%;margin-bottom:2px;display:inline;background:#e7e7f9"  readonly class="form-control">
                    <textarea class="form-control" name="subsubrekomendasi[]" placeholder="Isi Rekomendasi....." cols="100" >{!! $subrek->subrekomendasi !!}</textarea><br>
                  </div>

                @endforeach
                <div id="container" ></div>
                
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <input type="hidden" name="id" value="{{ $subrekomendasi->id }}">
                    <input type="hidden" name="obyek_id" value="{{ $subrekomendasi->obyek_id }}">
                </div>
            </div>
          
        </div>
  
</form>
@endsection

<style>
  .latarbelakang{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .sasaran{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .ruanglingkup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .pelaksanaan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .penutup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .kesimpulan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  #penjelasan{width: 100%; height: 600px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">
 
    $(document).ready(function() {
        var count = {{ $subrekomendasi->loadJumRek()}};
        $("#add").click(function(){
            count += 1;
            $('#container').append(
                '<div class="box-body" style="background:#f5f5f5;margin:2px;">'
                    +'<div class="box-header" style="background: #e8e8de;padding: 5px;"><b>No {{ $subrekomendasi->subnomor }}.'+count+'</b></div>'
                        +'<input type="hidden" size="10" name="subnomor[]" value="{{ $subrekomendasi->subnomor }}"><br>'
                        +'<label style="width:8%">Kodifikasi :</label>'
                        +'<select class="form-control" id="kodefikasi'+count+'" style="width:50%;margin-bottom:2px;display:inline" name="kodefikasi[]"> '
                            @foreach($kodifikasi as $kodif)
                              +'<option value="{{$kodif->kodifikasi}}">[{{$kodif->kodifikasi}}] {{$kodif->kategori}}</option>'
                            @endforeach
                        +'</select><br>'
                        +'<label id="label'+count+'" style="width:8%">Nilai :</label>'
                        +'<input id="nilai'+count+'" type="text" size="10" name="nilai[]" style="width:10%;margin-bottom:2px;display:inline"  class="form-control">&nbsp;'
                        +'<input type="text" size="20" id="nilaicuren'+count+'" style="width:15%;margin-bottom:2px;display:inline;background:#e7e7f9"  readonly class="form-control">'
                        +'<textarea class="form-control" name="subsubrekomendasi[]" placeholder="Isi Rekomendasi....." cols="100" ></textarea><br>'
                        
                +'</div>'
                    
            );
            function formatNumber(n) {
              return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }
            $('#nilai'+count).on('keyup', function() {
                var nilainya=this.value;
                  nr=formatNumber(nilainya);
                $('#nilaicuren'+count).val(nr);
               
            });
            $('#label'+count).hide()
            $('#nilai'+count).hide()
            $('#nilaicuren'+count).hide()
            $("#kodefikasi"+count).change(function(){
               var value=$(this).val();
               if(value=='03.C.01' || value=='03.C.02' || value=='03.C.03' || value=='04.E.01' || value=='04.E.02' || value=='04.E.03' || value=='05.A.03' || value=='07.X.01' || value=='07.X.02' ){
                  $('#label'+count).show()
                  $('#nilai'+count).show()
                  $('#nilaicuren'+count).show()
               }else{
                  $('#label'+count).hide()
                  $('#nilai'+count).hide()
                  $('#nilaicuren'+count).hide()
               }
               
            })
        
        });

        @foreach($subsubrekomendasi as $no=>$subrek)
        
        function formatNumber(n) {
          return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }
        $('#nilai{{$no}}').on('keyup', function() {
            var nilainya=this.value;
              nr=formatNumber(nilainya);
            $('#nilaicuren{{$no}}').val(nr);
            
        });
        $("#kodefikasi{{$no}}").change(function(){
            var value=$(this).val();
            var nil=$("#nilai{{$no}}").val();
            if(value=='03.C.01' || value=='03.C.02' || value=='03.C.03' || value=='04.E.01' || value=='04.E.02' || value=='04.E.03' || value=='05.A.03' || value=='07.X.01' || value=='07.X.02' ){
              $('#label{{$no}}').show()
              $('#nilai{{$no}}').show()
              $('#nilaicuren{{$no}}').show()
              
            }else{
              $('#label{{$no}}').hide()
              $('#nilai{{$no}}').hide()
              $('#nilaicuren{{$no}}').hide()
            }
            
        })
    
      @endforeach 
    
                            
        
    });
  
                    
</script>

<script>
  $(function () {
    $('.latarbelakang').wysihtml5()
  })

  $(function () {
    $('.sasaran').wysihtml5()
  })

  $(function () {
    $('.ruanglingkup').wysihtml5()
  })

  $(function () {
   $('.pelaksanaan').wysihtml5()
  })

  $(function () {
   $('.penutup').wysihtml5()
  })

  $(function () {
   $('.kesimpulan').wysihtml5()
  })
  
  $(function () {
    CKEDITOR.replace('penjelasan')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  // $(function () {
  //   CKEDITOR.replace('kesimpulan')
    //bootstrap WYSIHTML5 - text editor
  //   $('.textarea').wysihtml5()
  // })
</script>

<script>
  function unitkerja() 
    {
      var unit=$('#unit_id').val();
      window.open("../popunitkerja/"+unit+"", "list", "width=800,height=420");
    }

  function show() 
    {
      window.open("{{ url('popketuatim')}}", "list", "width=800,height=500");
    }
    
  function showpeng() 
    { var unit=$('#kategori_audit_id').val();
      if(unit==3 || unit==4 || unit==7){
        window.open("{{ url('popuppj/4')}}", "list", "width=800,height=420");
      }else{
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }
      
    }

  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }

  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
