@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de" >
        <B>AUDIT PLAN</B>
      </div>
      <div class="box-body" id="prin">
          <table  border="0" width="100%" style="border-collapse:collapse;font-family:'Times New Roman', Times, serif">
            <tr>
              <td colspan="3" align="center"><img src="{{url('/img/ks.jpg')}}" widt="300px" height="150px"><hr></td>
            </tr>
            <tr>
              <td width="15%" class="tdr">Unit Organisasi</td>
              <td class="tdr">: {{ $obyeks->unit_kerja['nama'] }}</td>
              <td class="tdr" width="15%">No. KKA :</td>
            </tr>
            <tr>
              <td class="tdr">Obyek Audit</td>
              <td class="tdr">: {{ $obyeks->nama }}</td>
              <td class="tdr"></td>
            </tr>
            <tr>
                <td class="tdr">Periode Audit</td>
                <td class="tdr">: 
                  @if(date('m',strtotime($obyeks->waktu))=='01' || date('m',strtotime($obyeks->waktu))=='02' || date('m',strtotime($obyeks->waktu))=='03')
                    Triwulan 1
                 
                  @endif

                  @if(date('m',strtotime($obyeks->waktu))=='04' || date('m',strtotime($obyeks->waktu))=='05' || date('m',strtotime($obyeks->waktu))=='06')
                    Triwulan 2
                 
                  @endif

                  @if(date('m',strtotime($obyeks->waktu))=='07' || date('m',strtotime($obyeks->waktu))=='08' || date('m',strtotime($obyeks->waktu))=='09')
                    Triwulan 3
                  
                  @endif

                  @if(date('m',strtotime($obyeks->waktu))=='10' || date('m',strtotime($obyeks->waktu))=='11' || date('m',strtotime($obyeks->waktu))=='12')
                    Triwulan 4
                  
                  @endif
               </td>
               <td class="tdr"> </td>
           </tr>
        </table>
          <hr>
      
           <table width="100%" border="0" style="border-collapse:collapse;font-family:'Times New Roman', Times, serif">
              <tr>
                <td width="2%"  class="tdr"><b>I.</b></td>
                <td class="tdr" colspan="3" ><b>TUJUAN AUDIT</b></td>
              </tr>
              
                  @foreach($tujuans as $no => $tujuan)  
                    <tr>
                      <td  class="tdr"></td>
                      <td  class="tdr" width="2%">{{ $no+1 }}.</td>
                      <td class="tdr" colspan="2">{{ $tujuan->tujuan }}</td>
                    </tr>
                  @endforeach
                
             
              <tr>
                <td width="2%"  class="tdr"><b>II.</b></td>
                <td class="tdr" colspan="3"><b>SASARAN</b></td>
              </tr>
              <tr>
                <td  class="tdr"></td>
                <td class="tdr" colspan="3">
                  @if(!is_null($sasaran))
                      {{ $sasaran->sasaran }}
                  @else
                    Silahkan Diisi
                  @endif

                </td>
              </tr>
                
                @foreach($subsasarans as $key => $subsasaran)
                  <tr>
                    <td  class="tdr"></td>
                    <td  class="tdr" width="2%">{{ $key+1}}.</td>
                    <td class="tdr" colspan="2">{{ $subsasaran->sasaran }}</td>
                  </tr>
                @endforeach
              
              <tr>
                <td width="2%"  class="tdr"><b>III.</b></td>
                <td class="tdr" colspan="3"><b>RESIKO POTENSIAL</b></td>
              </tr>
              <tr>
                <td  class="tdr"></td>
                <td class="tdr" colspan="3">
                  @if(!is_null($risiko))
                  {{ $risiko->risiko }}
                  @else
                    Silahkan Diisi
                  @endif
                </td>
              </tr>
                
                @foreach($subrisikos as $key => $subrisiko)
                  <tr>
                    <td  class="tdr"></td>
                    <td  class="tdr" width="2%">{{ $key+1}}.</td>
                    <td class="tdr" colspan="2">{{ $subrisiko->risiko }}</td>
                  </tr>
                @endforeach
             
              <tr>
                <td width="2%"  class="tdr"><b>IV.</b></td>
                <td class="tdr" colspan="3"><b>RUANG LINGKUP</b></td>
              </tr>
              <tr>
                <td  class="tdr"></td>
                <td class="tdr" colspan="3">Adapun unit kerja yang terkait dengan audit ini adalah:</td>
              </tr>
                @foreach($ruanglingkups as $key => $ruanglingkup)
                  <tr>
                    <td  class="tdr"></td>
                    <td  class="tdr" width="2%">{{ $key+1}}.</td>
                    <td class="tdr" colspan="2">{{ $obyeks->unit['nama']}} {{ $ruanglingkup->unit_kerja['nama']}}</td>
                  </tr>
                @endforeach
               
              <tr>
                <td width="2%"  class="tdr"><b>V.</b></td>
                <td class="tdr" colspan="3"><b>TIM AUDIT</b></td>
              </tr>
              <tr>
                <td  class="tdr"></td>
                <td  class="tdr" width="2%">1.</td>
                <td class="tdr" width="30%">{{ $obyeks->pengawas['nama'] }}</td>
                <td class="tdr">(Pengawas)</td>
              </tr>
              <tr>
                <td  class="tdr"></td>
                <td  class="tdr" width="2%">2.</td>
                <td class="tdr">{{ $obyeks->ketua_tim['nama'] }}.</td>
                <td class="tdr">(Ketua Tim)</td>
              </tr>
                @foreach($obyekstim as $key => $anggota)
                  <tr>
                    <td  class="tdr"></td>
                    <td  class="tdr" width="2%">{{ $key+3}}.</td>
                    <td class="tdr">{{ $anggota->karyawan['nama']}}</td>
                    <td class="tdr">(Anggota)</td>
                  </tr>
                @endforeach
                    
                <tr>
                  <td width="2%"  class="tdr"><b>VI.</b></td>
                  <td class="tdr" colspan="3"><b>RENCANA PENERBITAN LAPORAN</b></td>
                </tr>
                <tr>
                  <td  class="tdr"></td>
                  <td class="tdr" colspan="3">Laporan hasil pemeriksaan akan diterbitkan tanggal {{ $sampai->tgl_sampai->format('d-m-y') }}</td>
                </tr>
            </table><br>
            
            <table width="100%" border="0" style="border-collapse:collapse;font-family:'Times New Roman', Times, serif">
              <tr>
                <td width="2%"  class="tdr"><b>VII.</b></td>
                <td class="tdr" colspan="3" ><b>JADWAL PELAKSANAAN AUDIT</b></td>
              </tr>
            </table>
            <table width="100%" border="1" style="border-collapse:collapse;font-family:'Times New Roman', Times, serif">
              <tr>
                <td rowspan="2" class="tdr" >NO</td>
                <td rowspan="2" class="tdr" >TAHAPAN AUDIT</td>
                <td class="tdr" COLSPAN="2">RENCANA AUDIT</td>
                <td rowspan="2" class="tdr" >KET</td>
              </tr>
              <tr>
                <td class="tdr" >TGL MULAI</td>
                <td class="tdr" >TGL SELESAI</td>
              </td>

              <tr>
                  <td  class="tdr" width="2%" ><b>1.</b></td>
                  <td  class="tdr"><b>DESK AUDIT</b></td>
                  <td  class="tdr"></td>
                  <td  class="tdr"></td>
                  <td  class="tdr"></td>
                </tr>
                @foreach($tahapans1 as $no => $jadwal1)
                 
                    <tr>
                      <td  class="tdr"></td>
                      <td  class="tdr">{{ $no+1 }}. {{ $jadwal1->tahapan['tahapan'] }}.</td>
                      <td class="tdr">{{ $jadwal1->tgl_mulai->format('d-m-y') }}</td>
                      <td class="tdr">{{ $jadwal1->tgl_sampai->format('d-m-y') }}</td>
                      <td class="tdr">{{ $selisih1[$no]+1}}</td>
                    </tr>
                  
                @endforeach

              <tr>
                <td  class="tdr" width="2%" ><b>2.</b></td>
                <td  class="tdr"><b>FIELD AUDIT</b></td>
                <td  class="tdr"></td>
                <td  class="tdr"></td>
                <td  class="tdr"></td>
              </tr>
                @foreach($tahapans2 as $no => $jadwal2)
                  
                    <tr>
                      <td  class="tdr"></td>
                      <td  class="tdr">{{ $no+1 }}. {{ $jadwal2->tahapan['tahapan'] }}.</td>
                      <td class="tdr">{{ $jadwal2->tgl_mulai->format('d-m-y') }}</td>
                      <td class="tdr">{{ $jadwal2->tgl_sampai->format('d-m-y')}}</td>
                      <td class="tdr">{{ $selisih2[$no]+1}}</td>
                    </tr>
              
                @endforeach

              <tr>
                <td  class="tdr" width="2%" ><b>3.</b></td>
                <td  class="tdr"><b>REPORTING AUDIT</b></td>
                <td  class="tdr"></td>
                <td  class="tdr"></td>
                <td  class="tdr"></td>
              </tr>
              @foreach($tahapans3 as $no => $jadwal3)
               
                  <tr>
                    <td  class="tdr"></td>
                    <td  class="tdr">{{ $no+1 }}. {{ $jadwal3->tahapan['tahapan'] }}.</td>
                    <td class="tdr">{{ $jadwal3->tgl_mulai->format('d-m-y') }}</td>
                    <td class="tdr">{{ $jadwal3->tgl_sampai->format('d-m-y') }}</td>
                    <td class="tdr">{{ $selisih3[$no]+1}}</td>
                  </tr>
               
              @endforeach
            </table>
            
      </div>
        <table width="100%">
          <tr>
           <td align="center"> <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Audit Plan</button></td>
          </tr>
        </table><br><br>
    </div>
</div>

@endsection

<script>
function print(divId) {
  var content = document.getElementById(divId).innerHTML;
  var mywindow = window.open('', 'Print', 'height=600,width=800');

  mywindow.document.write('<html><head><title>Print</title>');
  mywindow.document.write('</head><body >');
  mywindow.document.write(content);
  mywindow.document.write('</body></html>');

  mywindow.document.close();
  mywindow.focus()
  mywindow.print();
  mywindow.close();
  return true;
}


function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
  return true;
}
</script>
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:14;}
</style>