@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<form method="post" action="{{ url('/lha/insert')}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>Laporan LHA</B>
              </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Latar Belakang</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Sasaran </a></li>
                  <li><a href="#tab_3" data-toggle="tab">Ruang Lingkup </a></li>
                  <li><a href="#tab_4" data-toggle="tab">Pelaksanaan </a></li>
                  <li><a href="#tab_5" data-toggle="tab">Penjelasan Singkat </a></li>
                  <li><a href="#tab_6" data-toggle="tab">Kesimpulan </a></li>
                  <li><a href="#tab_7" data-toggle="tab">Penutup </a></li>
                  <li><a href="{{ url('lha/file/'.$obyeks->id)}}" >Media File Manager</a></li>
                </ul>
                <div class="tab-content">
                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane active" id="tab_1">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Latar Belakang :</label>
                          <textarea class="latarbelakang" name="latar_belakang" rows="50" cols="80">{{ old('tujuan') }}</textarea>
                        </div>
                      </div>
                    </div>

                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_2">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Sasaran Audit :</label>
                          <textarea class="sasaran" name="sasaran" rows="10" cols="80">
                            {{$sasaran->sasaran}}<br>
                            @foreach($sasarans as $key=>$sasarans)
                                {{$key+1}}.{{ $sasarans->unitkerja['nama']}}<br>
                            @endforeach
                          </textarea>
                        </div>
                      </div>
                    </div>
                  
                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_3">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Ruang Lingkup Audit :</label>
                          <textarea class="ruanglingkup" name="ruang_lingkup" rows="10" cols="80">
                              {{$ruanglingkup->ruanglingkup}}<br>
                              @foreach($ruanglingkups as $key=>$ruanglingkups)
                                  {{$key+1}}.{{ $ruanglingkups->ruanglingkup}}<br>
                              @endforeach
                          </textarea>
                        </div>
                      </div>
                    </div>

                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_4">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Pelaksanaan Audit :</label>
                          <textarea class="pelaksanaan" name="pelaksanaan" rows="10" cols="80">
                            Sifat audit adalah {{ $kategori->keterangan}} terhadap {{ $obyeks->nama }}. Audit dilakukan mulai tanggal {{ $jadwalmulai->tgl_mulai->format('d F Y') }} 
                             sampai tanggal {{ $jadwalsampai->tgl_sampai->format('d F Y') }} sesuai dengan Memo Dinas Kepala Kesatuan Pengawas Intern No:{{ $obyeks->id }}/ST.MD/GM/IA-KS/{{ date('Y',strtotime($obyeks->waktu))}},
                              tanggal {{ date('d F Y',strtotime($obyeks->tgl_setujui)) }} dan Surat Tugas Head Of Internal Audit No: {{ $obyeks->id }}/ST/GM/IA-KS/{{ date('Y',strtotime($obyeks->waktu))}}, tanggal {{ date('d F Y',strtotime($obyeks->waktu)) }}.
                          </textarea>
                        </div>
                      </div>
                    </div>

                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_5">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Penjelasan Singkat Obyek Yang diaudit :</label>
                          <textarea id="penjelasan" name="penjelasan" rows="10" cols="80">{{ old('tujuan') }}</textarea>
                        </div>
                      </div>
                    </div>

                   <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_6">
                      <div class="box-body">
                        <div class="form-group">
                            <a href="{{ url('lha/file/'.$obyeks->id)}}" ><span class="btn btn-primary btn-sm">Media File</span></a><br><br>
                          <label>Kesimpulan Hasil Audit :</label>
                          <textarea class="kesimpulan" name="kesimpulan" rows="10" cols="80">{{ old('tujuan') }}</textarea>
                        </div>
                      </div>
                      <span class="btn btn-success" id="add" style="margin-top:30px">Tambah Temuan</span>
                      <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                      <br>
                      
                      <div id="container" style="margin-left:20px;"></div><br><br>
                    </div>

                    <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_7">
                        <div class="box-body">
                          <div class="form-group">
                              
                            <label>Penutup :</label>
                            <textarea class="penutup" name="penutup" rows="10" cols="80">{{ old('tujuan') }}</textarea>
                          </div>
                        </div>
                      </div>

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-floppy-disk"></i>Submit</button>
                      <a href=" {{ url('lha/') }}"><span class="btn btn-info btn-sm"><i class="glyphicon glyphicon-arrow-left"></i>Kembali</span></a>
                      <input type="hidden" name="id" value="{{ $obyeks->id }}">
                    </div>
                </div>
            </div>
        </div>
  
</form>
@endsection

<style>
  .latarbelakang{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .sasaran{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .ruanglingkup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .pelaksanaan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .penutup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .kesimpulan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  #penjelasan{width: 100%; height: 600px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function() {
     var count = 0;
      $("#add").click(function(){
        count += 1;
          $('#container').append(
                    '<div class="box-body">'
                      +'<div class="form-group">'
                          +'<label>Temuan 6.'+count+' :</label><input type="hidden" name="nomor[]" value="6.'+count+'">'
                            +'<div class="input-group" style="margin-bottom:2px" >'
                             +'<div class="input-group-btn">'
                                +'<button type="button" class="btn btn-info" >Judul</button>'
                              +'</div>'
                              +' <input class="form-control pull-right" type="text" name="judul[]"  value="" placeholder="Judul Sub"> '
                             +'</div>'

                             +'<div class="input-group">'
                                +'<div class="input-group-btn">'
                                  +'<button type="button" class="btn btn-info" >Kode</button>'
                                +'</div>'
                             +' <select class="form-control pull-right" name="kodifikasi[]"> '
                                  @foreach($kodifikasi as $kodifikasi)
                                    +'<option value="{{$kodifikasi->kodifikasi}}">[{{$kodifikasi->kodifikasi}}] {{$kodifikasi->kategori}}</option>'
                                  @endforeach
                             +'</select>'
                             +'</div>'
                          +'<textarea id="rekomendasi6'+count+'" name="rekomendasi6[]" rows="10" cols="80"></textarea>'
                            +'<div class="input-group" style="margin-bottom: 2px;">'
                              +'<div class="input-group-btn">'
                                +'<button type="button" class="btn btn-info" >Keterangan</button>'
                                +'</div>'
                                +'<input class="form-control pull-right" type="text" name="head_rekomendasi[]"   placeholder="Keterangan">' 
                            +'</div>'
                        +'</div>'
                      +'</div>'
                
          );
          $(function () {
            CKEDITOR.replace('rekomendasi6'+count)
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
          })
           
    });

    $('#add').click();
  
                        
    
  });
       
                    
</script>

<script>
  $(function () {
    $('.latarbelakang').wysihtml5()
  })

  $(function () {
    $('.sasaran').wysihtml5()
  })

  $(function () {
    $('.ruanglingkup').wysihtml5()
  })

  $(function () {
   $('.pelaksanaan').wysihtml5()
  })

  $(function () {
   $('.penutup').wysihtml5()
  })

  $(function () {
   $('.kesimpulan').wysihtml5()
  })
  
  $(function () {
    CKEDITOR.replace('penjelasan')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  // $(function () {
  //   CKEDITOR.replace('kesimpulan')
    //bootstrap WYSIHTML5 - text editor
  //   $('.textarea').wysihtml5()
  // })
</script>

<script>
  function unitkerja() 
    {
      var unit=$('#unit_id').val();
      window.open("../popunitkerja/"+unit+"", "list", "width=800,height=420");
    }

  function show() 
    {
      window.open("{{ url('popketuatim')}}", "list", "width=800,height=500");
    }
    
  function showpeng() 
    { var unit=$('#kategori_audit_id').val();
      if(unit==3 || unit==4 || unit==7){
        window.open("{{ url('popuppj/4')}}", "list", "width=800,height=420");
      }else{
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }
      
    }

  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }

  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
