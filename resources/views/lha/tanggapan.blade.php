@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<form method="post" action="{{ url('/lha/inserttanggapan')}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>Form Tanggapan</B>
              </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                @foreach($rekomendasi as $no=>$rek)
                <input name="rekomendasi[]" type="hidden" value="{{$rek->id}}">
                    @if($no==0)
                        <li class="active"><a href="#tab{{$no}}" data-toggle="tab">Temuan {{$rek->nomor}}</a></li>
                    @else
                        <li ><a href="#tab{{$no}}" data-toggle="tab">Temuan {{$rek->nomor}}</a></li>
                    @endif
                @endforeach
                  
                 
                </ul>
                <div class="tab-content">
                  <!------------------------------------------------tab 1---->
                  @foreach($rekomendasi as $no=>$rek)
                    
                    @if($no==0)
                        <div class="tab-pane active" id="tab{{$no}}">
                    @else
                        <div class="tab-pane" id="tab{{$no}}">
                    @endif
                        <div class="box-body">
                            <div class="form-group">
                            <label>Tanggapan {{ $rek->nomor }}: </label>
                            <textarea id="tanggapan{{$no}}" name="tanggapan[]" rows="10" style="width:100%;height: 200px;" >{{$rek->tanggapan}}</textarea>
                            <input type="hidden" name="rekomendasi_id[]"  value="{{ $rek->id }}">
                            </div>
                        </div>
                        
                    </div>
                    
                  @endforeach
                   

                  
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <a href=" {{ url('lha/edit/'.$obyeks->id) }}"><span class="btn btn-info">Back</span></a>
                      <input type="hidden" name="id" value="{{ $obyeks->id }}">
                    </div>
                </div>
            </div>
        </div>
  
</form>
@endsection

<style>
  .latarbelakang{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .sasaran{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .ruanglingkup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .pelaksanaan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .penutup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .kesimpulan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  #penjelasan{width: 100%; height: 600px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">
 @foreach($rekomendasi as $no=>$rek)
    
    $(function (){
        CKEDITOR.replace('tanggapan{{$no}}')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()

    });
@endforeach    
                    


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
