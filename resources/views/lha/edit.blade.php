@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<form method="post" action="{{ url('/lha/update')}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>Laporan LHA</B>
              </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Latar Belakang</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Sasaran </a></li>
                  <li><a href="#tab_3" data-toggle="tab">Ruang Lingkup </a></li>
                  <li><a href="#tab_4" data-toggle="tab">Pelaksanaan </a></li>
                  <li><a href="#tab_5" data-toggle="tab">Penjelasan Singkat </a></li>
                  <li><a href="#tab_6" data-toggle="tab">Kesimpulan </a></li>
                  <li><a href="{{ url('lha/editrekomendasi/'.$obyeks->id)}}" >Rekomendasi </a></li>
                  <li><a href="{{ url('lha/tanggapan/'.$obyeks->id)}}" >Tanggapan </a></li>
                  <li><a href="#tab_7" data-toggle="tab">Penutup </a></li>
                  <li><a href="{{ url('lha/file/'.$obyeks->id)}}" >Media File Manager</a></li>
                 
                </ul>
                <div class="tab-content">
                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane active" id="tab_1">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Latar Belakang :</label>
                          <textarea class="latarbelakang" name="latar_belakang" rows="50" cols="80">{{$lha->latar_belakang}}</textarea>
                        </div>
                      </div>
                    </div>

                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_2">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Sasaran Audit :</label>
                          <textarea class="sasaran" name="sasaran" rows="10" cols="80">
                            {{$lha->sasaran}}
                            
                          </textarea>
                        </div>
                      </div>
                    </div>
                  
                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_3">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Ruang Lingkup Audit :</label>
                          <textarea class="ruanglingkup" name="ruang_lingkup" rows="10" cols="80">{{$lha->ruang_lingkup}}</textarea>
                        </div>
                      </div>
                    </div>

                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_4">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Pelaksanaan Audit :</label>
                          <textarea class="pelaksanaan" name="pelaksanaan" rows="10" cols="80">{{$lha->pelaksanaan}}</textarea>
                        </div>
                      </div>
                    </div>

                  <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_5">
                      <div class="box-body">
                        <div class="form-group">
                          <label>Penjelasan Singkat Obyek Yang diaudit :</label>
                          <textarea id="penjelasan" name="penjelasan" rows="10" cols="80">{{$lha->penjelasan}}</textarea>
                        </div>
                      </div>
                    </div>

                   <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_6">
                      <div class="box-body">
                        <div class="form-group">
                          <a href="{{ url('lha/file/'.$obyeks->id)}}" ><span class="btn btn-primary btn-sm">Media File</span></a><br><br>
                          
                          <label>Kesimpulan Hasil Audit :</label>
                          <textarea class="kesimpulan" name="kesimpulan" rows="10" cols="80">{{$lha->kesimpulan}}</textarea>
                        </div>
                      </div>
                      <span class="btn btn-success btn-sm" id="add" style="margin-top:30px"><i class="glyphicon glyphicon-plus"></i>Tambah Temuan</span>
                      <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                      <br>
                      @foreach($rekomendasi as $key=>$rekomendasi)
                        <input type="hidden" name="rekomendasi_id[]" value="{{ $rekomendasi->id}}">
                        
                          <div class="box-body">
                            <div class="form-group">
                              <label>Temuan 6.{{$key+1}} :</label><input type="hidden" name="nomor[]" value="6.{{$key+1}}"><br>
                              <span style="margin-bottom:1%" class="btn btn-danger btn-xs" onclick="hapus_temuan({{ $rekomendasi->id}})">Hapus Temuan</span>
                              <div class="input-group" style="margin-bottom: 2px;">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-info" >Judul</button>
                                </div>
                                  <input class="form-control pull-right" type="text" name="judul[]"  value="{!! $rekomendasi->judul !!}" placeholder="Judul Sub"> 
                              </div>
                              <div class="input-group">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-info" >Kode</button>'
                                </div>
                                <select class="form-control pull-right" name="kodifikasi[]"> 
                                  @foreach($kodifikasi as $kodifikassi)
                                    <option value="{{$kodifikassi->kodifikasi}}" @if($rekomendasi->kodifikasi==$kodifikassi->kodifikasi) selected @endif >[{{$kodifikassi->kodifikasi}}] {{$kodifikassi->kategori}}</option>
                                  @endforeach
                                </select>
                              </div>
                              <textarea id="rekomendasiedit6{{$key+1}}" name="rekomendasi6[]" rows="10" cols="80">{!! $rekomendasi->rekomendasi !!}</textarea>
                              <div class="input-group" style="margin-bottom: 2px;">
                                <div class="input-group-btn">
                                  <button type="button" class="btn btn-info" >Keterangan</button>
                                </div>
                                  <input class="form-control pull-right" type="text" name="head_rekomendasi[]"  value="{!! $rekomendasi->head_rekomendasi !!}" placeholder="Keterangan"> 
                              </div>
                            </div>
                          </div>
                          <script>
                            $(function (){
                                CKEDITOR.replace('rekomendasiedit6{{ $key+1 }}')
                                //bootstrap WYSIHTML5 - text editor
                                $('.textarea').wysihtml5()
                    
                            });
                          </script>
                      @endforeach
                      <div id="container" ></div><br><br>
                    </div>

                    <!------------------------------------------------tab 1---->
                    <div class="tab-pane" id="tab_7">
                        <div class="box-body">
                          <div class="form-group">
                            <label>Penutup :</label>
                            <textarea class="penutup" name="penutup" rows="10" cols="80">{{$lha->penutup}}</textarea>
                          </div>
                        </div>
                      </div>

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
                      <a href=" {{ url('lha/') }}"><span class="btn btn-info btn-sm"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</span></a>
                      <input type="hidden" name="id" value="{{ $obyeks->id }}">
                    </div>
                </div>
            </div>
        </div>
  
</form>

@endsection

<style>
  .latarbelakang{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .sasaran{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .ruanglingkup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .pelaksanaan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .kesimpulan{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
  .penutup{width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>
@push('link-java')


<script type="text/javascript">
 
  $(document).ready(function() {
     var count = {{ $counts }};
      $("#add").click(function(){
        count += 1;
          $('#container').append(
            '<div class="box-body">'
                      +'<input type="hidden" name="rekomendasi_id[]" value="0">'
                      +'<div class="form-group">'
                          +'<label>Temuan 6.'+count+' :</label><input type="hidden" name="nomor[]" value="6.'+count+'">'
                            +'<div class="input-group" style="margin-bottom: 2px;">'
                             +'<div class="input-group-btn">'
                                +'<button type="button" class="btn btn-info" >Judul</button>'
                              +'</div>'
                              +' <input class="form-control pull-right" type="text" name="judul[]"  value="" placeholder="Judul Sub"> '
                             +'</div>'
                             +'<div class="input-group">'
                                +'<div class="input-group-btn">'
                                  +'<button type="button" class="btn btn-info" >Kode</button>'
                                +'</div>'
                             +' <select class="form-control pull-right" name="kodifikasi[]"> '
                                  @foreach($kodifikasi as $kodifikasi)
                                    +'<option value="{{$kodifikasi->kodifikasi}}">[{{$kodifikasi->kodifikasi}}] {{$kodifikasi->kategori}}</option>'
                                  @endforeach
                             +'</select>'
                             +'</div>'
                          +'<textarea id="rekomendasi6'+count+'" name="rekomendasi6[]" rows="10" cols="80"></textarea>'
                            +'<div class="input-group" style="margin-bottom: 2px;">'
                              +'<div class="input-group-btn">'
                                +'<button type="button" class="btn btn-info" >Keterangan</button>'
                                +'</div>'
                                +'<input class="form-control pull-right" type="text" name="head_rekomendasi[]"   placeholder="Keterangan">' 
                            +'</div>'
                        +'</div>'
                      +'</div>'
                
          );
          $(function () {
            CKEDITOR.replace('rekomendasi6'+count)
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
          })
           
    });
                  
    
  });
  
  function hapus_temuan(a){
      $.ajax({
          type: 'GET',
          url: "{{url('lha/hapus_temuan')}}",
          data: "id="+a,
          success: function(msg){
              location.reload();
          }
      }); 
      
  }
 

  $(function () {
    $('.latarbelakang').wysihtml5()
  })

  $(function () {
    $('.sasaran').wysihtml5()
  })

  $(function () {
    $('.ruanglingkup').wysihtml5()
  })

  $(function () {
   $('.pelaksanaan').wysihtml5()
  })
  
  $(function () {
   $('.penutup').wysihtml5()
  })

  $(function () {
   $('.kesimpulan').wysihtml5()
  })

  $(function () {
    CKEDITOR.replace('penjelasan')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  
</script>

@endpush