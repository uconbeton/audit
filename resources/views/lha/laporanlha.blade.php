@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>Laporan LHA</B>
      </div>
      <div class="box-body" id="prin">
          <table  border="0" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:50px">
            {{-- <tr>
              <td align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="150px"><br>
                 <h2 style="margin-bottom:4px"><u><b>&nbsp&nbsp;LAPORAN&nbsp&nbsp&nbsp&nbsp;LHA&nbsp&nbsp;</b></u></h2>                  
                <br><br>
              </td>
            </tr> --}}
            <tr>
              <td class="tdr"><br><br><br></td>
            </tr>
            <tr>
              <td class="tdr"><b>1. <u>LATAR BELAKANG</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->latar_belakang !!}</td>
            </tr>
            <tr>
              <td class="tdr"><b>2. <u>SASARAN AUDIT</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->sasaran !!}</td>
            </tr>
            <tr>
              <td class="tdr"><b>3. <u>RUANG LINGKUP AUDIT</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->ruang_lingkup !!}</td>
            </tr>
            <tr>
              <td class="tdr"><b>4. <u>PELAKSANAAN AUDIT</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->pelaksanaan !!}</td>
            </tr>
            <tr>
              <td class="tdr"><b>5. <u>PENJELASAN SINGKAT OBYEK YANG DIAUDIT</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->penjelasan !!}</td>
            </tr>
            <tr>
              <td class="tdr"><b>6. <u>KESIMPULAN HASIL AUDIT</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->kesimpulan !!}</td>
            </tr>
              @foreach($rekomendasi as $rekomen)
                <tr>
                  <td class="tdr" style="padding: 5px 0px 0px 22px;"><b>{{ $rekomen->nomor }}.{{ $rekomen->judul }}</b></td>
                </tr>
                <tr>
                  <td class="isitdr" style="padding: 5px 0px 0px 40px;">
                    {!! $rekomen->rekomendasi !!}<br>
                      @if($rekomen->loadJumRek()>0)              
                        <table width="100%" >
                          
                          <tr>
                            <td colspan="2" ><b><u>Rekomendasi</u></b></td>
                          </tr>
                          <tr>
                            <td class="isitdrr" colspan="2">
                              {{ $rekomen->head_rekomendasi}}
                            
                            </td>
                          </tr>
                          @foreach($subrekomendasi->where('rekomendasi_id',$rekomen->id) as $nosub=> $subrek)
                            @if($jumsub->shift()['count']==1)  
                              @foreach($subsubrekomendasi->where('sub_rekomendasi_id',$subrek->id) as $ur=>$subsub)
                                <tr>
                                  <td class="isitdrr" width="1%" >&nbsp;&nbsp;{{ $subsub->no}}.</td>
                                  <td class="isitdrr">
                                    {{ $subsub->subrekomendasi}}
                                  
                                  </td>
                                </tr>
                              @endforeach
                            @else
                                <tr>
                                  <td class="isitdrr" width="1%" >&nbsp;&nbsp;{{$subrek->no}}.</td>
                                  <td class="isitdrr">{{ $subrek->pic['pic']}} {{ $subrek->subrekomendasi}}<br>
                                  
                                    @foreach($subsubrekomendasi->where('sub_rekomendasi_id',$subrek->id) as $ur=>$subsub)
                                        {{$subsub->no}}.{{ $subsub->subrekomendasi}}<br>
                                    @endforeach
                                  </td>
                                </tr>
                            @endif
                          @endforeach
                            <tr>
                              <td colspan="2" ><b><u>Tanggapan</u></b></td>
                            </tr>
                            <tr>
                            <td class="isitdrr" colspan="2" >{!! $rekomen->tanggapan !!}</td>
                            </tr>
                        </table><br>
                      @endif
                  </td>
                </tr>
              @endforeach
            <tr>
              <td class="tdr"><b>7. <u>PENUTUP</u></b></td>
            </tr>
            <tr>
              <td class="isitdr">{!! $lha->penutup !!}</td>
            </tr>
          </table>
          
          
           
      
      </div>
      <table width="100%">
      
        <tr>
          <td align="center"> 
              
            <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Laporan LHA</button>
            <button   class="btn btn-sm btn-primary" type="submit" id="btn-export" onclick="exportHTML();"><span class="fa fa-file-word-o"></span> Export To Word</button>
          </td>
        </tr>
     
      </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      mywindow.print();
      mywindow.close();
      return true;
  }
 
    function show() 
      {
        window.open("{{ url('popuppj/2')}}", "list", "width=800,height=420");
      }
    function showpeng() 
    {
      window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
    }
    function showss(no) 
      {
        window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
      }
    
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
<script>
  function exportHTML(){
     var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
          "xmlns:w='urn:schemas-microsoft-com:office:word' "+
          "xmlns='http://www.w3.org/TR/REC-html40'>"+
          "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
     var footer = "</body></html>";
     var sourceHTML = header+document.getElementById("prin").innerHTML+footer;
     
     var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
     var fileDownload = document.createElement("a");
     document.body.appendChild(fileDownload);
     fileDownload.href = source;
     fileDownload.download = 'Laporan_LHA.doc';
     fileDownload.click();
     document.body.removeChild(fileDownload);
  }
</script>
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:14;text-transform:capitalize;}
.isitdr{padding:5px 5px 5px 22px;font-size:14;}
.isitdrr{padding:5px 5px 5px 5px;font-size:14;vertical-align: top;}
.content-footer {
    text-align: center;
}
.source-html-outer {
    border: #d0d0d0 1px solid;
    border-radius: 3px;
    padding: 10px 20px 20px 20px;
}
</style>