@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<form method="post" action="{{ url('/lha/inserttujuan')}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>MEMO DINAS</B>
              </div>
              <div class="box-body">
                <div class="ket">
                    <div class="form-group" style="padding:1em;margin-bottom:0px;background: #dfdfec;">
                        <label>Keterangan Waktu Undangan</label>
                    </div>
                    <div class="form-group" style="padding:0.5em">
                        <label>Tanggal</label>
                        <div class='input-group date' id='datepicker'>
                            <input type='text' name="tanggal" class="form-control" value="{{ date('m/d/Y',strtotime($obyeks->tgl_undangan_memo)) }}"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                       
                    </div>
                    <div class="form-group" style="padding:0.5em">
                        <div class="form-group">
                            <label>Jam:</label>
                        </div>
                        <div class="input-group">
                            <input type="text" name="jam_memo" class="form-control time3" value="{{ date('H:i',strtotime($obyeks->jam_undangan_memo)) }}" >
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <span class="btn btn-sm btn-success" id="add" style="margin-top:30px;margin-left:13px"><i class="glyphicon glyphicon-plus"></i> Tambah</span>
                <a href="{{ url('lha/')}}"><span class="btn btn-sm btn-primary" style="margin-top:30px"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</span></a>
                <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                <br><br>
                @foreach($mengetahui as $kepada)
                    <div class="form-group" style="margin-left:20px;">
                        <label>Kepada YTH:</label><br>
                        <a href="{{ url('lha/hapustujuan/'.$kepada->id)}}"><span class="btn btn-danger" ><i class="glyphicon glyphicon-remove"></i></span></a>
                        <input type="text" class="form-control pull-right" style="display:inline;width:96%" name="kepada[]" value="{{ $kepada->kepada}}">
                    </div>
                @endforeach
                <div id="container" style="margin-left:20px;"></div><br><br>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <input type="hidden" id="id" name="id" value="{{ $obyeks->id }}" class="form-control">
                  <a href="{{ url('lha/memo/'.$obyeks->id)}}"><span class="btn btn-success" ><i class="glyphicon glyphicon-print"></i> Memo DInas</span></a>
                </div>
              </div>
            </div>
       
  </div>
  

</form>
@endsection
<style>
    .ket{margin:3px;width:100%;background:#f1f1f1;}
</style>
<script>
    function klikdate(no){
      $('#datepicker'+no).datepicker({
        autoclose: true
      })
    }
</script>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function() {
     var count = 0;
      $("#add").click(function(){
        count += 1;
          $('#container').append(
                '<br><div class="form-group" style="margin-left:20px;">'
                +'<label>Kepada YTH:</label><br>'
                +'<input type="text" class="form-control pull-right"  name="kepada[]" >'
                +'</div>'
                
          );
            //alert(count);
           
    });

    $('#add').click();
  
                        
    $(".hapus").live('click', function (ev) {
      if (ev.type == 'click') {
        $(this).parents(".records").fadeOut();
        $(this).parents(".records").remove();
        count -= 1;
        $('#add').val(count);
      }
    });
  });
       
                    
</script>

<script>
  
  function obyekaudit() 
    {
      window.open("{{ url('popobyekaudit')}}", "list", "width=800,height=420");
    }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
