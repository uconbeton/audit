@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger" style="width:99%">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="col-xs-12">
    <div class="box">
        @if(is_null($obyekfile->sts_lha))
            <div class="box-header"><a href="{{url('lha/form/'.$obyek_id)}}" ><span class="btn btn-primary btn-sm" ><i class="fa fa-reply"></i> Kembali</span></a></div>
        @else
            <div class="box-header"><a href="{{url('lha/edit/'.$obyek_id)}}" ><span class="btn btn-primary btn-sm" ><i class="fa fa-reply"></i> Kembali</span></a></div>
        @endif
        <!-- /.box-header -->
       
            <form method="post" enctype="multipart/form-data" action="{{ url('/lha/insertfile/')}}">
                <div class="box-body">         
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <input class="form-control pull-right" type="hidden" name="obyek_id" readonly  value="{{ $obyek_id }}" placeholder="Nama Obyek">
                    <label>Nama File :</label>
                    <input type="text" class="form-control pull-right"  name="nama" ><br>                           
                    <label>File :</label>
                    <input type="file" class="form-control pull-right"  name="filepencatatan" ><br>                           
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="box">
        <table width="90%" border="1" style="margin:2% 5% 5% 5%;" >
            <tr>
                @foreach($file as $x=>$file)
                    
                    @if(($x+1)%4==0)
                        <td width="25%" style="font-size:11px;padding:5px;border:solid 1px #eaeaf3" align="center">
                            <img src="{{ url('img/'.$file->type.'.png')}}" class="file"><hr>
                            <input readonly type="text" class="pp" value="{{Storage::disk('public')->url($file->file)}}" id="myInput{{$x}}">
                            <span class="btn btn-primary btn-sm" onclick="klik{{$x}}()" >Copy Link</span> 
                            <a href="{{Storage::disk('public')->url($file->file)}}" target="_blank"><span class="btn btn-warning btn-sm" >&nbsp;<i class="fa fa-search"></i></span></a>
                           
                            <a href="{{url('/lha/deletefile/'.$file->id.'/'.$file->obyek_id)}}" ><span class="btn btn-danger btn-sm" >&nbsp;<i class="fa fa-remove"></i></span></a>
                            
                        </td>
                    </tr>
                    @else
                        <td width="25%" style="font-size:11px;padding:5px;border:solid 1px #eaeaf3" align="center">
                            <img src="{{ url('img/'.$file->type.'.png')}}" class="file"><hr>
                            <input readonly type="text" class="pp" value="{{Storage::disk('public')->url($file->file)}}" id="myInput{{$x}}">
                            <span class="btn btn-primary btn-sm" onclick="klik{{$x}}()" >Copy Link</span> 
                            <a href="{{Storage::disk('public')->url($file->file)}}" target="_blank"><span class="btn btn-warning btn-sm" >&nbsp;<i class="fa fa-search"></i></span></a>
                            
                            <a href="{{url('/lha/deletefile/'.$file->id.'/'.$file->obyek_id)}}" ><span class="btn btn-danger btn-sm" >&nbsp;<i class="fa fa-remove"></i></span></a>
                            
                        </td>
                    @endif
                @endforeach
            
        </table><br>
    </div>
</div>
@endsection
<style>
    .pp{
        width:100%;
        height:30px;
        background: #ecf0f5;
        border:solid 1px #ecf0f5;
    }
    .file{
        width:230px;
        height:250px;
    }
    .btn {
        padding:1px;
        margin:5px;
    }
    .ii{
        color:red;
    }
    .tooltip {
    position: relative;
    display: inline-block;
    }

    .tooltip .tooltiptext {
    visibility: hidden;
    width: 140px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -75px;
    opacity: 0;
    transition: opacity 0.3s;
    }

    .tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
    }

    .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
    }
</style>
@push('link-java')

    @foreach($files as $x=>$files)  
        <script>
            function klik{{$x}}() {
                var copyText = document.getElementById("myInput{{$x}}");
                copyText.select();
                document.execCommand("copy");
            
            }

        </script>
    @endforeach
 
        
    
@endpush
