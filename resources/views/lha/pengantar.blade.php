@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>Surat Pengantar LHA</B>
      </div>
      <div class="box-body" id="prin">

          <table  border="0" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:5%;margin-right:5%">
            <tr>
              <td colspan="3" align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="110px">
                 <br><br>
              </td>
            </tr>
            <tr>
              <td class="tdr">Nomor </td>
              <td class="tdr">:</td>
              <td class="tdr"> {{ $obyeks->no_pengantar }}/GM/IA-KS/{{ $obyeks->kode_laporan }}/{{ $obyeks->tahun_pkat}}</td>
            </tr>
            <tr>
                <td class="tdr">Lampiran </td>
                <td class="tdr">:</td>
                <td class="tdr" > 1 (Satu) set</td>
            </tr>
            <tr>
              <td class="tdr" valign="top">Perihal </td>
              <td class="tdr" valign="top">:</td>
              <td class="tdr" valign="top"><b>Laporan Hasil {{$obyeks->nama}}</b>
              </td>
            </tr>
            
            <tr>
                <td colspan="3" class="tdr"><hr></td>
            </tr>
            <tr>
                <td colspan="3" class="tdr" align="justify" style="padding-left:80px" >
                    <b>Kepada Yth,<br>
                    Direktur Utama<br>
                    PT.Krakatau Steel (Persero), Tbk.<br>
                    di Cilegon<br></b><br>
                    Dengan Hormat,<br>
                    Berdasarkan Program Kerja Audit Tahunan (PKAT) Internal Audit PT.Krakatau Steel (Persero),Tbk.
                    Tahun {{$obyeks->tahun_pkat}} , Kami telah melaksanakan {{ $obyeks->nama}} .
                    <br><br>
                    Pelaksanaan audit berdasarkan Standar Profesi Auditor Internal dan sesuai prosedur audit yang berlaku. Hasil pelaksanaan @if($obyeks->kode_laporan=='LHA' || $obyeks->kode_laporan=='LHK' ) audit @else evaluasi @endif, kami sampaikan
                    dalam suatu @if($obyeks->kode_laporan=='LHA')Laporan Hasil Audit (LHA)@elseif($obyeks->kode_laporan=='LHK')Laporan Hasil Audit Khusus (LHK)@else Laporan Hasil Evaluasi (LHE)@endif  sebagaimana telampir.
                    <br><br>
                    Atas masalah dan rekomendasi yang diberikan, unit kerja terkait sebagai obyek @if($obyeks->kode_laporan=='LHA' || $obyeks->kode_laporan=='LHK' ) audit @else evaluasi @endif, menyetujui untuk menindaklanjuti dengan segera dalam jangka waktu
                    60 hari kalender setelah LHA diterima.
                    <br><br>
                    Demikian @if($obyeks->kode_laporan=='LHA')Laporan Hasil Audit (LHA)@elseif($obyeks->kode_laporan=='LHK')Laporan Hasil Audit Khusus (LHK)@else Laporan Hasil Evaluasi (LHE)@endif disampaikan dan atas perhatiannya diucapkan terima kasih.
                


                </td>
            </tr>
            
          </table>
          <table  border="0" width="90%" style="border-collapse:collapse;font-family:Tahoma;margin-left:5%;margin-right:5%">
            <tr align="center">
              <td class="tdr"width="70%"></td>
              <td class="tdr" style="text-align: center"><br>Cilegon, {{ date('d F Y')}}<br>INTERNAL AUDIT<br><br><br><br>
                <h3 style="margin-bottom:4px"><u><b>&nbsp&nbsp;Haryanto&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">Head of Internal Audit</h4>
              </td>
            </tr>
            <tr>
                <td colspan="2" class="tdr"><b>Tembusan, Yth:</b></td>
            </tr>
            @foreach($mengetahui as $no=>$kepada)
                <tr>
                    <td colspan="2" class="tdr">{{$no+1}}.{{ $kepada->kepada}}</td>
                </tr>
            @endforeach
          </table>
          
          
      
      </div>
      <hr>
      <table width="100%">
     
        <tr>
          <td align="center" style="padding:10px"> 
            <a href="{{ url('lha/tembusan/'.$obyeks->id)}}"><span class="btn btn-sm btn-primary" ><i class="glyphicon glyphicon-arrow-left"></i> Ubah Tembusan</span></a>
            <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Surat Pengantar LHA</button>
            
          </td>
        </tr>
     
      </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=1100');

      mywindow.document.write('<html><head><title></title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      setTimeout(function(){
        mywindow.print();
        mywindow.close();
      },250);
      return true;
  }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
 
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:16;text-align: justify;line-height: 1.4em;}
.tdrs{padding:5px;font-size:16;text-transform:capitalize;text-align: justify;font-weight:bold;word-spacing: 3px;}
#prin{margin-left:10%;margin-right:10%;width:80%;}
</style>