@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
             </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Kode<br>Obyek</th>
                  <th>Nama Obyek</th>
                  <th width="20%">Tujuan</th>
                  <th>Tim Audit</th>
                  <th width="12%">Act</th>
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($obyeks as $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->unit_kerja['kode_unit'] }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>
                            @php $x = 1; @endphp
                            @foreach( $tujuans as $tujuan )
                              @if($o->id==$tujuan->obyek_id)
                                {{ $x++ }}.{{ $tujuan->tujuan }}<br>
                              @endif
                            @endforeach
                          </td>
                          <td>
                              <ul>
                                  <li>{{ $o->pengawas['nama'] }} (Pengawas)</li>
                                  <li>{{ $o->ketua_tim['nama'] }} (Ketua Tim)</li>
                                  
                                  @foreach( $obyektims as $obyektim )
                                    @if($o->id == $obyektim->obyek_id)
                                      <li>{{ $obyektim->karyawan['nama'] }} (anggota) </li>
                                    @endif
                                    
                                  @endforeach
                                </ul>
                          </td>
                          <td align="left">
                            <form action="" method="post">
                              
                              @if($o ->sts==1)
                                <a href="{{ url('detobyek/form/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="fa fa-gear">Proses</span></a>
                              @endif
                              @if($o ->sts==2)
                                <a href="{{ url('detobyek/edit/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="fa fa-gear">Proses</span></a>
                                <a href="{{ url('obyek/detail/'.$o->id.'') }}" class=" btn btn-sm btn-info"><span class="fa fa-search-plus">Audit Plan</span></a>
                              @endif
                              @if($o ->sts==3)
                              <a href="{{ url('obyek/detail/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print">Audit Plan</span></a>
                              <a href="{{ url('detobyek/formtembusan/'.$o->id.'') }}" class=" btn btn-sm btn-success"><span class="fa fa-print"></span>Memo DInas</a>
                              
                              @endif
                              @if($o ->sts==2)
                                <a href="{{ url('detobyek/approve/'.$o->id.'') }}" class=" btn btn-sm btn-warning"><span class="fa fa-check-square-o">Selesai</span></a>
                              @endif
                              
                            </form>  
                          </td>
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
              {{ $obyeks->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
  .btn{margin:3px;}
</style>
@push('link-java')
  <script>
  
    $(function () {
      $('#example1').DataTable({
        'lengthChange' : false,
        'autoWidth': false,
        'paging': false,
        "columnDefs": [
          { "width": "25%", "targets": 2 },
          { "width": "30%", "targets": 3},
          { "width": "24%", "targets": 4 },
          { "width": "10%", "targets": 5 }
        ]      
      })
      
    })
  </script>
@endpush