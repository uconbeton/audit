@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<form method="post" action="{{ url('/detobyek/inserttembusan/'.$obyeks->id)}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>FORM TEMBUSAN</B>
              </div>
              <div class="box-body">
                <div class="form-group" >
                    <label>Kepada YTH:</label>
                        <input class="form-control pull-right" type="text"  name="kepada_yth"  value="" >
                </div>
               
                <span class="btn btn-success" id="add" style="margin-top:30px">Tambah Tembusan</span>
                <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                <br>
                
                <div id="container" style="margin-left:20px;"></div><br><br>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Simpan dan Cetak</button>
                  <input type="hidden" id="id" name="id" value="{{ $obyeks->id }}" class="form-control">
                </div>
              </div>
            </div>
        </div>
  </div>
  

</form>
@endsection

<script>
    function klikdate(no){
      $('#datepicker'+no).datepicker({
        autoclose: true
      })
    }
</script>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function() {
     var count = 0;
      $("#add").click(function(){
        count += 1;
          $('#container').append(
                '<br><label>Tembusan YTH:</label><br>'
                +'<input type="text" class="form-control pull-right"  name="kepada[]" >'
                +'</div>'
                
          );
            //alert(count);
           
    });

    $('#add').click();
    $('#add').click();
  
                        
    $(".remove_item").live('click', function (ev) {
      if (ev.type == 'click') {
        $(this).parents(".records").fadeOut();
        $(this).parents(".records").remove();
        count -= 1;
        $('#jum').val(count);
      }
    });
  });
       
                    
</script>

<script>
  
  function obyekaudit() 
    {
      window.open("{{ url('popobyekaudit')}}", "list", "width=800,height=420");
    }
 
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
