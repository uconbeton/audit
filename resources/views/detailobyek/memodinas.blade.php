@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header" style="background: #e8e8de">
        <B>MEMO DINAS</B>
      </div>
      <div class="box-body" id="prin" >
          <table  border="0" width="90%" align="center" style="border-collapse:collapse;font-family:Tahoma">
            <tr>
              <td colspan="3" align="center">
                <img src="{{url('/img/ks.jpg')}}" widt="300px" height="150px"><br>
                 {{-- <h2 style="margin-bottom:4px"><u><b>&nbsp&nbsp;MEMO&nbsp&nbsp&nbsp&nbsp;DINAS&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">Nomor : @if($obyeks->id>9){{ $obyeks->id }}@else 0{{ $obyeks->id }} @endif/IA-MD.ST/{{ date('m',strtotime($obyeks->waktu))}}.{{ date('y',strtotime($obyeks->waktu))}}</h4>                   --}}
                 <h2 style="margin-bottom:4px"><u><b>&nbsp&nbsp;MEMO&nbsp&nbsp&nbsp&nbsp;DINAS&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">Nomor : &nbsp&nbsp&nbsp;/IA-MD.ST/{{ date('m',strtotime($obyeks->waktu))}}.{{ date('y',strtotime($obyeks->waktu))}}</h4>                  
              </td>
            </tr>
            <tr>
              <td width="20%" class="tdr">Kepada YTH</td>
              <td class="tdr">:&nbsp&nbsp;{{ $mengetahui->kepada}}</td>
            <td class="tdr" width="15%"></td>
            </tr>
            <tr>
              <td class="tdr">Dari</td>
              <td class="tdr">:&nbsp&nbsp;Head of internal Audit</td>
              <td class="tdr" width="15%"></td>
            </tr>
            <tr>
              <td class="tdr">Tanggal</td>
              <td class="tdr">:&nbsp&nbsp;{{ date('d F Y')}}</td>
              <td class="tdr" width="15%"></td>
            </tr>
            <tr>
              <td class="tdr">Perihal</td>
              <td class="tdr">:&nbsp&nbsp;Audit atas {{ $obyeks->nama }}</td>
              <td class="tdr" width="15%"></td>
            </tr>
            <tr>
                <td colspan="3" class="tdr" align="justify">
                  <br><br>
                  Dengan hormat,<br>
                  Sesuai dengan program kerja audit tahunan (PKAT) tahun {{date('Y')}} Internal Audit, Kami Akan 
                  melaksanakan audit terhadap kegiatan {{ $obyeks->nama }}.<br><br>
                  Pelaksanaan audit direncanakan mulai tanggal {{ date('d F Y',strtotime($mulai->tgl_mulai))}} sampai dengan {{ date('d F Y',strtotime($sampai->tgl_sampai))}} dengan susunan tim sebagai berikut : 
                </td>
              </tr>
          </table>
          <table  border="0" width="90%" align="center" style="border-collapse:collapse;font-family:Tahoma">
            <tr>
              <td  class="tdr" width="2%">1.</td>
              <td class="tdr" width="40%">{{ $obyeks->pengawas['nama'] }}</td>
              <td class="tdr" width="6%">NIK</td>
              <td class="tdr" width="10%">{{ $obyeks->pengawas['nik'] }}</td>
              <td class="tdr">(Pengawas)</td>
            </tr>
            <tr>
              <td  class="tdr" width="2%">2.</td>
              <td class="tdr">{{ $obyeks->ketua_tim['nama'] }}.</td>
              <td class="tdr">NIK</td>
              <td class="tdr">{{ $obyeks->ketua_tim['nik'] }}</td>
              <td class="tdr">(Ketua Tim)</td>
            </tr>
              @foreach($obyekstim as $key => $anggota)
                <tr>
                  <td  class="tdr" width="2%">{{ $key+3}}.</td>
                  <td class="tdr">{{ $anggota->karyawan['nama']}}</td>
                  <td class="tdr">NIK</td>
                  <td class="tdr">{{ $anggota->karyawan['nik'] }}</td>
                  <td class="tdr">(Anggota)</td>
                </tr>
              @endforeach
                    
          </table><br>
          <table  border="0" width="90%" align="center" style="border-collapse:collapse;font-family:Tahoma">
              <tr>
                <td class="tdr" align="justify" colspan="3">
                  Sehubung dengan hal tersebut, kami mengharapkan bantuannya untuk menginformasikan
                  dan mengkordinasikan kepada unit-unit kerja terkait pada {{ $obyeks->unit_kerja['nama']}}.<br>
                  Demikian disampaikan, atas bantuan dan kerja samanya diucapkan terima kasih.
                </td>
              </tr>
          </table><br><br>
          <table  border="0" width="90%" align="center" style="border-collapse:collapse;font-family:Tahoma">
            <tr align="center">
              <td class="tdr"width="60%"></td>
              <td class="tdr">Cilegon, {{ date('d F Y') }}<br>INTERNAL AUDIT<br><br><br><br><br>
                <h3 style="margin-bottom:4px"><u><b>&nbsp&nbsp;Haryanto&nbsp&nbsp;</b></u></h2><h4 style="margin-top:4px">General Manager</h4>
              </td>
            </tr>
            <tr>
                <td class="tdr">Tembusan Yth</td>
                <td class="tdr"></td>
            </tr>
            @foreach($tembusan as $key => $tembusan)
              <tr>
                  <td class="tdr">{{$key+1}}. {{$tembusan->kepada}}</td>
                  <td class="tdr"></td>
              </tr>
            @endforeach
          </table>
         
      
      </div>
      <hr>
        <table width="100%">
          <tr>
           <td align="center"> <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Memo</button></td>
          </tr>
        </table><br><br>
    </div>
</div>

@endsection

<script>
   function print(divId) {
      var content = document.getElementById(divId).innerHTML;
      var mywindow = window.open('', 'Print', 'height=600,width=900,margin-left:10px');

      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      mywindow.print();
      mywindow.close();
      return true;
  }
 
    function show() 
      {
        window.open("{{ url('popuppj/2')}}", "list", "width=800,height=420");
      }
    function showpeng() 
    {
      window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
    }
    function showss(no) 
      {
        window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
      }
    
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>
<style>
label{width:13%;background: #bff1f7;padding-left:10px;}
.tdr{padding:5px;font-size:15px;text-transform:capitalize;}
</style>