@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<ul class="nav nav-tabs" style="margin-bottom:5px">
  <li class="active"><a href="#home">Obyek Audit</a></li>
  <li><a href="#menu1">Jadwal Pelaksanaan</a></li>
 </ul>



<form method="post" action="{{ url('/detobyek/update/'.$obyeks->id)}}">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">


    
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>FORM DETAIL OBYEK</B>
              </div>
              <div class="box-body">
                <div class="form-group">
                    <label>Unit:</label>
                    <input class="form-control pull-right" type="hidden" name="id"  readonly value="{{ $obyeks->id }}" placeholder="Nama Obyek">
                      <input class="form-control pull-right" type="text"  readonly value="{{ $obyeks->unit['nama'] }}" placeholder="Nama Obyek">
                </div>
                <div class="form-group">
                    <label>Unit Kerja:</label>
                      <input class="form-control pull-right" type="text" readonly  value="{{ $obyeks->unit_kerja['nama'] }}" placeholder="Nama Obyek">
                </div>
                <div class="form-group">
                    <label>Nama Obyek:</label>
                      <input class="form-control pull-right" type="text" readonly  value="{{ $obyeks->nama }}" placeholder="Nama Obyek">
                </div>
                 <div class="form-group">
                    <label>Sasaran:</label><br>
                    <textarea  name="sasaran" rows="4" cols="100%" style="width:100%">{{ $sasaran->sasaran }}</textarea>
                </div>
                <label>Sub Sasaran :</label>
                <textarea id="editor2" name="subsasaran" rows="10" cols="80">
                    <ul>
                        @foreach($subsasarans as $key => $subsasaran)
                            <li>.{{ $subsasaran->sasaran }}</li>
                        @endforeach
                    </ul>
                 </textarea>

                <div class="form-group">
                    <label>Resiko:</label><br>
                    <textarea  name="risiko" rows="4" cols="100%" style="width:100%">{{ $risiko->risiko }}</textarea>
                </div>
                <label>Sub Resiko :</label>
                <textarea id="editor3" name="subrisiko" rows="10" cols="80">
                    <ul>
                        @foreach($subrisikos as $key => $subrisiko)
                            <li>.{{ $subrisiko->risiko }}</li>
                        @endforeach
                    </ul>
                </textarea>
                <div class="form-group">
                  <label>Ruang Lingkup:</label><br>
                  <textarea  name="ruanglingkup" rows="4" cols="100%" style="width:100%">{{ $dasarruanglingkups->ruanglingkup }}</textarea>
                </div>
                <div class="form-group">
                    <label>Ruang Lingkup Audit:</label>
                    <select class="form-control select2" multiple="multiple" data-placeholder="Pilih Unit Kerja" name="sub_ruanglingkup[]" style="color:#000">
                        @php $no = 1; @endphp
                        @foreach($unitkerjas as $unitkerja)
                            <option value="{{ $unitkerja->id }}">{{ $unitkerja->nama }}</option>
                        @endforeach
                    </select>
                    @foreach($ruanglingkups as $urut =>$ruanglingkup)
                        <input type="hidden" name="sub_ruanglingkuplama[]" value="{{ $ruanglingkup->unit_kerja_id }}"><span class="btn btn-success">{{ $urut+1}}.{{ $ruanglingkup->unit_kerja['nama'] }}</span>
                    @endforeach
                  </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
        </div>
  </div>
  <div id="menu1" class="tab-pane fade">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header" style="background: #e8e8de">
              <B>JADWAL PENELITIAN</B>
            </div>
            <div class="box-body">
              @foreach($jadwals as $no=> $jadwal)
                
                <div class="form-group">
                  <label>{{ $no+1 }}.{{ $jadwal->tahapan['tahapan']}}:</label>
                  <input type="hidden" name="tahapan_id[]" value="{{$jadwal->tahapan['id'] }}">
                  <input type="hidden" name="kategori[]" value="{{$jadwal->tahapan['kategori'] }}">
                  <div class="input-group date">
                    
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" placeholder="Tanggal Mulai" value="{{ date('m/d/Y',strtotime($jadwal->tgl_mulai)) }}" onclick="klikdate({{ $no+1 }});" name="tgl_mulai[]"  id="datepicker{{ $no+1 }}">
                    <input type="text" class="form-control pull-right" placeholder="Tanggal Selesai" value="{{ date('m/d/Y',strtotime($jadwal->tgl_sampai)) }}" onclick="klikdate2({{ $no+1 }});" name="tgl_sampai[]"  id="datepicker2{{ $no+1 }}">
                  </div>
                </div>
               

               @endforeach
              
            </div>
        </div>
  </div>
  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</div>
</form>
@endsection


<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  $(document).ready(function(){
      $(".nav-tabs a").click(function(){
          $(this).tab('show');
      });
  });
  </script>

<script>
  function show() 
    {
      window.open("{{ url('popuppj/2')}}", "list", "width=800,height=420");
    }
  function showpeng() 
    {
      window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
    }
  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }
  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
