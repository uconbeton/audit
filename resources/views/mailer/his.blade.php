@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div  id="pesan">
        
    </div>
@endif





  
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>History</B>
              </div>
              <div class="box-body">
              <div id="wait"><img src="img/loading.gif" width="40px" height="40px"> Proses pengiriman email............................</div>
              <div id="wait2"><img src="img/loading.gif" width="40px" height="40px"> Proses Import file............................</div>
                
              <form id="myForm" method="post"  class="form-horizontal" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                
                  <span onclick="kirimemail()" class="btn btn-primary btn-sm">Kirim Email</span>
                  <span onclick="deletehis()" class="btn btn-primary btn-sm">Hapus History</span>
               
                <hr>
                <table id="example1"  class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th >No</th>
                      <th>PIC</th>
                      <th>Email</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                      @php $no = 1; @endphp
                      @foreach($items as $o)
                          <tr>
                              <td>
                                <input type="checkbox" name="cek[]" value="{{ $o->pic}}" checked> {{ $no++ }}
                              </td>
                              <td>{{ $o->pic}}<br>To: {{ $email->shift()['email'] }}<br>Cc: {{ $cc->shift()['cc'] }}</td>
                              <td> 
                                  <table width="100%" border="1">
                                    <tr align="center">  
                                      <td class="tdh">Tahun</td>
                                      <td class="tdh">Kode Lha</td>
                                      <td class="tdh">Kode Lap</td>
                                      <td class="tdh">No Temuan</td>
                                      <td class="tdh">L.Status</td>
                                      <td class="tdh">Aging</td>
                                      <td class="tdh">Keterangan</td>
                                    </tr>
                                    @foreach($temuan->where('pic',$o->pic) as $temu)
                                      <tr>  
                                        <td class="tdf">{{$temu->tahun}}</td>
                                        <td class="tdf">{{$temu->kode_lha}}</td>
                                        <td class="tdf">{{$temu->kode_lap}}</td>
                                        <td class="tdf">{{$temu->no_temuan}}</td>
                                        <td class="tdf">{{$temu->last_status}}</td>
                                        <td class="tdf">{{$temu->aging}}</td>
                                        <td class="tdf">{{$temu->keterangan}}</td>
                                      </tr>
                                    @endforeach
                                  </table>
                              
                              </td>
                              <td>
                                @if($o->sts==0)
                                    <i class="fa fa-send"></i>
                                @else
                                    <i class="fa fa-check"></i>
                                @endif

                              </td>
                              
                          </tr>
                          
                      @endforeach
                    </tbody>
                    
                  </table>
                </form>
              </div>
            </div>
        </div>
  

   
    
  
@endsection

<style>
  td{font-size:9px;padding:10px;}
  .tdf{padding-left:5px;border:solid 1px #b3b3a1;}
  .tdh{background: #e6d8fd;border:solid 1px #b3b3a1;}
  table{border:solid 1px #b3b3a1;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  $(document).ready(function(){
    $('#wait').hide();
    $('#wait2').hide();
      $(".nav-tabs a").click(function(){
          $(this).tab('show');
      });
  });
  </script>

<script>
 
 

function deletehis(){
  var data = $('#myForm').serialize();
      $.ajax({
        type: 'POST',
        url: '{{url('/email/deletehis.php')}}',
        data: data,
        beforeSend: function(){
          $('#kirimemail').attr("disabled","disabled");
          $('#kirim').attr("disabled","disabled");
          $('#myForm').css("opacity",".2");
         
        },
        success: function(datas){
            alert(datas);
            window.location.assign('{{url('/broadcase/history')}}'); 
        }
      });
}
function kirimemail(){
      var data = $('#myForm').serialize();
      $.ajax({
        type: 'POST',
        url: '{{url('/email/kirim_email_his.php')}}',
        data: data,
        beforeSend: function(){
          $('#kirimemail').attr("disabled","disabled");
          $('#kirim').attr("disabled","disabled");
          $('#myForm').css("opacity",".2");
          $('#wait').show();
        },
        success: function(datas){
            // if(data=='Sukses terkirim'){
            //   alert('Sukses');
            //   window.location.assign('{{url('/broadcase')}}');
            // }
            alert(datas);
            window.location.assign('{{url('/broadcase/history')}}'); 
        }
      });
}

$(function () {
    $('#example1').DataTable({
      'lengthChange' : false,
      'autoWidth': false,
      "columnDefs": [
        { "width": "4%", "targets": 0 },
        { "width": "25%", "targets": 1 },
        { "width": "4%", "targets": 3 }
      ]      
    })
    
  })
</script>
