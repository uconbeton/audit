@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div  id="pesan">
        
    </div>
@endif





  
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>Import Excel</B>
              </div>
              <div class="box-body">
              <div id="wait"><img src="img/loading.gif" width="40px" height="40px"> Proses pengiriman email............................</div>
              <div id="wait2"><img src="img/loading.gif" width="40px" height="40px"> Proses Import file............................</div>
                
              <form id="myForm" method="post"  class="form-horizontal" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <div class="form-group" style="margin-left:0px;width:100%">
                  <label>File Excel:</label>
                    <input class="form-control" type="file" name="fileupload"  value="{{ old('fileupload') }}" placeholder="example@email.com">
                </div>
                
                  <button type="submit" id="kirim" class="btn btn-primary btn-sm">Import</button>
                  <span onclick="kirimemail()" class="btn btn-primary btn-sm">Kirim Email</span>
                  <span onclick="resetdata()" class="btn btn-primary btn-sm">Reset</span>
               
                <hr>
                <table id="example1"  class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th >No</th>
                      <th>PIC</th>
                      <th>Email</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                      @php $no = 1; @endphp
                      @foreach($items as $o)
                          <tr>
                              <td>
                                <input type="checkbox" name="cek[]" value="{{ $o->pic}}" checked> {{ $no++ }}
                                <input type="hidden" name="email[]" value="{{$email->shift()['email']}}" >
                              </td>
                              <td>{{ $o->pic}}<br>To: {{ $email2->shift()['email'] }}<br>Cc: {{ $cc->shift()['cc'] }}</td>
                              <td> 
                                  <table width="100%" border="1">
                                    <tr align="center">  
                                      <td class="tdh">Tahun</td>
                                      <td class="tdh">Kode Lha</td>
                                      <td class="tdh">Kode Lap</td>
                                      <td class="tdh">No Temuan</td>
                                      <td class="tdh">L.Status</td>
                                      <td class="tdh">Aging</td>
                                      <td class="tdh">Keterangan</td>
                                    </tr>
                                    @foreach($temuan->where('pic',$o->pic) as $temu)
                                      <tr>  
                                        <td class="tdf">{{$temu->tahun}}</td>
                                        <td class="tdf">{{$temu->kode_lha}}</td>
                                        <td class="tdf">{{$temu->kode_lap}}</td>
                                        <td class="tdf">{{$temu->no_temuan}}</td>
                                        <td class="tdf">{{$temu->last_status}}</td>
                                        <td class="tdf">{{$temu->aging}}</td>
                                        <td class="tdf">{{$temu->keterangan}}</td>
                                      </tr>
                                    @endforeach
                                  </table>
                              
                              </td>
                              <td>
                                @if($o->sts==0)
                                    <i class="fa fa-send"></i>
                                @else
                                    <i class="fa fa-check"></i>
                                @endif

                              </td>
                              
                          </tr>
                          
                      @endforeach
                    </tbody>
                    
                  </table>
                </form>
              </div>
            </div>
        </div>
  

   
    
  
@endsection

<style>
  td{font-size:9px;padding:10px;}
  .tdf{padding-left:5px;border:solid 1px #b3b3a1;}
  .tdh{background: #e6d8fd;border:solid 1px #b3b3a1;}
  table{border:solid 1px #b3b3a1;}
</style>
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  $(document).ready(function(){
      $(".nav-tabs a").click(function(){
          $(this).tab('show');
      });
  });
  </script>

<script>
 
 $(document).ready(function(e){
	$('#wait').hide();
	$('#wait2').hide();
  $("#myForm").on('submit', function(e){
      e.preventDefault();
      $.ajax({
          type: 'POST',
          url: '{{url('/broadcase/importtemuan')}}',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
              $('#kirim').attr("disabled","disabled");
              $('#myForm').css("opacity",".5");
              $('#wait2').show();
          },
          success: function(datas){
            $('#wait2').hide();
            window.location.assign('{{url('/broadcase')}}');
           
          }
      });
  });
});

function kirimemail(){
      var data = $('#myForm').serialize();
      $.ajax({
        type: 'POST',
        url: '{{url('/email/kirim_email.php')}}',
        data: data,
        beforeSend: function(){
          $('#kirimemail').attr("disabled","disabled");
          $('#kirim').attr("disabled","disabled");
          $('#myForm').css("opacity",".2");
          $('#wait').show();
        },
        success: function(datas){
            // if(data=='Sukses terkirim'){
            //   alert('Sukses');
            //   window.location.assign('{{url('/broadcase')}}');
            // }
            alert(datas);
            window.location.assign('{{url('/broadcase')}}'); 
        }
      });
}

function resetdata(){
      $.ajax({
        type: 'GET',
        url: '{{url('/email/reset_temuan.php')}}',
        data: 'sts=0',
        success: function(datas){
            
            window.location.assign('{{url('/broadcase')}}'); 
        }
      });
}

$(function () {
    $('#example1').DataTable({
      'lengthChange' : false,
      'autoWidth': false,
      'paging': false,
      "columnDefs": [
        { "width": "4%", "targets": 0 },
        { "width": "25%", "targets": 1 },
        { "width": "4%", "targets": 3 }
      ]      
    })
    
  })
</script>
