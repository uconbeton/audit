<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box" style="min-height: 55px;">
    <a href="{{ url('/kategori')}}"><span class="info-box-icon bg-red" style="height: 55px;"><i class="fa fa-fax"></i></span></a>

    <div class="info-box-content">
      <span class="info-box-text">Kode Audit</span>
      <span class="info-box-number">PKAT</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<div class="clearfix visible-sm-block"></div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box" style="min-height: 55px;">
        <a href="{{ url('/')}}"><span class="info-box-icon bg-aqua" style="height: 55px;"><i class="ion ion-ios-gear-outline"></i></span></a>

      <div class="info-box-content">
        <span class="info-box-text">Progres Realisasi </span>
        <span class="info-box-number">PKAT<small></small></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box" style="min-height: 55px;">
        <a href="{{ url('/tahapan')}}"><span class="info-box-icon bg-green" style="height: 55px;"><i class="fa fa-sitemap"></i></span></a>

      <div class="info-box-content">
        <span class="info-box-text">Progres Realisasi</span>
        <span class="info-box-number">Tahapan</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box" style="min-height: 55px;">
        <a href="{{ url('/kode')}}"><span class="info-box-icon bg-yellow" style="height: 55px;"><i class="ion ion-ios-people-outline"></i></span></a>

      <div class="info-box-content">
        <span class="info-box-text">Temuan Dan Rekomendasi</span>
        <span class="info-box-number">Audit</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>