<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li>
      <a href="{{ url('')}}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        
      </a>
      
    </li>
    @if(Auth::user()->posisi_id==5)
      <li>
        <a href="{{url('user')}}">
          <i class="fa fa-th"></i> <span>User Audit </span>
          <span class="pull-right-container">
            
          </span>
        </a>
      </li>
      <li>
        <a href="{{url('broadcase')}}">
          <i class="fa fa-th"></i> <span>Broadcast Email </span>
          <span class="pull-right-container">
            
          </span>
        </a>
      </li>
      <li>
        <a href="{{url('broadcase/history')}}">
          <i class="fa fa-th"></i> <span>Riwayat Broadcast </span>
          <span class="pull-right-container">
            
          </span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Master Unit Kerja</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('master/')}}"><i class="fa fa-minus"></i>Direktorat</a></li>
          <li><a href="{{url('master/subdit')}}"><i class="fa fa-minus"></i>Subdit</a></li>
          <li><a href="{{url('master/divisi')}}"><i class="fa fa-minus"></i>Divisi</a></li>
          <li><a href="{{url('master/dinas')}}"><i class="fa fa-minus"></i>Dinas</a></li>
          <li><a href="{{url('master/seksi')}}"><i class="fa fa-minus"></i>Seksi</a></li>
          <li><a href="{{url('master/perusahaan')}}"><i class="fa fa-minus"></i>Anak Perusahaan</a></li>
          
        </ul>
      </li>
      <li>
        <a href="{{url('obyek')}}">
          <i class="fa fa-th"></i> <span>Perencanaan</span>
          <span class="pull-right-container">
            
          </span>
        </a>
      </li>
     
      @endif
      @if( Auth::user()->posisi_id==1 || Auth::user()->posisi_id==5)
        <li>
          <a href="{{url('detobyek')}}">
            <i class="fa fa-th"></i> <span>Audit Plan & Memo</span>
            <span class="pull-right-container">
              
            </span>
          </a>
        </li>
      @endif
      @if(Auth::user()->posisi_id==2 || Auth::user()->posisi_id==1 || Auth::user()->posisi_id==5)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Desk Audit Ketua</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('deskaudit')}}"><i class="fa fa-minus"></i>Desk Audit Program</a></li>
          <li><a href="{{url('deskaudit/pencatatanket')}}"><i class="fa fa-minus"></i>Desk Audit Pencatatan</a></li>
            
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Compliance Ketua</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('compliance')}}"><i class="fa fa-minus"></i>Compliance Program</a></li>
          <li><a href="{{url('compliance/pencatatanket')}}"><i class="fa fa-minus"></i>Compliance Pencatatan</a></li>
            
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Substantive Ketua</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('substantive')}}"><i class="fa fa-minus"></i>Substantive Program</a></li>
          <li><a href="{{url('substantive/pencatatanket')}}"><i class="fa fa-minus"></i>Substantive Pencatatan</a></li>
            
        </ul>
      </li>
      
    @endif
    @if(Auth::user()->posisi_id==1)
    <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Desk Audit Pengawas</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('deskaudit/indexapp')}}"><i class="fa fa-minus"></i>Persetujuan Desk Audit</a></li>
          <li><a href="{{url('deskaudit/pencatatanpeng')}}"><i class="fa fa-minus"></i>Persetujuan Pencatatan</a></li>
          
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Compliance Pengawas</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('compliance/indexapp')}}"><i class="fa fa-minus"></i>Persetujuan Compliance</a></li>
          <li><a href="{{url('compliance/pencatatanpeng')}}"><i class="fa fa-minus"></i>Persetujuan Pencatatan</a></li>
          
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Substantive Pengawas</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('substantive/indexapp')}}"><i class="fa fa-minus"></i>Substantive Program</a></li>
          <li><a href="{{url('substantive/pencatatanpeng')}}"><i class="fa fa-minus"></i>Substantive Pencatatan</a></li>
          
        </ul>
      </li>
    @endif
    @if(Auth::user()->posisi_id==3 || Auth::user()->posisi_id==5 ||  Auth::user()->posisi_id==2)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Desk Audit Anggota</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{url('deskaudit/pencatatan')}}"><i class="fa fa-minus"></i>Desk Audit Pencatatan</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Compliance Anggota</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{url('compliance/pencatatan')}}"><i class="fa fa-minus"></i>Compliance Pencatatan</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Substantive Anggota</span>
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{url('substantive/pencatatan')}}"><i class="fa fa-minus"></i>Substantive Pencatatan</a></li>
        </ul>
      </li>
      
      @endif
      @if(Auth::user()->posisi_id==5)
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i>
            <span>Substantive Admin</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{url('substantive/indexadmin')}}"><i class="fa fa-minus"></i>Substantive Pencatatan</a></li>
          </ul>
        </li>
      @endif
      @if(Auth::user()->posisi_id==2 || Auth::user()->posisi_id==1 || Auth::user()->posisi_id==5)
      <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i>
            <span>Report LHA</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('lha')}}"><i class="fa fa-minus"></i>Index LHA</a></li>
             
          </ul>
        </li>
      @endif
      <!-------Report LHA-------------------------------------->
      
      
  </ul>