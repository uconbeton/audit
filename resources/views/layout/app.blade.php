<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PT Krakatau Steel (Persero) Tbk.</title>
  <link rel="shortcut icon" href="{{url('img/fav.png')}}">
  @include('layout.style')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div id="loading"></div>
<div class="wrapper">

 @include('layout.header')
   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ url('img/ks.png')}}" width="100%" height="10%">
        </div>
        <div class="pull-left info">
          <p>{{ substr(Auth::user()->nama,0,15)}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
     
	 @include('layout.sidebar')
    </section>
 </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        
      </h1>
     
    </section>

     <section class="content">
      <div class="row">
        <div class="box box-primary">
          <div class="box-header" style="background: #e8e8de;margin-top: -29px;text-align:right;padding: 2px;">
            <form method="post" action="{{url('/session_tahun/')}}" style="display:inline">
              <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
              <B>Pilih Tahun</B>
              <select class="form-control" name="tahun" style="width:10%;display:inline">
                @foreach($viewobyek as $tahuncari)
                 
                    <option value="{{$tahuncari->tahun_pkat}}" @if(session('tahun')==$tahuncari->tahun_pkat) selected @endif>{{$tahuncari->tahun_pkat}}</option>
                 
                @endforeach
              </select>
              <button type="submit" class="btn btn-primary">Proses</button>
            </form>
          </div> 
        </div> 
		
		@yield('konten')
		
		
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('layout.footer')
@include('layout.script') 
@stack('link-java')
</body>
</html>
