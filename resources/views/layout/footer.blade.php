<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; {{date('Y')}} PT Krakatau Steel (Persero) Tbk.</strong>
  </footer>
@include('layout.setting')
  <div class="control-sidebar-bg"></div>
</div>