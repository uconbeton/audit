@include('layout.style')
<style>
#tabtab{
	padding:10px;
	width:100%;
	height:100%;
}td{
	font-size:12px;
	padding:0px;
}
th{
	font-size:12px;
}
</style>
<div id="tabtab">
	<table id="example1" class="table table-bordered table-striped">
		<thead>
			<tr>
                <th width="5%">No</th>
                <th width="15%">Kode</th>
				<th>Nama </th>
				
			</tr>
		</thead>
		
		<tbody>
				@php $no = 1; @endphp
				@foreach($items as $o)
				<tr onClick="ada('{{ $o->nama }}','{{ $o->id }}','{{ $o->kode }}')">
						<td>{{ $no++ }}</td>
						<td>{{ $o->kode }}</td>
						<td>{{ $o->nama }}</td>
					</tr>
				@endforeach
		</tbody>
	</table>

	
<script type="text/javascript" >

	function ada(nama,id,kode){ 
		window.opener.$('#unit_kerja_id').val(id);
		window.opener.$('#nama_unit_kerja').val(nama);
        window.opener.$('#kode').val(kode);
		
		window.close();
	}
	
</script>
@include('layout.script') 
<script>

  
	
	$(document).ready( function () {
	  $('#example1').DataTable();
	});
  </script>