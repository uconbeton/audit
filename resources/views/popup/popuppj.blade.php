@include('layout.style')
<style>
#tabtab{
	padding:10px;
	width:100%;
	height:100%;
}td{
	font-size:12px;
	padding:0px;
}
th{
	font-size:12px;
}
</style>
<div id="tabtab">
	<table id="example1" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th width="10%">NIK</th>
				<th>Nama </th>
				
			</tr>
		</thead>
		
		<tbody>
				@php $no = 1; @endphp
				@foreach($items as $o)
				<tr onClick="ada('{{ $o->nama }}','{{ $o->nik }}','{{ $o->posisi_id }}','{{ $id }}')">
						<td>{{ $no++ }}</td>
						<td>{{ $o->nik }}</td>
						<td>{{ $o->nama }}</td>
					</tr>
				@endforeach
		</tbody>
	</table>

	
<script type="text/javascript" >

	function ada(nama,nik,id){ 
		window.opener.$('#nik_pengawas').val(nik);
		window.opener.$('#nama_pengawas').val(nama);
		
		window.close();
	}
	
</script>
@include('layout.script') 