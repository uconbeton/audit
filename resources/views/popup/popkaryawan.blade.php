@include('layout.style')
<style>
#tabtab{
	padding:10px;
	width:100%;
	height:100%;
}td{
	font-size:12px;
	padding:0px;
}
th{
	font-size:12px;
}
</style>
<div class="box-body">
    <table id="example1"  class="table table-bordered table-striped">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th>Nik </th>
				<th>Nama </th>
				<th  width="20%">Divisi</th>
				
			</tr>
		</thead>
		
		<tbody>
				@php $no = 1; @endphp
				@foreach($items as $o)
				<tr onClick="ada('{{ $o->nama }}','{{ $o->nik }}','{{ $o->unit_kerja_id }}')">
						<td>{{ $no++ }}</td>
						<td>{{ $o->nik }}</td>
						<td>{{ $o->nama }}</td>
						<td>{{ $o->unit_kerja['nama'] }}</td>
					</tr>
				@endforeach
		</tbody>
	</table>
</div>
	
<script type="text/javascript" >

	function ada(nama,nik,unit_kerja_id){ 
		window.opener.$('#nama').val(nama);
		window.opener.$('#nik').val(nik);
		window.opener.$('#unit_kerja_id').val(unit_kerja_id);
		
		window.close();
	}
	
</script>


@include('layout.script')
<script>

        $(function () {
          $('#example1').DataTable({
            'lengthChange' : true,
            'autoWidth': false,
            'paging': true,
            "columnDefs": [
              { "width": "10%", "targets": 1 }
            ]      
          })
          
        })
      </script>
      