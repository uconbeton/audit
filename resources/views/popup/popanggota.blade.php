@include('layout.style')
<style>
#tabtab{
	padding:10px;
	width:100%;
	height:100%;
}td{
	font-size:12px;
	padding:0px;
}
th{
	font-size:12px;
}
</style>
<div id="tabtab">
	<table id="example1" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th>Nama </th>
				<th  width="20%">Divisi</th>
				<th width="20%">Posisi</th>
				
			</tr>
		</thead>
		
		<tbody>
				@php $no = 1; @endphp
				@foreach($items as $o)
				<tr onClick="ada('{{ $o->nama }}','{{ $o->id }}','{{ $o->posisi_id }}')">
						<td>{{ $no++ }}</td>
						<td>{{ $o->nama }}</td>
						<td>{{ $o->divisi['nama'] }}</td>
						<td>{{ $o->posisi['nama'] }} - {{ $o->posisi_id }}</td>
					</tr>
				@endforeach
		</tbody>
	</table>

	
<script type="text/javascript" >

	function ada(nama,id,posisi_id){ 
		window.opener.$('#tim_id'+posisi_id).val(id);
		window.opener.$('#nama'+posisi_id).val(nama);
		
		window.close();
	}
	
</script>
@include('layout.script') 