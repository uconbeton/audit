@extends('layout.app')
@section('konten')
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              
               </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example1"  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th >No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Cost</th>
                  <th></th>
                  
                </tr>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach($useraudit as $o)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $o->nik }}</td>
                          <td>{{ $o->nama }}</td>
                          <td>{{ $o->jabatan }}</td>
                          <td>{{ $o->unit_kerja_id }}</td>
                          <td>
                            <a href="{{ url('user/edit/'.$o->id.'') }}" class=" btn btn-sm btn-primary"><span class="fa fa-gear">Update</span></a>
                              
                          </td>
                          
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
              {{ $useraudit->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
@endsection
<style>
  .btn{margin:3px;}
</style>
@push('link-java')
  <script>
  
    $(function () {
      $('#example1').DataTable({
        'lengthChange' : false,
        'autoWidth': false,
        'paging': false,
        "columnDefs": [
          { "width": "10%", "targets": 1 },
          { "width": "40%", "targets": 2 },
          { "width": "30%", "targets": 3},
          { "width": "15%", "targets": 4 }
          { "width": "5%", "targets": 5 }
        ]      
      })
      
    })
  </script>
@endpush