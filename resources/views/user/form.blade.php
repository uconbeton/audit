@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif




<form method="post" action="/user/insert">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header" style="background: #e8e8de">
                <B>FORM USER AUDIT</B>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label>Nama:</label>
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger" onclick="karyawan()">Pilih</button>
                    </div>
                    <input type="hidden" id="nik" name="nik" value="{{ old('nik') }}"class="form-control">
                    <input type="hidden" id="unit_kerja_id" name="unit_kerja_id" value="{{ old('unit_kerja_id') }}"class="form-control">
                    <input type="text" id="nama" name="fullname" value="{{ old('nama') }}" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label>Username:</label>
                    <input class="form-control pull-right" type="text" name="username" id="email"  value="{{ old('email') }}" placeholder="Nama Obyek">
                </div>

                <div class="form-group">
                  <label>Akses:</label>
                  <select class="form-control" style="width: 100%;" id="akses_id" name="akses_id"  tabindex="-1" aria-hidden="true">
                    @foreach($akses as $akses)
                      <option value="{{ $akses->id }}" @if (old('akses_id') ==$akses->id) {{ 'selected' }} @endif>[ {{ $akses->id }} ] {{ $akses->akses }}</option>
                    @endforeach
                  </select>
                </div>


                
                
                
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
        </div>
  
</form>
@endsection


<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  $(document).ready(function(){
      $(".nav-tabs a").click(function(){
          $(this).tab('show');
      });
  });
  </script>

<script>
  function unitkerja() 
    {
      var unit=$('#unit_id').val();
      window.open("../popunitkerja/"+unit+"", "list", "width=800,height=420");
    }

  function show() 
    {
      window.open("{{ url('popketuatim')}}", "list", "width=800,height=500");
    }
    
  function showpeng() 
    { var unit=$('#kategori_audit_id').val();
      if(unit==3 || unit==4 || unit==7){
        window.open("{{ url('popuppj/4')}}", "list", "width=800,height=420");
      }else{
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }
      
    }

  function karyawan(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popkaryawan/", "list", "width=800,height=420");
    }

  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>
