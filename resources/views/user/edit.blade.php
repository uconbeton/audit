@extends('layout.app')
@section('konten')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


  
  <form method="post" action="/obyek/update/{{ $obyeks->id}}">
    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header" style="background: #e8e8de">
          <B>FORM USER</B>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label>NIK:</label>
              <input class="form-control pull-right" type="text" name="nik"  value="{{ $data->nik }}" placeholder="Nama Obyek">
          </div>
          <div class="form-group">
            <label>Nama:</label>
              <input class="form-control pull-right" type="text" name="nama"  value="{{ $data->nama }}" placeholder="Nama Obyek">
          </div>
          <div class="form-group">
            <label>Jabatan:</label>
              <input class="form-control pull-right" type="text" name="jabatan"  value="{{ $data->jabatan }}" placeholder="Nama Obyek">
          </div>

          <div class="form-group">
            <label>Kategori Audit:</label>
            <select class="form-control" style="width: 100%;" id="kategori_audit_id" name="kategori_audit_id"  tabindex="-1" aria-hidden="true">
              @foreach($jabatan as $jabatan)
                <option value="{{ $jabatan->id }}" @if ( $data->jabatan ==$jabatan->jabatan) {{ 'selected' }} @endif>{{ $jabatan->jabatan }}</option>
              @endforeach
            </select>
          </div>


            <label>Tujuan :</label>
            <textarea id="editor1" name="tujuan" rows="10" cols="80">
              @foreach($tujuans as $tujuan)
                <li>{{ $tujuan->tujuan }}</li>
              @endforeach
            </textarea>
          <!-- Date range -->

          <div class="form-group">
            <label>Pengawas:</label>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-danger" onclick="showpeng()">Pilih</button>
              </div>
              <input type="hidden" id="nik_pengawas" name="nik_pengawas" value="{{ $obyeks->nik_pengawas }}" class="form-control">
              <input type="text" id="nama_pengawas" name="nama_pengawas" value="{{ $obyeks->pengawas['nama'] }}" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label>Ketua Tim:</label>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="button" class="btn btn-danger" onclick="show()">Pilih</button>
              </div>
              <input type="hidden" id="nik_ketua_tim" name="nik_ketua_tim" value="{{ $obyeks->nik_ketua_tim }}"class="form-control">
              <input type="text" id="nama_ketua_tim" name="nama_ketua_tim" value="{{ $obyeks->ketua_tim['nama'] }}" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label>Anggota:</label>
            <select class="form-control select2" multiple="multiple" data-placeholder="Pilih Anggota" name="anggota[]" style="color:#000">
                @php $no = 1; @endphp
                @foreach($anggotas as $anggota)
                    <option value="{{ $anggota->nik }}">{{ $anggota->nama }}</option>
                @endforeach
            </select>
           
              @foreach($obyekstim as $urut =>$anggotalama)
                <input type="hidden" name="anggotalama[]" value="{{ $anggotalama->nik }}"><span class="btn btn-success">{{ $urut+1}}.{{ $anggotalama->karyawan['nama'] }}  (anggota)</span>
              @endforeach
           
          </div>
          
          
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
  </div>
</form>
@endsection
<script src="{{url('/bower_components/jquery.min.js')}}"></script>

<script>
  function unitkerja() 
    {
      var unit=$('#unit_id').val();
      window.open("../../popunitkerja/"+unit+"", "list", "width=800,height=420");
    }

  function show() 
    {
      window.open("{{ url('popketuatim')}}", "list", "width=800,height=500");
    }
    
  function showpeng() 
    { var unit=$('#kategori_audit_id').val();
      if(unit==3 || unit==4 || unit==7){
        window.open("{{ url('popuppj/4')}}", "list", "width=800,height=420");
      }else{
        window.open("{{ url('popuppj/1')}}", "list", "width=800,height=420");
      }
      
    }

  function showanggota(no) 
    {
      // window.open("{{ url('popanggota/"no"')}}", "list", "width=800,height=420");
      window.open("../popanggota/"+no+"", "list", "width=800,height=420");
    }

  function showss(no) 
    {
      window.open("mod/popbarang.php?no="+no+"", "list", "width=800,height=420");
    }


    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>

<script>
  function klikdate(no){
    $('#datepicker'+no).datepicker({
      autoclose: true
    })
  }

  function klikdate2(no){
    $('#datepicker2'+no).datepicker({
      autoclose: true
    })
  }
    
</script>