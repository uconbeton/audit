@extends('layout.app')
@section('konten')
<style>
    th{text-align:center;background: #e8e8f5;}
    td{padding:5px;}
</style>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
    
@include('layout.dashboard')

<div class="col-md-12">
  @if(Auth::user()->posisi_id==1)
    <div class="alert alert-warning  alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        Terdapat permintaan persetujuan data audit pada list notifikasi dibawah ini.
    </div>
    <div class="nav-tabs-custom" >
      <ul class="nav nav-tabs">
        <li class="active"><a href="#activity" data-toggle="tab">Notifikasi Persetujuan Program</a></li>
        <li><a href="#timeline" data-toggle="tab">Notifikasi Persetujuan Pencatatan</a></li>
        
      </ul>
      <div class="tab-content" style="padding: 7px;">
          <div class="active tab-pane" id="activity">
                @foreach($obyekspeng as $not=>$notif)
                  @if($notif->sts_deskaudit_id=='0')
                  
                    
                      
                        <!-- Post -->
                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{ url('img/loading.gif')}}" alt="user image">
                                
                                <span class="username">
                                  <a href="{{ url('deskaudit/indexapp')}}">Persetujuan Deskaudit "{{ $notif->nama }}"</a>
                                  <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                </span>
                                <span class="description"><a href="{{ url('deskaudit/indexapp')}}" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Cek Persetujuan</a> Dikirim {{ date('d F Y',strtotime($notif->tgl_sts_deskaudit_id))}}  </span>
                            
                          </div>
                          
                          

                        </div>
                        
                      
                      
                    
                  @elseif($notif->sts_compliance_id=='0')
                  
                    
                    
                        <!-- Post -->
                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{ url('img/loading.gif')}}" alt="user image">
                                
                                <span class="username">
                                  <a href="{{ url('compliance/indexapp')}}">Persetujuan Compliance "{{ $notif->nama }}"</a>
                                  <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                </span>
                                <span class="description"><a href="{{ url('compliance/indexapp')}}" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Cek Persetujuan</a> Dikirim {{ date('d F Y',strtotime($notif->tgl_sts_compliance_id))}}  </span>
                            
                          </div>
                          
                          

                        </div>
                        
                      
                    
                    @elseif($notif->sts_substantive_id=='0')
                  
                      
                          <!-- Post -->
                          <div class="post">
                            <div class="user-block">
                              <img class="img-circle img-bordered-sm" src="{{ url('img/loading.gif')}}" alt="user image">
                                  
                                  <span class="username">
                                      <a href="{{ url('substantive/indexapp')}}">Persetujuan Substantive "{{ $notif->nama }}"</a>
                                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                  </span>
                                  <span class="description"><a href="{{ url('substantive/indexapp')}}" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Cek Persetujuan</a> Dikirim {{ date('d F Y',strtotime($notif->tgl_sts_substantive_id))}}  </span>
                              
                            </div>
                            
                            

                          </div>
                      
                      
                  @endif
                @endforeach
          </div>


          <div class="tab-pane" id="timeline">
              @foreach($obyeks as $not=>$notif)
                @if($notif->sts_pencatatan_deskaudit=='0')
                
                  
                    
                      <!-- Post -->
                      <div class="post">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="{{ url('img/loading.gif')}}" alt="user image">
                              
                              <span class="username">
                                <a href="{{ url('deskaudit/pencatatanpeng')}}">Persetujuan Deskaudit "{{ $notif->nama }}"</a>
                                <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                              </span>
                              <span class="description"><a href="{{ url('deskaudit/pencatatanpeng')}}" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Cek Persetujuan</a> Dikirim {{ date('d F Y',strtotime($notif->tgl_sts_pencatatan_deskaudit))}}  </span>
                          
                        </div>
                        
                        

                      </div>
                      
                    
                    
                  
                @elseif($notif->sts_pencatatan_compliance=='0')
                
                  
                  
                      <!-- Post -->
                      <div class="post">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="{{ url('img/loading.gif')}}" alt="user image">
                              
                              <span class="username">
                                <a href="{{ url('compliance/pencatatanpeng')}}">Persetujuan Compliance "{{ $notif->nama }}"</a>
                                <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                              </span>
                              <span class="description"><a href="{{ url('compliance/pencatatanpeng')}}" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Cek Persetujuan</a> Dikirim {{ date('d F Y',strtotime($notif->tgl_sts_pencatatan_compliance))}}  </span>
                          
                        </div>
                        
                        

                      </div>
                      
                    
                  
                  @elseif($notif->sts_pencatatan_substantive=='0')
                
                    
                        <!-- Post -->
                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{ url('img/loading.gif')}}" alt="user image">
                                
                                <span class="username">
                                    <a href="{{ url('substantive/pencatatanpeng')}}">Persetujuan Substantive "{{ $notif->nama }}"</a>
                                  <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                </span>
                                <span class="description"><a href="{{ url('substantive/pencatatanpeng')}}" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Cek Persetujuan</a> Dikirim {{ date('d F Y',strtotime($notif->tgl_sts_pencatatan_substantive))}}  </span>
                            
                          </div>
                          
                          

                        </div>
                    
                    
                @endif
              @endforeach
        </div>
    </div>
       
  </div> 
 @endif
</div>

    <div class="col-md-12" >
        <div class="box box-primary">
            <div class="box-header" style="background: #e8e8de">
            <B>Progres Realisasi PKAT {{ session('tahun')}}</B>
            </div>
            <div class="box-body">
                <table width="100%" border="1">
                    <tr>
                        <th rowspan="2" width="5%">No</th>
                        <th rowspan="2">Nama Obyek</th>
                        <th rowspan="2" width="5%">Rencana Terbit</th>
                        <th width="10%" colspan="2">Desk</th>
                        <th width="10%" colspan="2">Compliance</th>
                        <th width="10%" colspan="2">Substantive</th>
                        <th width="5%" rowspan="2">Draft LHA </th>
                        <th width="5%" rowspan="2">Bahas</th>
                        <th width="5%" rowspan="2">Terbit</th>
                        <th width="10%" rowspan="2">Persen</th>
                    </tr>
                    <tr>
                        <th>Prog</th>
                        <th>Catt</th>
                        <th>Prog</th>
                        <th>Catt</th>
                        <th>Prog</th>
                        <th>Catt</th>
                    </tr>

                    @foreach($obyekss as $no=> $obyek)
                        <tr>
                            <td align="center">{{ $no+1 }}</td>
                            <td>{{ $obyek->nama}}</td>
                            <td>{{ date('d/m/Y',strtotime($obyek->loadPenerbitanLha()->tgl_sampai))}}</td>
                            
                                @if($obyek->sts_deskaudit_id==1)
                                    @if($obyek->tgl_sts_deskaudit_id >= $obyek->loadDeskAuditProgram()->tgl_sampai)
                                      <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadDeskAuditProgram()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_deskaudit_id}}"></td>
                                    @else
                                      <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadDeskAuditProgram()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_deskaudit_id}}"></td>
                                    @endif
                                    
                                @else
                                      <td align="center"></td>
                                @endif
                           
                                @if($obyek->sts_pencatatan_deskaudit==1)
                                    @if($obyek->tgl_sts_pencatatan_deskaudit >= $obyek->loadDeskAuditCatatan()->tgl_sampai)
                                        <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadDeskAuditCatatan()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pencatatan_deskaudit}}"></td>
                                    @else
                                        <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadDeskAuditCatatan()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pencatatan_deskaudit}}"></td>
                                    @endif
                                @else
                                      <td align="center"></td>
                                @endif


                            
                                @if($obyek->sts_compliance_id==1)
                                    @if($obyek->tgl_sts_compliance_id >= $obyek->loadComplianceProgram()->tgl_sampai)
                                      <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadComplianceProgram()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_compliance_id}}"></td>
                                    @else
                                      <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadComplianceProgram()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_compliance_id}}"></td>
                                    @endif
                                @else
                                      <td align="center"></td>
                                @endif
                            
                                @if($obyek->sts_pencatatan_compliance==1)
                                    @if($obyek->tgl_sts_pencatatan_compliance >= $obyek->loadComplianceCatatan()->tgl_sampai)
                                        <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadComplianceCatatan()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pencatatan_compliance}}"></td>
                                    @else
                                        <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadComplianceCatatan()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pencatatan_compliance}}"></td>
                                    @endif
                                @else
                                        <td align="center"></td>
                                @endif
                            
                                @if($obyek->sts_substantive_id==1)
                                    @if($obyek->tgl_sts_substantive_id >= $obyek->loadSubstantiveeProgram()->tgl_sampai)
                                      <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadSubstantiveeProgram()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_substantive_id}}"></td>
                                    @else
                                      <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadSubstantiveeProgram()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_substantive_id}}"></td>
                                    @endif
                                @else
                                      <td align="center"></td>
                                @endif
                            
                            
                                @if($obyek->sts_pencatatan_substantive==1)
                                    @if($obyek->tgl_sts_pencatatan_substantive >= $obyek->loadSubstantiveCatatan()->tgl_sampai)
                                        <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadSubstantiveCatatan()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pencatatan_substantive}}"></td>
                                    @else
                                        <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadSubstantiveCatatan()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pencatatan_substantive}}"></td>
                                    @endif
                                @else
                                        <td align="center"></td>
                                @endif
                            
                            
                                @if($obyek->sts_setujui1==1)
                                
                                    @if($obyek->tgl_sts_setujui1 >= $obyek->loadPenyusunanLha()->tgl_sampai)
                                        <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadPenyusunanLha()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_setujui1}}"></td>
                                    @else
                                        <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadPenyusunanLha()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_setujui1}}"></td>
                                    @endif
                       
                                @else
                                        <td align="center"></td>
                                @endif

                                @if($obyek->sts_setujui2==1)
                                    
                                    @if($obyek->tgl_sts_setujui2 >= $obyek->loadMemoLha()->tgl_sampai)
                                        <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadMemoLha()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_setujui2}}"></td>
                                    @else
                                        <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadMemoLha()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_setujui2}}"></td>
                                    @endif
                                
                                @else
                                          <td align="center"></td>
                                @endif

                                @if($obyek->sts_pengantar==1)
                                
                                    @if($obyek->tgl_sts_pengantar >= $obyek->loadPenerbitanLha()->tgl_sampai)
                                        <td bgcolor="#ff2a2a" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadPenerbitanLha()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pengantar}}"></td>
                                    @else
                                        <td bgcolor="#60c36c" align="center" title="Rencana : {{date('Y-m-d',strtotime($obyek->loadPenerbitanLha()->tgl_sampai))}} / Aktual :{{$obyek->tgl_sts_pengantar}}"></td>
                                    @endif
                       
                                @else
                                        <td align="center"></td>
                                @endif
                        </td>
                            <td align="center">
                               {{ $obyek->total}}%
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td bgcolor="#fff" colspan="2" align="center">% Progres </td>
                        <td bgcolor="#fff" align="center"></td>
                        <td bgcolor="#fff" align="center">5</td>
                        <td bgcolor="#fff" align="center">15</td>
                        <td bgcolor="#fff" align="center">20</td>
                        <td bgcolor="#fff" align="center">30</td>
                        <td bgcolor="#fff" align="center">35</td>
                        <td bgcolor="#fff" align="center">60</td>
                        <td bgcolor="#fff" align="center">80</td>
                        <td bgcolor="#fff" align="center">90</td>
                        <td bgcolor="#fff" align="center">100</td>
                        <td bgcolor="#fff" align="center">{{ $totalpresentase }}%</td>
                    </tr>
                </table>
            </div>
        </div> 
    </div> 
    
    


@endsection
<!-- jQuery 3 -->
<script src="{{url('/bower_components/jquery/dist/jquery.min.js')}}"></script>




<script>
  
  $(function () {
    
    /*
     * Flot Interactive Chart
     * -----------------------
     */
    // We use an inline data source in the example, usually data would
    // be fetched from a server
    var data = [], totalPoints = 100

    function getRandomData() {

      if (data.length > 0)
        data = data.slice(1)

      // Do a random walk
      while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y    = prev + Math.random() * 10 - 5

        if (y < 0) {
          y = 0
        } else if (y > 100) {
          y = 100
        }

        data.push(y)
      }

      // Zip the generated y values with the x values
      var res = []
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
      }

      return res
    }

    var interactive_plot = $.plot('#interactive', [getRandomData()], {
      grid  : {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color     : '#3c8dbc'
      },
      lines : {
        fill : true, //Converts the line chart to area chart
        color: '#3c8dbc'
      },
      yaxis : {
        min : 0,
        max : 100,
        show: true
      },
      xaxis : {
        show: true
      }
    })

    var updateInterval = 500 //Fetch data ever x milliseconds
    var realtime       = 'on' //If == to on then fetch data every x seconds. else stop fetching
    function update() {

      interactive_plot.setData([getRandomData()])

      // Since the axes don't change, we don't need to call plot.setupGrid()
      interactive_plot.draw()
      if (realtime === 'on')
        setTimeout(update, updateInterval)
    }

    //INITIALIZE REALTIME DATA FETCHING
    if (realtime === 'on') {
      update()
    }
    //REALTIME TOGGLE
    $('#realtime .btn').click(function () {
      if ($(this).data('toggle') === 'on') {
        realtime = 'on'
      }
      else {
        realtime = 'off'
      }
      update()
    })
    /*
     * END INTERACTIVE CHART
     */

    /*
     * LINE CHART
     * ----------
     */
    //LINE randomly generated data

    var sin = [], cos = []
    // for (var i = 0; i < 14; i += 0.5) {
    //   sin.push([i, Math.sin(i)])
    //   cos.push([i, Math.cos(i)])
    // }
    @foreach($kategori as $kat=>$kate)
      sin.push([{{$kat++}}, Math.sin('{{$kat++}}')])
      cos.push([{{$kat++}}, Math.cos({{$kat++}})])

    @endforeach
    var line_data1 = {
      data : sin,
      color: '#3c8dbc'
    }
    var line_data2 = {
      data : cos,
      color: '#00c0ef'
    }

    //----------------------------------------------------------kategori--------------------------------

    $.plot('#kategoriii', [line_data1], {
      grid  : {
        hoverable  : true,
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0,
        lines     : {
          show: true
        },
        points    : {
          show: true
        }
      },
      lines : {
        fill : false,
        color: ['#3c8dbc', '#f56954']
      },
      yaxis : {
        show: true
      },
      xaxis : {
        show: true
      }
    })




    //Initialize tooltip on hover
    $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
      position: 'absolute',
      display : 'none',
      opacity : 0.8
    }).appendTo('body')
    $('#line-chart').bind('plothover', function (event, pos, item) {

      if (item) {
        var x = item.datapoint[0].toFixed(2),
            y = item.datapoint[1].toFixed(2)

        $('#line-chart-tooltip').html(item.series.label + ' of ' + x + ' = ' + y)
          .css({ top: item.pageY + 5, left: item.pageX + 5 })
          .fadeIn(200)
      } else {
        $('#line-chart-tooltip').hide()
      }

    })
    /* END LINE CHART */

    /*
     * FULL WIDTH STATIC AREA CHART
     * -----------------
     */
    var areaData = [[2, 88.0], [3, 93.3], [4, 102.0], [5, 108.5], [6, 115.7], [7, 115.6],
      [8, 124.6], [9, 130.3], [10, 134.3], [11, 141.4], [12, 146.5], [13, 151.7], [14, 159.9],
      [15, 165.4], [16, 167.8], [17, 168.7], [18, 169.5], [19, 168.0]]
    $.plot('#area-chart', [areaData], {
      grid  : {
        borderWidth: 0
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color     : '#00c0ef'
      },
      lines : {
        fill: true //Converts the line chart to area chart
      },
      yaxis : {
        show: false
      },
      xaxis : {
        show: false
      }
    })

    /* END AREA CHART */

    /*
     * BAR CHART
     * ---------
     */
//-----------------------------------------bar chart--------------------------------------------------------------
var kateg = {
      //data : [['January', 10], ['February', 8], ['March', 4], ['April', 13], ['May', 17], ['June', 9]],
      data : [
                    @foreach($kategori as $kat=>$kate)
                                
                         ['{{$kate->kode}}',{{ $jumket[$kat]}}],
                                
                          
                    @endforeach
      ],
      color: 'green'
    }
    $.plot('#kategori', [kateg], {
      grid  : {
        borderWidth: 1,
        borderColor: 'red',
        tickColor  : 'red'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */




//-----------------------------------------bar chart--------------------------------------------------------------
    var bar_data = {
      //data : [['January', 10], ['February', 8], ['March', 4], ['April', 13], ['May', 17], ['June', 9]],
      data : [
                    @foreach($obyeks as $no=> $obyek)
                        
                            @foreach($kodifikasi as $kode)
                                
                                    ['{{$kode->kodifikasi}}',{{ ($ssubrekomendasi->shift()['count'] + $ssubsubrekomendasi->shift()['count'])}}],
                                
                            @endforeach
                    
                    @endforeach
      ],
      color: '#3c8dbc'
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */

    /*
     * DONUT CHART
     * -----------
     */
    //-------------------------- deskaudit-------------------------------------------------------------------------
    var deskData = [
      { label: 'Selesai', data: {{$prodesk}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$prodesnnot}}, color: 'red' }
    ]
    $.plot('#deskaudit', deskData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

    var deskcatData = [
      { label: 'Selesai', data: {{$pendesk}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$pendesknot}}, color: 'red' }
    ]
    $.plot('#deskcatatan', deskcatData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })
    

    //---------------------------------------compliance-----------------------------------------------------------
    var comData = [
      { label: 'Selesai', data: {{$procom}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$procomnot}}, color: 'red' }
    ]
    $.plot('#compliance', comData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })


    var comcatData = [
      { label: 'Selesai', data: {{$pencom}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$pencomnot}}, color: 'red' }
    ]
    $.plot('#comcatatan', comcatData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

    //---------------------------------------substantive-----------------------------------------------------------
    var comData = [
      { label: 'Selesai', data: {{$prosubs}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$prosubsnot}}, color: 'red' }
    ]
    $.plot('#substantive', comData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })


    var comcatData = [
      { label: 'Selesai', data: {{$pensubs}}, color: '#3c8dbc' },
      { label: 'Proses', data: {{$pensubsnot}}, color: 'red' }
    ]
    $.plot('#subsmcatatan', comcatData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

  })

  /*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
</script>