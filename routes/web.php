<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('a/{personnel_no}/{email?}', 'Auth\LoginController@programaticallyEmployeeLogin')
    ->name('login.a');


Route::group([
    'middleware'    => 'auth'],function(){
        Route::get('/', 'HomeController@index');
        Route::get('/kode', 'HomeController@kodifikasi');
        Route::get('/tahapan', 'HomeController@tahapan');
        Route::get('/kategori', 'HomeController@kategori');
        Route::get('/tarikdata', 'Popcontroller@tarik');
        Route::get('/tarikkaryawan', 'Popcontroller@tarikkaryawan');
    });

Route::get('/loginn', function () {
    return view('login.login');
});
Route::group([
    'prefix' =>'obyek',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'ObyekController@obyeklist');
    Route::get('/form', 'ObyekController@obyekform');
    Route::get('/edit/{id}', 'ObyekController@obyekedit');
    Route::post('/insert', 'ObyekController@insert');
    Route::post('/update/{id}', 'ObyekController@update');
    Route::post('/updatemenyetujui/{id}', 'ObyekController@updatemenyetujui');
    Route::get('/detail/{id}', 'ObyekController@detail');
    Route::get('/memo/{id}', 'ObyekController@memo');
    Route::get('/delete/{id}', 'ObyekController@delete');
    Route::get('/deltim/{id}', 'ObyekController@deltim');
	
});
Route::post('/session_tahun', 'MasterController@session_tahun');
Route::group([
    'prefix' =>'master',
    'middleware'    => 'auth'],function(){
    
    Route::get('/', 'MasterController@direktorat');
    Route::get('/subdit', 'MasterController@subdit');
    Route::get('/divisi', 'MasterController@divisi');
    Route::get('/dinas', 'MasterController@dinas');
    Route::get('/seksi', 'MasterController@seksi');
    Route::get('/perusahaan', 'MasterController@perusahaan');
    
    
    
});

Route::group([
    'prefix' =>'user',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'UserauditController@index');
    Route::get('/form', 'UserauditController@form');
    Route::get('/edit/{id}', 'UserauditController@edit');
	
});

Route::group([
    'prefix' =>'detobyek',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'DetailobyekController@index');
    Route::get('/form/{id}', 'DetailobyekController@form');
    Route::get('/approve/{id}', 'DetailobyekController@approve');
    Route::get('/edit/{id}', 'DetailobyekController@formedit');
    Route::get('/formtembusan/{id}', 'DetailobyekController@formtembusan');
    Route::post('/insert', 'DetailobyekController@insert');
    Route::post('/inserttembusan/{id}', 'DetailobyekController@inserttembusan');
    Route::post('/update/{id}', 'DetailobyekController@update');
    Route::get('/detail/{id}', 'ObyekController@detail');
    Route::get('/delete/{id}', 'ObyekController@delete');
    Route::get('/memodinas/{id}', 'DetailobyekController@memodinas');
    
	
});

Route::group([
    'prefix' =>'deskaudit',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'DeskController@index');
    Route::get('/indexapp', 'DeskController@indexapp');
    Route::get('/form/{id}', 'DeskController@form');
    Route::get('/formrevisi/{id}', 'DeskController@formrevisi');
    Route::get('/formrevisipencatatan/{id}', 'DeskController@formrevisipencatatan');
    Route::post('/insert', 'DeskController@insert');
    Route::post('/update/{id}', 'DeskController@update');
    Route::post('/insertpencatatan/{id}', 'DeskController@insertpencatatan');
    Route::post('/insert_revisi/{id}', 'DeskController@insert_revisi');
    Route::post('/insert_revisipencatatan/{id}', 'DeskController@insert_revisipencatatan');
    Route::get('/edit/{id}', 'DeskController@edit');
    Route::get('/detaildesk/{id}', 'DeskController@indexdesk');
    Route::get('/detaildeskpeng/{id}', 'DeskController@indexdeskpeng');
    Route::get('/detailpengawas/{id}', 'DeskController@indexpengawas');
    Route::get('/detailpencatatan/{id}', 'DeskController@detailpencatatan');
    Route::get('/detailpencatatanpeng/{id}', 'DeskController@detailpencatatanpeng');
    Route::get('/pencatatan/', 'DeskController@indexpencatatan');
    Route::get('/pencatatanpeng/', 'DeskController@indexpencatatanpeng');
    Route::get('/pencatatanket/', 'DeskController@indexpencatatanket');
    Route::get('/delete/{id}', 'DeskController@delete');
    Route::get('/kirimdeskaudit/{id}', 'DeskController@kirimdeskaudit');
    Route::get('/kirimpencatatandeskaudit/{id}', 'DeskController@kirimpencatatandeskaudit');
    Route::get('/setujui/{id}', 'DeskController@setujui');
    Route::get('/setujuipencatatan/{id}', 'DeskController@setujuipencatatan');
    Route::get('/delete_langkah/{id}', 'DeskController@delete_langkah');
    Route::get('/file/{id}/{id1}', 'DeskController@filedeskaudit');
    Route::get('/deletefile/{kode}/{id}/{id1}', 'DeskController@deletefile');
    Route::post('/insertfile/', 'DeskController@insertfile');
	
});

Route::group([
    'prefix' =>'compliance',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'ComplianceController@index');
    Route::get('/indexapp', 'ComplianceController@indexapp');
    Route::get('/form/{id}', 'ComplianceController@form');
    Route::get('/formrevisi/{id}', 'ComplianceController@formrevisi');
    Route::get('/formrevisipencatatan/{id}', 'ComplianceController@formrevisipencatatan');
    Route::post('/insert', 'ComplianceController@insert');
    Route::post('/update/{id}', 'ComplianceController@update');
    Route::post('/insertpencatatan/{id}', 'ComplianceController@insertpencatatan');
    Route::post('/insert_revisi/{id}', 'ComplianceController@insert_revisi');
    Route::post('/insert_revisipencatatan/{id}', 'ComplianceController@insert_revisipencatatan');
    Route::get('/edit/{id}', 'ComplianceController@edit');
    Route::get('/detailcompliance/{id}', 'ComplianceController@indexcompliance');
    Route::get('/detailpengawas/{id}', 'ComplianceController@indexpengawas');
    Route::get('/detailpencatatan/{id}', 'ComplianceController@detailpencatatan');
    Route::get('/detailpencatatanpeng/{id}', 'ComplianceController@detailpencatatanpeng');
    Route::get('/pencatatan/', 'ComplianceController@indexpencatatan');
    Route::get('/pencatatanpeng/', 'ComplianceController@indexpencatatanpeng');
    Route::get('/pencatatanket/', 'ComplianceController@indexpencatatanket');
    Route::get('/delete/{id}', 'ComplianceController@delete');
    Route::get('/kirimcompliance/{id}', 'ComplianceController@kirimcompliance');
    Route::get('/kirimpencatatancompliance/{id}', 'ComplianceController@kirimpencatatancompliance');
    Route::get('/setujui/{id}', 'ComplianceController@setujui');
    Route::get('/setujuipencatatan/{id}', 'ComplianceController@setujuipencatatan');
    Route::get('/delete_langkah/{id}', 'ComplianceController@delete_langkah');
    Route::get('/file/{id}/{id1}', 'ComplianceController@filecompliance');
    Route::get('/deletefile/{kode}/{id}/{id1}', 'ComplianceController@deletefile');
    Route::post('/insertfile/', 'ComplianceController@insertfile');
	
});

Route::group([
    'prefix' =>'substantive',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'SubstantiveTestController@index');
    Route::get('/indexapp', 'SubstantiveTestController@indexapp');
    Route::get('/indexadmin', 'SubstantiveTestController@indexadmin');
    Route::get('/form/{id}', 'SubstantiveTestController@form');
    Route::get('/formrevisi/{id}', 'SubstantiveTestController@formrevisi');
    Route::get('/formrevisipencatatan/{id}', 'SubstantiveTestController@formrevisipencatatan');
    Route::post('/insert', 'SubstantiveTestController@insert');
    Route::post('/update/{id}', 'SubstantiveTestController@update');
    Route::post('/insertpencatatan/{id}', 'SubstantiveTestController@insertpencatatan');
    Route::post('/insert_revisi/{id}', 'SubstantiveTestController@insert_revisi');
    Route::post('/insert_revisipencatatan/{id}', 'SubstantiveTestController@insert_revisipencatatan');
    Route::get('/edit/{id}', 'SubstantiveTestController@edit');
    Route::get('/detailsubstantive/{id}', 'SubstantiveTestController@indexsubstantive');
    Route::get('/detailpengawas/{id}', 'SubstantiveTestController@indexpengawas');
    Route::get('/detailpencatatan/{id}', 'SubstantiveTestController@detailpencatatan');
    Route::get('/detailpencatatanpeng/{id}', 'SubstantiveTestController@detailpencatatanpeng');
    Route::get('/pencatatan/', 'SubstantiveTestController@indexpencatatan');
    Route::get('/pencatatanpeng/', 'SubstantiveTestController@indexpencatatanpeng');
    Route::get('/pencatatanket/', 'SubstantiveTestController@indexpencatatanket');
    Route::get('/delete/{id}', 'SubstantiveTestController@delete');
    Route::get('/kirimsubstantive/{id}', 'SubstantiveTestController@kirimsubstantive');
    Route::get('/kirimpencatatansubstantive/{id}', 'SubstantiveTestController@kirimpencatatansubstantive');
    Route::get('/setujui/{id}', 'SubstantiveTestController@setujui');
    Route::get('/setujuipencatatan/{id}', 'SubstantiveTestController@setujuipencatatan');
    Route::get('/delete_langkah/{id}', 'SubstantiveTestController@delete_langkah');
    Route::get('/file/{id}/{id1}', 'SubstantiveTestController@filesubstantive');
    Route::get('/deletefile/{kode}/{id}/{id1}', 'SubstantiveTestController@deletefile');
    Route::post('/insertfile/', 'SubstantiveTestController@insertfile');
	
});

Route::group([
    'prefix' =>'lha',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'LhaController@index');
    Route::get('/form/{id}', 'LhaController@form');
    Route::get('/formrekomendasi/{id}', 'LhaController@formrekomendasi');
    Route::get('/hapus_temuan', 'LhaController@hapus_temuan');
    Route::get('/formsub/{id}', 'LhaController@formsub');
    Route::get('/tujuan/{id}', 'LhaController@tujuan');
    Route::get('/tembusan/{id}', 'LhaController@tembusan');
    Route::get('/editrekomendasi/{id}', 'LhaController@editrekomendasi');
    Route::get('/tanggapan/{id}', 'LhaController@tanggapan');
    Route::get('/edit/{id}', 'LhaController@edit');
    Route::get('/laporan/{id}', 'LhaController@laporan');
    Route::post('/insert', 'LhaController@insert');
    Route::post('/insertsub', 'LhaController@insertsub');
    Route::post('/inserttujuan', 'LhaController@inserttujuan');
    Route::post('/inserttembusan', 'LhaController@inserttembusan');
    Route::get('/surattugas/{id}', 'LhaController@surattugas');
    Route::get('/hapustujuan/{id}', 'LhaController@hapustujuan');
    Route::get('/hapustembusan/{id}', 'LhaController@hapustembusan');
    Route::get('/memo/{id}', 'LhaController@memo');
    Route::get('/pengantar/{id}', 'LhaController@pengantar');
    Route::get('/slip/{id}', 'LhaController@slip');
    Route::post('/insertrekomendasi', 'LhaController@insertrekomendasi');
    Route::post('/updaterekomendasi', 'LhaController@updaterekomendasi');
    Route::post('/inserttanggapan', 'LhaController@inserttanggapan');
    Route::post('/update', 'LhaController@update');
    Route::post('/updatemenyetujui/{id}', 'LhaController@updatemenyetujui');
    Route::get('/detail/{id}', 'LhaController@detail');
    Route::get('/memo/{id}', 'LhaController@memo');
    Route::get('/memo/{id}', 'LhaController@memo');
    Route::get('/deletesub/{id}', 'LhaController@deletesub');
    Route::get('/setujui1/{id}', 'LhaController@setujui1');
    Route::get('/setujui2/{id}', 'LhaController@setujui2');
    Route::get('/setujui3/{id}', 'LhaController@setujui3');
    Route::get('/deskaudit/{id}', 'LhaController@cetakdeskaudit');
    Route::get('/compliance/{id}', 'LhaController@cetakcompliance');
    Route::get('/substantive/{id}', 'LhaController@cetaksubstantive');
    Route::get('/prosessetujui1/{id}', 'LhaController@ijin1');
    Route::get('/prosessetujui2/{id}', 'LhaController@ijin2');
    Route::get('/prosessetujui3/{id}', 'LhaController@ijin3');
    Route::get('/file/{id}', 'LhaController@filelha');
    Route::get('/deletefile/{kode}/{id}', 'LhaController@deletefile');
    Route::post('/insertfile/', 'LhaController@insertfile');
	
});

Route::group([
    'prefix' =>'sasaran',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'SasaranController@sasaranlist');
    Route::get('/form', 'SasaranController@sasaranform');
    Route::get('/edit/{id}', 'ObyekController@obyekedit');
    Route::post('/insert', 'SasaranController@insert');
	
});

Route::group([
    'prefix' =>'broadcase',
    'middleware'    => 'auth'],function(){
    Route::get('/', 'MasterController@formemail');
    Route::get('/history', 'MasterController@his');
    Route::post('/kirimemail', 'MasterController@kirimemail');
    Route::post('/importtemuan', 'MasterController@importtemuan');
	
});

Route::get('popuppj/{id?}', 'Popcontroller@poppj');
Route::get('popanggota/{id?}', 'Popcontroller@popanggota');
Route::get('popkaryawan/', 'Popcontroller@popkaryawan');
Route::get('popobyekaudit/', 'Popcontroller@popobyekaudit');
Route::get('popunitkerja/{id?}', 'Popcontroller@popunitkerja');
Route::get('popketuatim', 'Popcontroller@popketuatim');

Auth::routes();
